import DataTable from "datatables.net";
import "datatables.net-bs5/css/dataTables.bootstrap5.css";
global.DataTable = DataTable;

document.addEventListener("DOMContentLoaded", () => {
	var defaultOrderCol = 0;
	var defaultOrderDir = "asc";
	document.querySelectorAll(".datatable").forEach((element) => {
		var defaultOrderCol = $(element).attr("orderCol") ?? defaultOrderCol;
		var defaultOrderDir = $(element).attr("orderDir") ?? defaultOrderDir;
		new DataTable("#" + $(element).attr("id"), {
			order: [[defaultOrderCol, defaultOrderDir]],
			lengthChange: false,
			pagingType: "simple",
			autoWidth: false,
			language: {
				decimal: "",
				emptyTable: "No hay información",
				info: "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
				infoEmpty: "Mostrando 0 to 0 of 0 Entradas",
				infoFiltered: "(Filtrado de _MAX_ total entradas)",
				infoPostFix: "",
				thousands: ",",
				lengthMenu: "Mostrar _MENU_ Entradas",
				loadingRecords: "Cargando...",
				processing: "Procesando...",
				search: "Buscar:",
				zeroRecords: "No se encontraron resultados",
				paginate: {
					first: "<< ",
					last: " >>",
					next: " >",
					previous: "< ",
				},
			},
		});
	});
});
