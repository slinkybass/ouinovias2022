import flatpickr from "flatpickr";
import "flatpickr/dist/l10n/es.js";
import "flatpickr/dist/flatpickr.css";

document.addEventListener("DOMContentLoaded", () => {
	document.querySelectorAll("[data-ea-datetime-field]").forEach((element) => {
		const datetime = flatpickr(element, {
			locale: moment.locale(),
			minuteIncrement: 1,
			altInput: true,
			allowInput: true,
			disableMobile: true,
			dateFormat: "YYYY-MM-DDTHH:mm:ss",
			altFormat: moment.localeData().longDateFormat("L") + " " + moment.localeData().longDateFormat("LT"),
			formatDate: (date, format, locale) => {
				return moment(date).format(format);
			},
			parseDate: (datestr, format) => {
				return moment(datestr, format, true).toDate();
			},
			enableTime: true,
			time_24hr: true,
		});
	});
});
