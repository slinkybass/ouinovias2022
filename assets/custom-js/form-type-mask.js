import IMask from "imask";

document.addEventListener("DOMContentLoaded", () => {
	document.querySelectorAll("[data-ea-mask-field]").forEach((element) => {
		var isRegex = element.getAttribute("mask-regex") == "true" ? true : false;
		var mask = isRegex ? new RegExp(element.getAttribute("mask")) : element.getAttribute("mask");
		IMask(element, {
			mask: mask
		});
	});
});
