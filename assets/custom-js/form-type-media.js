document.addEventListener("DOMContentLoaded", () => {
	var imageExts = ["apng", "avif", "bmp", "gif", "jpg", "jpeg", "jfif", "pjpeg", "pjp", "png", "svg", "webp"];
	document.querySelectorAll(".media-input").forEach((element) => {
		var formWidget = $(element).closest(".form-widget");
		var input = formWidget.find(".media-input input");

		var mediaThumb = formWidget.find(".media-thumb");
		var mediaContainer = mediaThumb.find(".media-container");

		var mediaImage = mediaContainer.find(".media-image");
		var mediaFile = mediaContainer.find(".media-file");
		var mediaRemove = mediaContainer.find(".media-remove");

		var modal = formWidget.find(".modal");
		var modalDimiss = formWidget.find('[data-bs-dismiss="modal"]');
		var iframe = modal.find("iframe");

		mediaRemove.on("click", function () {
			input.val("");
			mediaThumb.removeClass("mb-2");
			mediaContainer.addClass("d-none");
		});

		input.on("input", function () {
			inputChanged(input.val());
		});

		iframe.on("load", function () {
			$(this).contents().on("click", ".select", function () {
				var path = $(this).attr("data-path");
				input.val(path);
				inputChanged(path);
				modalDimiss.trigger("click");
			});
		});

		function inputChanged(path) {
			if (path) {
				var fileExt = path.split("?").shift().split(".").pop().toLowerCase();
				var isImage = imageExts.includes(fileExt);
				if (isImage) {
					mediaImage.css("background-image", 'url("' + path + '")');
					mediaImage.find("a").attr("href", path);
					mediaImage.removeClass("d-none");
					mediaFile.addClass("d-none");
				} else {
					mediaFile.removeClass("d-none");
					mediaFile.find("a").attr("href", path);
					mediaImage.addClass("d-none");
				}
				mediaThumb.addClass("mb-2");
				mediaContainer.removeClass("d-none");
			} else {
				mediaThumb.removeClass("mb-2");
				mediaContainer.addClass("d-none");
			}
		}
	});
});
