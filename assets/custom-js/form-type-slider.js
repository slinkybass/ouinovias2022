import noUiSlider from "nouislider";
global.noUiSlider = noUiSlider;
import 'nouislider/dist/nouislider.css';

document.addEventListener("DOMContentLoaded", () => {
	document.querySelectorAll("[data-ea-slider-field]").forEach((element) => {
		var slider = element.parentElement.parentElement.getElementsByClassName('slider')[0];
		noUiSlider.create(slider, {
			tooltips: true,
			connect: true,
			step: element.step ? parseFloat(element.step) : 1,
			start: [
				element.value ? parseFloat(element.value) : (element.min ? parseFloat(element.min) : 0)
			],
			format: {
				to: function (value) {
					return parseFloat(parseFloat(value).toFixed(2));
				},
				from: function (value) {
					return parseFloat(parseFloat(value).toFixed(2));
				},
			},
			range: {
				min: element.min ? parseFloat(element.min) : 0,
				max: element.max ? parseFloat(element.max) : 99,
			},
		});

		slider.noUiSlider.on("update", function (value) {
			element.value = value;
		});
		element.addEventListener("change", function () {
			slider.noUiSlider.set(this.value);
		});
	});
});
