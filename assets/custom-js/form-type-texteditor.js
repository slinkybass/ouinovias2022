import tinymce from "tinymce";
import "tinymce/icons/default/icons.js";
import "tinymce/themes/silver/theme.js";
import "tinymce/skins/ui/oxide/skin.css";

import "tinymce/plugins/advlist";
import "tinymce/plugins/anchor";
import "tinymce/plugins/autolink";
import "tinymce/plugins/autoresize";
import "tinymce/plugins/code";
import "tinymce/plugins/emoticons";
import "tinymce/plugins/fullscreen";
import "tinymce/plugins/hr";
import "tinymce/plugins/image";
import "tinymce/plugins/link";
import "tinymce/plugins/lists";
import "tinymce/plugins/media";
import "tinymce/plugins/pagebreak";
import "tinymce/plugins/paste";
import "tinymce/plugins/preview";
import "tinymce/plugins/tabfocus";
import "tinymce/plugins/table";

document.addEventListener("DOMContentLoaded", () => {
	document.querySelectorAll("[data-ea-texteditor-field]").forEach((element) => {
		tinymce.init({
			target: element,
			language: moment.locale(),
			branding: false,
			menubar: false,
			statusbar: false,
			contextmenu: false,
			browser_spellcheck: true,
			autoresize_bottom_margin: 0,
			relative_urls : false,
			remove_script_host : false,
			document_base_url : 'https://reservas.ouinovias.com/',
			setup: function (editor) {
				editor.on("change", function () {
					editor.save();
				});
			},
			skin: getCookie("ea/colorScheme") == "dark" ? "oxide-dark" : "",
			content_css: getCookie("ea/colorScheme") == "dark" ? "dark" : "",
			plugins: 'advlist anchor autolink autoresize code emoticons fullscreen hr image link lists media pagebreak paste preview tabfocus table',
			toolbar: element.getAttribute("data-toolbar") ?? 'bold italic underline strikethrough | fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent | numlist bullist | forecolor backcolor removeformat |link image media table | emoticons | hr pagebreak anchor | code preview fullscreen'
		});
	});
});

function getCookie(c_name) {
	if (document.cookie.length > 0) {
		var c_start = document.cookie.indexOf(c_name + "=");
		if (c_start != -1) {
			c_start = c_start + c_name.length + 1;
			var c_end = document.cookie.indexOf(";", c_start);
			if (c_end == -1) {
				c_end = document.cookie.length;
				return document.cookie.substring(c_start, c_end);
			}
		}
	}
	return null;
}
