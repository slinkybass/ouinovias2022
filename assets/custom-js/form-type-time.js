import flatpickr from "flatpickr";
import "flatpickr/dist/l10n/es.js";
import "flatpickr/dist/flatpickr.css";

document.addEventListener("DOMContentLoaded", () => {
	document.querySelectorAll("[data-ea-time-field]").forEach((element) => {
		const time = flatpickr(element, {
			locale: moment.locale(),
			minuteIncrement: 1,
			altInput: true,
			allowInput: true,
			disableMobile: true,
			dateFormat: "HH:mm",
			altFormat: moment.localeData().longDateFormat("LT"),
			formatDate: (date, format, locale) => {
				return moment(date).format(format);
			},
			parseDate: (datestr, format) => {
				return moment(datestr, format, true).toDate();
			},
			enableTime: true,
			time_24hr: true,
			noCalendar: true,
		});
	});
});
