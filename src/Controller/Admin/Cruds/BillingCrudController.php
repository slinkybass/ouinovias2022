<?php

namespace App\Controller\Admin\Cruds;

use App\Entity\Billing;
use App\Field\FieldGenerator;
use App\Service\CsvService;

use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Option\EA;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Factory\FilterFactory;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

class BillingCrudController extends AbstractCrudController
{
	private $em;
	private $translator;
	private $adminUrlGenerator;
	private $csvService;

	public function __construct(EntityManagerInterface $em, TranslatorInterface $translator, AdminUrlGenerator $adminUrlGenerator, CsvService $csvService)
	{
		$this->em = $em;
		$this->translator = $translator;
		$this->adminUrlGenerator = $adminUrlGenerator;
		$this->csvService = $csvService;
	}

	public static function getEntityFqcn(): string
	{
		return Billing::class;
	}

	public function configureCrud(Crud $crud): Crud
	{
		$crud->setEntityLabelInSingular($this->translator->trans('entities.billing.singularPaid'));
		$crud->setEntityLabelInPlural($this->translator->trans('entities.billing.pluralPaid'));
		$crud->setDefaultSort(['paid' => 'ASC', 'year' => 'DESC', 'month' => 'DESC']);

		$entityId = filter_input(INPUT_GET, EA::ENTITY_ID, FILTER_SANITIZE_URL);
		$entity = $entityId ? $this->em->getRepository($this->getEntityFqcn())->find($entityId) : null;
		if ($entity) {
			$crud->setPageTitle(Crud::PAGE_DETAIL, $this->translator->trans('entities.billing.singularPaid') . ': ' . $entity);
			$crud->setPageTitle(Crud::PAGE_EDIT, $this->translator->trans('ea.titles.edit', [
				'%entity_label_singular%' => $this->translator->trans('entities.billing.singularPaid') . ': ' . $entity
			]));
		}

		return $crud;
	}

	public function configureFields(string $pageName): iterable
	{
		$dataPanel = FieldGenerator::panel($this->translator->trans('entities.billing.sections.data'))
			->setIcon('fas fa-fw fa-file-invoice-dollar');
		$professional = FieldGenerator::association('professional')
			->setLabel($this->translator->trans('entities.professional.singular'))
			->setCrudController(ProfessionalCrudController::class)
			->setQueryBuilder(function ($queryBuilder) {
				return $queryBuilder
					->leftJoin('entity.role', 'r')
					->andWhere("r.name = 'ROLE_PROFESSIONAL'")
					->andWhere("entity.active = true");
			})->setColumns(6);
		$months = [];
		foreach (range(1, 12) as $id) {
			$months[$id] = $this->translator->trans('entities.billing.fields.months.' . $id);
		}
		$month = FieldGenerator::choice('month')
			->setLabel($this->translator->trans('entities.billing.fields.month'))
			->setChoices(array_flip($months))
			->setColumns(3);
		$year = FieldGenerator::integer('year')
			->setLabel($this->translator->trans('entities.billing.fields.year'))
			->setColumns(3);
		$date = FieldGenerator::text('date')
			->setLabel($this->translator->trans('entities.billing.fields.date'));
		$bill = FieldGenerator::media('bill')
			->setLabel($this->translator->trans('entities.billing.fields.bill'))
			->setFormTypeOption('attr.data-type', 'public_facturas')
			->setRequired(true);
		$paid = FieldGenerator::switch('paid')
			->setLabel($this->translator->trans('entities.billing.fields.paid'));
		$num = FieldGenerator::text('num')
			->setLabel($this->translator->trans('entities.billing.fields.num'))
			->setRequired(true)
			->setColumns(4);
		$price = FieldGenerator::float('price')
			->setLabel($this->translator->trans('entities.billing.fields.price'))
			->setTemplatePath('field/price.html.twig')
			->setRequired(true)
			->setColumns(4);
		$calcPrice = FieldGenerator::float('calcPrice')
			->setLabel($this->translator->trans('entities.billing.fields.calcPrice'))
			->setFormTypeOption('mapped', false)
			->setTemplatePath('field/price.html.twig');
		$accountNumber = FieldGenerator::text('accountNumber')
			->setLabel($this->translator->trans('entities.billing.fields.accountNumber'))
			->setRequired(true)
			->setColumns(4);
		$cifNifType = FieldGenerator::text('cifNifType')
			->setLabel($this->translator->trans('entities.billing.fields.cifNifType'))
			->setRequired(true)
			->setColumns(6);
		$cifNif = FieldGenerator::text('cifNif')
			->setLabel($this->translator->trans('entities.billing.fields.cifNif'))
			->setRequired(true)
			->setColumns(6);
		$address = FieldGenerator::text('address')
			->setLabel($this->translator->trans('entities.billing.fields.address'))
			->setRequired(true)
			->setColumns(5);
		$city = FieldGenerator::text('city')
			->setLabel($this->translator->trans('entities.billing.fields.city'))
			->setRequired(true)
			->setColumns(5);
		$cp = FieldGenerator::text('cp')
			->setLabel($this->translator->trans('entities.billing.fields.cp'))
			->setRequired(true)
			->setColumns(2);

		if ($pageName == Crud::PAGE_INDEX) {
			yield $professional;
			yield $date;
			yield $num;
			yield $price;
			yield $calcPrice;
			yield $accountNumber;
			yield $cifNif;
			yield $paid;
			yield $bill->setTemplatePath('field/bill.html.twig');
		} else if ($pageName == Crud::PAGE_DETAIL) {
			yield $dataPanel;
			yield $professional;
			yield $date;
			yield $num;
			yield $price;
			yield $calcPrice;
			yield $accountNumber;
			yield $cifNifType;
			yield $cifNif;
			yield $address;
			yield $city;
			yield $cp;
			yield $bill;
			yield $paid;
		} else if ($pageName == Crud::PAGE_NEW) {
			yield $dataPanel;
			yield $professional;
			yield $month;
			yield $year;
			yield $num;
			yield $price;
			yield $accountNumber;
			yield $cifNifType;
			yield $cifNif;
			yield $address;
			yield $city;
			yield $cp;
			yield $bill;
			yield $paid;
		} else if ($pageName == Crud::PAGE_EDIT) {
			yield $dataPanel;
			yield $professional;
			yield $month;
			yield $year;
			yield $num;
			yield $price;
			yield $accountNumber;
			yield $cifNifType;
			yield $cifNif;
			yield $address;
			yield $city;
			yield $cp;
			yield $bill;
			yield $paid;
		}
	}

	public function configureActions(Actions $actions): Actions
	{
		if (!$this->getUser()->hasPermission('entityBilling')) {
			$actions = Actions::new();
		}

		$actions->add(Crud::PAGE_INDEX, Action::new('export', $this->translator->trans('ea.actions.downloadAsCSV'))
			->setIcon('icon ti ti-download')
			->linkToCrudAction('exportAction')
			->createAsGlobalAction()
		);

		return $actions;
	}

	public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
	{
		$response = $this->container->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters)
			->andWhere("entity.paid = true");

		return $response;
	}

	public function exportAction(Request $request)
	{
		$context = $request->attributes->get(EA::CONTEXT_REQUEST_ATTRIBUTE);
		$fields = array(
			FieldGenerator::text('professional.name')->setLabel(strtoupper($this->translator->trans('entities.user.fields.name'))),
			FieldGenerator::text('professional.lastname')->setLabel(strtoupper($this->translator->trans('entities.user.fields.lastname'))),
			FieldGenerator::text('cifNifType')->setLabel(strtoupper($this->translator->trans('entities.billing.fields.cifNifType'))),
			FieldGenerator::text('cifNif')->setLabel(strtoupper($this->translator->trans('entities.billing.fields.cifNif'))),
			FieldGenerator::text('address')->setLabel(strtoupper($this->translator->trans('entities.billing.fields.address'))),
			FieldGenerator::text('city')->setLabel(strtoupper($this->translator->trans('entities.billing.fields.city'))),
			FieldGenerator::text('cp')->setLabel(strtoupper($this->translator->trans('entities.billing.fields.cp'))),
			FieldGenerator::text('tipoDocumento')->setLabel(strtoupper($this->translator->trans('entities.billing.fields.tipoDocumento'))),
			FieldGenerator::text('num')->setLabel(strtoupper($this->translator->trans('entities.billing.fields.num'))),
			FieldGenerator::text('formaPago')->setLabel(strtoupper($this->translator->trans('entities.billing.fields.formaPago'))),
			FieldGenerator::text('accountNumber')->setLabel(strtoupper($this->translator->trans('entities.billing.fields.iban'))),
			FieldGenerator::text('price')->setLabel(strtoupper($this->translator->trans('entities.billing.fields.price'))),
			FieldGenerator::text('fechaEmision')->setLabel(strtoupper($this->translator->trans('entities.billing.fields.fechaEmision'))),
			FieldGenerator::text('fechaVencimiento')->setLabel(strtoupper($this->translator->trans('entities.billing.fields.fechaVencimiento'))),
			FieldGenerator::text('professional.email')->setLabel(strtoupper($this->translator->trans('entities.user.fields.email'))),
		);
		$fields = FieldCollection::new($fields);
		$filters = $this->container->get(FilterFactory::class)->create($context->getCrud()->getFiltersConfig(), $fields, $context->getEntity());
		$entities = $this->createIndexQueryBuilder($context->getSearch(), $context->getEntity(), $fields, $filters)->getQuery()->getResult();
		$data = $this->csvService->getEntityAsData($entities, $fields, true);
		$entityName = $this->translator->trans('entities.billing.plural');
		return $this->csvService->export($data, $entityName . ' - ' . date_create()->format('Y-m-d_H-i-s') . '.csv');
	}
}
