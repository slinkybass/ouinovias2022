<?php

namespace App\Controller\Admin\Cruds;

use App\Entity\DisponibilityTesting;
use App\Field\FieldGenerator;
use App\Service\CsvService;

use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Option\EA;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Factory\FilterFactory;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

class DisponibilityTestingCrudController extends AbstractCrudController
{
	private $em;
	private $translator;
	private $adminUrlGenerator;
	private $csvService;

	public function __construct(EntityManagerInterface $em, TranslatorInterface $translator, AdminUrlGenerator $adminUrlGenerator, CsvService $csvService)
	{
		$this->em = $em;
		$this->translator = $translator;
		$this->adminUrlGenerator = $adminUrlGenerator;
		$this->csvService = $csvService;
	}

	public static function getEntityFqcn(): string
	{
		return DisponibilityTesting::class;
	}

	public function configureCrud(Crud $crud): Crud
	{
		$crud->setEntityLabelInSingular($this->translator->trans('entities.disponibilityTesting.singular'));
		$crud->setEntityLabelInPlural($this->translator->trans('entities.disponibilityTesting.plural'));
		$crud->setDefaultSort(['date' => 'ASC']);

		$entityId = filter_input(INPUT_GET, EA::ENTITY_ID, FILTER_SANITIZE_URL);
		$entity = $entityId ? $this->em->getRepository($this->getEntityFqcn())->find($entityId) : null;
		if ($entity) {
			$crud->setPageTitle(Crud::PAGE_DETAIL, $this->translator->trans('entities.disponibilityTesting.singular') . ': ' . $entity);
			$crud->setPageTitle(Crud::PAGE_EDIT, $this->translator->trans('ea.titles.edit', [
				'%entity_label_singular%' => $this->translator->trans('entities.disponibilityTesting.singular') . ': ' . $entity
			]));
		}

		return $crud;
	}

	public function configureFields(string $pageName): iterable
	{
		$dataPanel = FieldGenerator::panel($this->translator->trans('entities.disponibilityTesting.sections.data'))
			->setIcon('fas fa-fw fa-calendar-day');
		$professional = FieldGenerator::association('professional')
			->setLabel($this->translator->trans('entities.professional.singular'))
			->setCrudController(ProfessionalCrudController::class)
			->setQueryBuilder(function ($queryBuilder) {
				return $queryBuilder
					->leftJoin('entity.role', 'r')
					->andWhere("r.name = 'ROLE_PROFESSIONAL'")
					->andWhere("entity.active = true");
			});
		$date = FieldGenerator::date('date')
			->setLabel($this->translator->trans('entities.disponibilityTesting.fields.date'))
			->setFormTypeOption('attr.data-min-date', 'today')
			->setColumns(6);
		$time = FieldGenerator::choice('time')
			->setLabel($this->translator->trans('entities.disponibilityTesting.fields.time'))
			->setChoices([
				$this->translator->trans('entities.disponibilityTesting.fields.times.1') => 1,
				$this->translator->trans('entities.disponibilityTesting.fields.times.2') => 2
			])->setColumns(6);
		$blocked = FieldGenerator::switch('blocked')
			->setLabel($this->translator->trans('entities.disponibilityTesting.fields.blocked'))
			->setColumns(12);

		if ($pageName == Crud::PAGE_INDEX) {
			yield $professional;
			yield $date;
			yield $time;
			yield $blocked->renderAsSwitch(false);
		} else if ($pageName == Crud::PAGE_DETAIL) {
			yield $dataPanel;
			yield $professional;
			yield $date;
			yield $time;
			yield $blocked;
		} else if ($pageName == Crud::PAGE_NEW) {
			yield $dataPanel;
			if ($this->isGranted('ROLE_PROFESSIONAL')) {
				$professional->setFormTypeOption('data', $this->getUser())->setColumns('d-none');
			}
			yield $professional;
			yield $date;
			yield $time;
		} else if ($pageName == Crud::PAGE_EDIT) {
			yield $dataPanel;
			if (!$this->isGranted('ROLE_PROFESSIONAL')) {
				yield $professional;
			}
			yield $date;
			yield $time;
			if (!$this->isGranted('ROLE_PROFESSIONAL')) {
				yield $blocked;
			}
		}
	}

	public function configureActions(Actions $actions): Actions
	{
		if (!$this->getUser()->hasPermission('entityDisponibilityTesting')) {
			$actions = Actions::new();
		}

		$actions->add(Crud::PAGE_INDEX, Action::new('export', $this->translator->trans('ea.actions.downloadAsCSV'))
			->setIcon('icon ti ti-download')
			->linkToCrudAction('exportAction')
			->createAsGlobalAction()
		);

		return $actions;
	}

	public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
	{
		$response = $this->container->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters);
		if ($this->isGranted('ROLE_PROFESSIONAL')) {
			$response->where('entity.professional = :professional')->setParameter('professional', $this->getUser());
		}
		$now = new \Datetime('now');
		$response->andWhere('entity.date >= :now')->setParameter('now', $now->setTime(0, 0));

		return $response;
	}

	public function exportAction(Request $request)
	{
		$context = $request->attributes->get(EA::CONTEXT_REQUEST_ATTRIBUTE);
		$fields = array();
		$entity = $this->em->getRepository($this->getEntityFqcn())->findOneBy(array(), array('id' => 'DESC'));
		if ($entity) {
			$arrEntity = (array) $entity; 
			foreach ($arrEntity as $k => $v) {
				$fields[] = preg_replace('/[\x00-\x1F\x7F]/u', '', str_replace($this->getEntityFqcn(), '', $k));
			}
		}
		$fields = FieldCollection::new($fields);
		$filters = $this->container->get(FilterFactory::class)->create($context->getCrud()->getFiltersConfig(), $fields, $context->getEntity());
		$entities = $this->createIndexQueryBuilder($context->getSearch(), $context->getEntity(), $fields, $filters)->getQuery()->getResult();
		$data = $this->csvService->getEntityAsData($entities, $fields);
		$entityName = $this->translator->trans('entities.disponibilityTesting.plural');
		return $this->csvService->export($data, $entityName . ' - ' . date_create()->format('Y-m-d_H-i-s') . '.csv');
	}
}
