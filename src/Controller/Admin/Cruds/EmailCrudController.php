<?php

namespace App\Controller\Admin\Cruds;

use App\Entity\Email;
use App\Field\FieldGenerator;
use App\Service\CsvService;

use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Option\EA;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Factory\FilterFactory;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

use Doctrine\ORM\EntityManagerInterface;

class EmailCrudController extends AbstractCrudController
{
	private $em;
	private $translator;
	private $adminUrlGenerator;
	private $csvService;

	public function __construct(EntityManagerInterface $em, TranslatorInterface $translator, AdminUrlGenerator $adminUrlGenerator, CsvService $csvService)
	{
		$this->em = $em;
		$this->translator = $translator;
		$this->adminUrlGenerator = $adminUrlGenerator;
		$this->csvService = $csvService;
	}

	public static function getEntityFqcn(): string
	{
		return Email::class;
	}

	public function configureCrud(Crud $crud): Crud
	{
		$crud->setEntityLabelInSingular($this->translator->trans('entities.email.singular'));
		$crud->setEntityLabelInPlural($this->translator->trans('entities.email.plural'));
		$crud->setDefaultSort(['creationDate' => 'DESC']);

		$entityId = filter_input(INPUT_GET, EA::ENTITY_ID, FILTER_SANITIZE_URL);
		$entity = $entityId ? $this->em->getRepository($this->getEntityFqcn())->find($entityId) : null;
		if ($entity) {
			$crud->setPageTitle(Crud::PAGE_DETAIL, $this->translator->trans('entities.email.singular') . ': ' . $entity);
			$crud->setPageTitle(Crud::PAGE_EDIT, $this->translator->trans('ea.titles.edit', [
				'%entity_label_singular%' => $this->translator->trans('entities.email.singular') . ': ' . $entity
			]));
		}

		return $crud;
	}

	public function configureFields(string $pageName): iterable
	{
		$dataPanel = FieldGenerator::panel($this->translator->trans('entities.email.sections.data'))
			->setIcon('fas fa-fw fa-envelope');
		$email = FieldGenerator::text('email')
			->setLabel($this->translator->trans('entities.email.fields.email'))
			->setColumns(12);
		$title = FieldGenerator::text('title')
			->setLabel($this->translator->trans('entities.email.fields.title'))
			->setColumns(12);
		$content = FieldGenerator::texteditor('content')
			->setLabel($this->translator->trans('entities.email.fields.content'))
			->setColumns(12);
		$creationDate = FieldGenerator::datetime('creationDate')
			->setLabel($this->translator->trans('entities.email.fields.creationDate'))
			->setColumns(12);
		$sended = FieldGenerator::switch('sended')
			->setLabel($this->translator->trans('entities.email.fields.sended'))
			->setColumns(12);
		$msg = FieldGenerator::texteditor('msg')
			->setLabel($this->translator->trans('entities.email.fields.msg'))
			->setColumns(12);

		if ($pageName == Crud::PAGE_INDEX) {
			yield $title;
			yield $creationDate;
			yield $sended;
		} else if ($pageName == Crud::PAGE_DETAIL) {
			yield $dataPanel;
			yield $email;
			yield $title;
			yield $content;
			yield $creationDate;
			yield $sended;
			yield $msg;
		} else if ($pageName == Crud::PAGE_NEW) {
			yield $dataPanel;
			yield $email;
			yield $title;
			yield $content;
			yield $creationDate;
			yield $sended;
			yield $msg;
		} else if ($pageName == Crud::PAGE_EDIT) {
			yield $dataPanel;
			yield $email;
			yield $title;
			yield $content;
			yield $creationDate;
			yield $sended;
			yield $msg;
		}
	}

	public function configureActions(Actions $actions): Actions
	{
		if (!$this->getUser()->hasPermission('entityEmail')) {
			$actions = Actions::new();
		} else {
			$actions->remove(Crud::PAGE_INDEX, Action::NEW);
			$actions->remove(Crud::PAGE_INDEX, Action::EDIT);
			$actions->remove(Crud::PAGE_DETAIL, Action::DELETE);
			$actions->remove(Crud::PAGE_DETAIL, Action::EDIT);
		}

		$actions->add(Crud::PAGE_INDEX, Action::new('export', $this->translator->trans('ea.actions.downloadAsCSV'))
			->setIcon('icon ti ti-download')
			->linkToCrudAction('exportAction')
			->createAsGlobalAction()
		);

		return $actions;
	}

	public function exportAction(Request $request)
	{
		$context = $request->attributes->get(EA::CONTEXT_REQUEST_ATTRIBUTE);
		$fields = array();
		$entity = $this->em->getRepository($this->getEntityFqcn())->findOneBy(array(), array('id' => 'DESC'));
		if ($entity) {
			$arrEntity = (array) $entity; 
			foreach ($arrEntity as $k => $v) {
				$fields[] = preg_replace('/[\x00-\x1F\x7F]/u', '', str_replace($this->getEntityFqcn(), '', $k));
			}
		}
		$fields = FieldCollection::new($fields);
		$filters = $this->container->get(FilterFactory::class)->create($context->getCrud()->getFiltersConfig(), $fields, $context->getEntity());
		$entities = $this->createIndexQueryBuilder($context->getSearch(), $context->getEntity(), $fields, $filters)->getQuery()->getResult();
		$data = $this->csvService->getEntityAsData($entities, $fields);
		$entityName = $this->translator->trans('entities.email.plural');
		return $this->csvService->export($data, $entityName . ' - ' . date_create()->format('Y-m-d_H-i-s') . '.csv');
	}
}
