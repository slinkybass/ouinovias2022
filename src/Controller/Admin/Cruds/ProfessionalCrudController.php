<?php

namespace App\Controller\Admin\Cruds;

use App\Entity\User;
use App\Field\FieldGenerator;
use App\Service\CsvService;

use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Config\Option\EA;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Factory\FilterFactory;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

class ProfessionalCrudController extends AbstractCrudController
{
	private $em;
	private $translator;
	private $adminUrlGenerator;
	private $csvService;
	private $passwordHasher;

	public function __construct(EntityManagerInterface $em, TranslatorInterface $translator, AdminUrlGenerator $adminUrlGenerator, CsvService $csvService, UserPasswordHasherInterface $passwordHasher)
	{
		$this->em = $em;
		$this->translator = $translator;
		$this->adminUrlGenerator = $adminUrlGenerator;
		$this->csvService = $csvService;
		$this->passwordHasher = $passwordHasher;
	}

	public static function getEntityFqcn(): string
	{
		return User::class;
	}

	public function configureCrud(Crud $crud): Crud
	{
		$crud->setEntityLabelInSingular($this->translator->trans('entities.professional.singular'));
		$crud->setEntityLabelInPlural($this->translator->trans('entities.professional.plural'));
		$crud->setDefaultSort(['name' => 'ASC', 'lastname' => 'ASC']);

		$entityId = filter_input(INPUT_GET, EA::ENTITY_ID, FILTER_SANITIZE_URL);
		$entity = $entityId ? $this->em->getRepository($this->getEntityFqcn())->find($entityId) : null;
		if ($entity) {
			$crud->setPageTitle(Crud::PAGE_DETAIL, $this->translator->trans('entities.professional.singular') . ': ' . $entity);
			$crud->setPageTitle(Crud::PAGE_EDIT, $this->translator->trans('ea.titles.edit', [
				'%entity_label_singular%' => $this->translator->trans('entities.professional.singular') . ': ' . $entity
			]));
		}

		return $crud;
	}

	public function configureFields(string $pageName): iterable
	{
		$session = $this->container->get('request_stack')->getSession();

		$dataPanel = FieldGenerator::panel($this->translator->trans('entities.professional.sections.data'))
			->setIcon('fas fa-fw fa-user-tie');
		$username = FieldGenerator::text('username')
			->setLabel($this->translator->trans('entities.user.fields.username'));
		$name = FieldGenerator::text('name')
			->setLabel($this->translator->trans('entities.user.fields.name'))
			->setColumns(6);
		$lastname = FieldGenerator::text('lastname')
			->setLabel($this->translator->trans('entities.user.fields.lastname'))
			->setColumns(6);
		$fullname = FieldGenerator::text('fullname')
			->setLabel($this->translator->trans('entities.user.fields.fullname'));
		$tel1 = FieldGenerator::phone('tel1')
			->setLabel($this->translator->trans('entities.user.fields.tel1'))
			->setColumns(6);
		$tel2 = FieldGenerator::phone('tel2')
			->setLabel($this->translator->trans('entities.user.fields.tel2'))
			->setColumns(6);
		$email = FieldGenerator::email('email')
			->setLabel($this->translator->trans('entities.user.fields.email'));
		$role = FieldGenerator::association('role')
			->setLabel($this->translator->trans('entities.role.singular'))
			->setQueryBuilder(function ($queryBuilder) {
				return $queryBuilder
					->andWhere("entity.name = 'ROLE_PROFESSIONAL'");
			})
			->setRequired(true);

		$dataExtraPanel = FieldGenerator::panel($this->translator->trans('entities.professional.sections.dataExtra'))
			->setIcon('fas fa-fw fa-user-tie');
		$professionalRate = FieldGenerator::association('professionalRate')
			->setLabel($this->translator->trans('entities.rate.singular'));
		$professionalURL = FieldGenerator::url('professionalURL')
			->setLabel($this->translator->trans('entities.user.fields.professionalURL'));
		$professionalDNI = FieldGenerator::text('professionalDNI')
			->setLabel($this->translator->trans('entities.user.fields.professionalDNI'))
			->setColumns(6);
		$professionalAddress = FieldGenerator::text('professionalAddress')
			->setLabel($this->translator->trans('entities.user.fields.professionalAddress'))
			->setColumns(6);
		$professionalContract = FieldGenerator::media('professionalContract')
			->setLabel($this->translator->trans('entities.user.fields.professionalContract'))
			->setFormTypeOption('attr.data-type', 'public_contratosProfesionales');
		$professionalContractDate = FieldGenerator::datetime('professionalContractDate')
			->setLabel($this->translator->trans('entities.user.fields.professionalContractDate'))
			->setFormTypeOption('attr.data-type', 'public_contratosProfesionales');
		$professionalExclusive = FieldGenerator::switch('professionalExclusive')
			->setLabel($this->translator->trans('entities.user.fields.professionalExclusive'))
			->setColumns(6);
		$active = FieldGenerator::switch('active')
			->setLabel($this->translator->trans('entities.user.fields.active'))
			->setColumns(6);
		$professionalType = FieldGenerator::choice('professionalType')
			->setLabel($this->translator->trans('entities.user.fields.professionalType'))
			->setChoices([
				$this->translator->trans('entities.user.fields.professionalTypes.1') => 1,
				$this->translator->trans('entities.user.fields.professionalTypes.2') => 2,
				$this->translator->trans('entities.user.fields.professionalTypes.3') => 3
			])->setColumns(4);
		$professionalProvinces = FieldGenerator::association('professionalProvinces')
			->setLabel($this->translator->trans('entities.province.plural'))
			->setColumns(4);
		$professionalIrpf = FieldGenerator::integer('professionalIrpf')
			->setLabel($this->translator->trans('entities.user.fields.professionalIrpf'))
			->setFormTypeOption('attr.min', 0)
			->setColumns(4);

		$passwordPanel = FieldGenerator::panel($this->translator->trans('entities.user.sections.password'))
			->setIcon('fas fa-fw fa-key');
		$password = FieldGenerator::repeat('plainPassword')
			->setFormTypeOptions([
				'type' => PasswordType::class,
				'first_options' => [
					'label' => $this->translator->trans('entities.user.fields.password')
				],
				'second_options' => [
					'label' => $this->translator->trans('entities.user.fields.repeatPassword')
				]
			]);


		$billingsPanel = FieldGenerator::panel($this->translator->trans('entities.billing.plural'))
			->setIcon('fas fa-fw fa-file-invoice-dollar');
		$professionalBillings = FieldGenerator::association('professionalBillings')
			->setLabel($this->translator->trans('entities.billing.plural'))
			->setTemplatePath('field/extendedMany.html.twig');

		if ($pageName == Crud::PAGE_INDEX) {
			if ($session->get('config')->enableUsername) {
				yield $username;
			}
			yield $fullname;
			yield $email;
			yield $active->renderAsSwitch(false);
		} else if ($pageName == Crud::PAGE_DETAIL) {
			yield $dataPanel;
			if ($session->get('config')->enableUsername) {
				yield $username;
			}
			yield $fullname;
			yield $tel1;
			yield $tel2;
			yield $email;
			yield $dataExtraPanel;
			yield $professionalRate;
			yield $professionalURL;
			yield $professionalDNI;
			yield $professionalAddress;
			yield $professionalContract;
			yield $professionalContractDate;
			yield $professionalExclusive;
			yield $active;
			yield $professionalType;
			yield $professionalProvinces->setTemplatePath('field/provinces.html.twig');
			yield $professionalIrpf;
			yield $billingsPanel;
			yield $professionalBillings;
		} else if ($pageName == Crud::PAGE_NEW) {
			yield $dataPanel;
			if ($session->get('config')->enableUsername) {
				yield $username;
			}
			yield $name;
			yield $lastname;
			yield $tel1;
			yield $tel2;
			yield $email;
			yield $role;
			yield $dataExtraPanel;
			yield $professionalRate;
			yield $professionalURL;
			yield $professionalDNI;
			yield $professionalAddress;
			yield $professionalContract;
			yield $professionalContractDate;
			yield $professionalExclusive->setColumns(12);
			yield $professionalType;
			yield $professionalProvinces;
			yield $professionalIrpf;
			yield $passwordPanel;
			yield $password->setRequired(true);
		} else if ($pageName == Crud::PAGE_EDIT) {
			yield $dataPanel;
			if ($session->get('config')->enableUsername) {
				yield $username;
			}
			yield $name;
			yield $lastname;
			yield $tel1;
			yield $tel2;
			yield $email;
			yield $dataExtraPanel;
			if (!$this->isGranted('ROLE_PROFESSIONAL')) {
				yield $professionalRate;
				yield $professionalURL;
			}
			yield $professionalDNI;
			yield $professionalAddress;
			if (!$this->isGranted('ROLE_PROFESSIONAL')) {
				yield $professionalContract;
				yield $professionalContractDate;
				yield $professionalExclusive;
				yield $active;
				yield $professionalType;
				yield $professionalProvinces;
			}
			yield $professionalIrpf;
			yield $passwordPanel;
			yield $password->setRequired(false);
		}
	}

	public function configureActions(Actions $actions): Actions
	{
		if (!$this->getUser()->hasPermission('entityProfessional')) {
			$crudAction = filter_input(INPUT_GET, EA::CRUD_ACTION, FILTER_SANITIZE_URL);
			$adminId = filter_input(INPUT_GET, EA::ENTITY_ID, FILTER_SANITIZE_URL);
			if (($crudAction == Crud::PAGE_DETAIL || $crudAction == Crud::PAGE_EDIT) && $this->getUser()->getId() == $adminId) {
				$actions->remove(Crud::PAGE_DETAIL, Action::INDEX);
				$actions->remove(Crud::PAGE_DETAIL, Action::DELETE);
				$actions->remove(Crud::PAGE_EDIT, Action::INDEX);
				$actions->remove(Crud::PAGE_EDIT, Action::DELETE);
			} else {
				$actions = Actions::new();
			}
		} else {
			$user = $this->getUser();

			$actions->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) use ($user) {
				return $action->displayIf(static function ($entity) use ($user) {
					return !$entity->getRole()->getIsAdmin() || ($entity->getRole()->getIsAdmin() && $user->getRole()->isUp($entity->getRole()));
				});
			});
			$actions->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) use ($user) {
				return $action->displayIf(static function ($entity) use ($user) {
					return !$entity->getRole()->getIsAdmin() || ($entity->getRole()->getIsAdmin() && $user->getRole()->isUp($entity->getRole()));
				});
			});
			$actions->update(Crud::PAGE_DETAIL, Action::EDIT, function (Action $action) use ($user) {
				return $action->displayIf(static function ($entity) use ($user) {
					return !$entity->getRole()->getIsAdmin() || ($entity->getRole()->getIsAdmin() && $user->getRole()->isUp($entity->getRole()));
				});
			});
			$actions->update(Crud::PAGE_DETAIL, Action::DELETE, function (Action $action) use ($user) {
				return $action->displayIf(static function ($entity) use ($user) {
					return !$entity->getRole()->getIsAdmin() || ($entity->getRole()->getIsAdmin() && $user->getRole()->isUp($entity->getRole()));
				});
			});
			$actions->update(Crud::PAGE_EDIT, Action::DELETE, function (Action $action) use ($user) {
				return $action->displayIf(static function ($entity) use ($user) {
					return !$entity->getRole()->getIsAdmin() || ($entity->getRole()->getIsAdmin() && $user->getRole()->isUp($entity->getRole()));
				});
			});

			$contratoList = Action::new('contratoList', $this->translator->trans('ea.actions.contrato'), 'icon ti ti-file-text')
				->linkToCrudAction('contratoAction');
			$contratoDetail = Action::new('contratoDetail', $this->translator->trans('ea.actions.contrato'), 'icon ti ti-file-text')
				->linkToCrudAction('contratoAction')
				->addCssClass('btn btn-outline-secondary');

			$actions->add(Crud::PAGE_INDEX, $contratoList);
			$actions->add(Crud::PAGE_DETAIL, $contratoDetail);
			$actions->add(Crud::PAGE_EDIT, $contratoDetail);

			$actions->update(Crud::PAGE_INDEX, 'contratoList', function (Action $action) {
				return $action->displayIf(static function ($entity) {
					return $entity->getProfessionalContract();
				});
			});
			$actions->update(Crud::PAGE_DETAIL, 'contratoDetail', function (Action $action) {
				return $action->displayIf(static function ($entity) {
					return $entity->getProfessionalContract();
				});
			});
			$actions->update(Crud::PAGE_EDIT, 'contratoDetail', function (Action $action) {
				return $action->displayIf(static function ($entity) {
					return $entity->getProfessionalContract();
				});
			});

			$actions->reorder(Crud::PAGE_INDEX, ['contratoList', Action::DETAIL, Action::EDIT, Action::DELETE]);
			$actions->reorder(Crud::PAGE_DETAIL, [Action::INDEX, Action::DELETE, 'contratoDetail', Action::EDIT]);
			$actions->reorder(Crud::PAGE_EDIT, [Action::INDEX, Action::DELETE, 'contratoDetail', Action::SAVE_AND_RETURN]);
		}

		$actions->add(Crud::PAGE_INDEX, Action::new('export', $this->translator->trans('ea.actions.downloadAsCSV'))
			->setIcon('icon ti ti-download')
			->linkToCrudAction('exportAction')
			->createAsGlobalAction()
		);

		return $actions;
	}

	public function edit(\EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext $context)
	{
		$redirect = parent::edit($context);
		if ($redirect instanceof \Symfony\Component\HttpFoundation\RedirectResponse) {
			if (!$this->getUser()->hasPermission('entityProfessional')) {
				$url = $this->adminUrlGenerator
					->setRoute('admin_home')
					->generateUrl();
				return $this->redirect($url);
			}
		}
		return $redirect;
	}

	public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
	{
		$response = $this->container->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters)
			->leftJoin('entity.role', 'r')
			->andWhere("entity.active = true")
			->andWhere("r.name = 'ROLE_PROFESSIONAL'");

		return $response;
	}

	public function createEditFormBuilder(EntityDto $entityDto, KeyValueStore $keyValueStore, AdminContext $context): FormBuilderInterface
	{
		$session = $this->container->get('request_stack')->getSession();

		$formBuilder = parent::createEditFormBuilder($entityDto, $keyValueStore, $context);
		$this->addEncodePasswordEventListener($formBuilder);
		if (!$session->get('config')->enableUsername) {
			$this->addUsernameEventListener($formBuilder);
		}

		return $formBuilder;
	}

	public function createNewFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
	{
		$session = $this->container->get('request_stack')->getSession();

		$formBuilder = parent::createNewFormBuilder($entityDto, $formOptions, $context);
		$this->addEncodePasswordEventListener($formBuilder);
		if (!$session->get('config')->enableUsername) {
			$this->addUsernameEventListener($formBuilder);
		}

		return $formBuilder;
	}

	public function addEncodePasswordEventListener(FormBuilderInterface $formBuilder)
	{
		$formBuilder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
			$user = $event->getData();
			if ($user->getPlainPassword()) {
				$user->setPassword($this->passwordHasher->hashPassword($user, $user->getPlainPassword()));
			}
		});
	}

	public function addUsernameEventListener(FormBuilderInterface $formBuilder)
	{
		$formBuilder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
			$user = $event->getData();
			$user->setUsername($user->getEmail());
		});
	}

	public function exportAction(Request $request)
	{
		$context = $request->attributes->get(EA::CONTEXT_REQUEST_ATTRIBUTE);
		$fields = array();
		$entity = $this->em->getRepository($this->getEntityFqcn())->findOneBy(array(), array('id' => 'DESC'));
		if ($entity) {
			$arrEntity = (array) $entity; 
			foreach ($arrEntity as $k => $v) {
				$fields[] = preg_replace('/[\x00-\x1F\x7F]/u', '', str_replace($this->getEntityFqcn(), '', $k));
			}
		}
		$fields = FieldCollection::new($fields);
		$filters = $this->container->get(FilterFactory::class)->create($context->getCrud()->getFiltersConfig(), $fields, $context->getEntity());
		$entities = $this->createIndexQueryBuilder($context->getSearch(), $context->getEntity(), $fields, $filters)->getQuery()->getResult();
		$data = $this->csvService->getEntityAsData($entities, $fields);
		$entityName = $this->translator->trans('entities.professional.plural');
		return $this->csvService->export($data, $entityName . ' - ' . date_create()->format('Y-m-d_H-i-s') . '.csv');
	}

	public function contratoAction(Request $request)
	{
		$professional = $this->em->getRepository($this->getEntityFqcn())->find($request->get('entityId'));
		$userId = base64_encode($professional->getId());

		return $this->redirectToRoute('public_contract', ['id' => $userId]);
	}
}
