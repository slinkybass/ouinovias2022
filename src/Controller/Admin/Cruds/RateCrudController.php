<?php

namespace App\Controller\Admin\Cruds;

use App\Entity\Rate;
use App\Entity\Service;
use App\Entity\RateService;
use App\Field\FieldGenerator;
use App\Form\FormGenerator;
use App\Service\CsvService;

use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Option\EA;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Factory\FilterFactory;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

use Doctrine\ORM\EntityManagerInterface;

class RateCrudController extends AbstractCrudController
{
	private $em;
	private $translator;
	private $adminUrlGenerator;
	private $csvService;

	public function __construct(EntityManagerInterface $em, TranslatorInterface $translator, AdminUrlGenerator $adminUrlGenerator, CsvService $csvService)
	{
		$this->em = $em;
		$this->translator = $translator;
		$this->adminUrlGenerator = $adminUrlGenerator;
		$this->csvService = $csvService;
	}

	public static function getEntityFqcn(): string
	{
		return Rate::class;
	}

	public function configureCrud(Crud $crud): Crud
	{
		$crud->setEntityLabelInSingular($this->translator->trans('entities.rate.singular'));
		$crud->setEntityLabelInPlural($this->translator->trans('entities.rate.plural'));
		$crud->setDefaultSort(['name' => 'ASC']);

		$entityId = filter_input(INPUT_GET, EA::ENTITY_ID, FILTER_SANITIZE_URL);
		$entity = $entityId ? $this->em->getRepository($this->getEntityFqcn())->find($entityId) : null;
		if ($entity) {
			$crud->setPageTitle(Crud::PAGE_DETAIL, $this->translator->trans('entities.rate.singular') . ': ' . $entity);
			$crud->setPageTitle(Crud::PAGE_EDIT, $this->translator->trans('ea.titles.edit', [
				'%entity_label_singular%' => $this->translator->trans('entities.rate.singular') . ': ' . $entity
			]));
		}

		return $crud;
	}

	public function configureFields(string $pageName): iterable
	{
		$dataPanel = FieldGenerator::panel($this->translator->trans('entities.rate.sections.data'))
			->setIcon('fas fa-fw fa-dollar-sign');
		$name = FieldGenerator::text('name')
			->setLabel($this->translator->trans('entities.rate.fields.name'));
		$url = FieldGenerator::url('url')
			->setLabel($this->translator->trans('entities.rate.fields.url'));
		$rateServices = FieldGenerator::association('rateServices')
			->setLabel($this->translator->trans('entities.service.plural'))
			->setTemplatePath('field/rateServices.html.twig');
		$professionals = FieldGenerator::association('professionals')
			->setLabel($this->translator->trans('entities.professional.plural'))
			->setCrudController(ProfessionalCrudController::class)
			->setTemplatePath('field/professional.html.twig');

		if ($pageName == Crud::PAGE_INDEX) {
			yield $name;
		} else if ($pageName == Crud::PAGE_DETAIL) {
			yield $dataPanel;
			yield $name;
			yield $url;
			yield $rateServices;
			yield $professionals;
		} else if ($pageName == Crud::PAGE_NEW) {
			yield $dataPanel;
			yield $name;
			yield $url;
		} else if ($pageName == Crud::PAGE_EDIT) {
			yield $dataPanel;
			yield $name;
			yield $url;
		}
	}

	public function configureActions(Actions $actions): Actions
	{
		if (!$this->getUser()->hasPermission('entityRate')) {
			$actions = Actions::new();
		} else {
			$rateServicesList = Action::new('rateServicesList', $this->translator->trans('ea.actions.rateServices'))
				->setIcon('icon ti ti-currency-dollar')
				->linkToCrudAction('rateServicesAction');
			$rateServicesDetail = Action::new('rateServicesDetail', $this->translator->trans('ea.actions.rateServices'))
				->setIcon('icon ti ti-currency-dollar')
				->linkToCrudAction('rateServicesAction');
			$duplicateList = Action::new('duplicateList', $this->translator->trans('ea.actions.duplicate'))
				->setIcon('icon ti ti-copy')
				->linkToCrudAction('duplicateAction');
			$duplicateDetail = Action::new('duplicateDetail', $this->translator->trans('ea.actions.duplicate'))
				->setIcon('icon ti ti-copy')
				->linkToCrudAction('duplicateAction');
			$actions->add(Crud::PAGE_INDEX, $rateServicesList);
			$actions->add(Crud::PAGE_DETAIL, $rateServicesDetail);
			$actions->add(Crud::PAGE_EDIT, $rateServicesDetail);
			$actions->add(Crud::PAGE_INDEX, $duplicateList);
			$actions->add(Crud::PAGE_DETAIL, $duplicateDetail);
			$actions->add(Crud::PAGE_EDIT, $duplicateDetail);
			$actions->reorder(Crud::PAGE_INDEX, ['duplicateList', 'rateServicesList', Action::DETAIL, Action::EDIT, Action::DELETE]);
			$actions->reorder(Crud::PAGE_DETAIL, [Action::INDEX, Action::DELETE, 'duplicateDetail', 'rateServicesDetail', Action::EDIT]);
			$actions->reorder(Crud::PAGE_EDIT, [Action::INDEX, Action::DELETE, 'duplicateDetail', 'rateServicesDetail', Action::SAVE_AND_RETURN]);
		}

		$actions->add(Crud::PAGE_INDEX, Action::new('export', $this->translator->trans('ea.actions.downloadAsCSV'))
			->setIcon('icon ti ti-download')
			->linkToCrudAction('exportAction')
			->createAsGlobalAction()
		);

		return $actions;
	}

	public function exportAction(Request $request)
	{
		$context = $request->attributes->get(EA::CONTEXT_REQUEST_ATTRIBUTE);
		$fields = array();
		$entity = $this->em->getRepository($this->getEntityFqcn())->findOneBy(array(), array('id' => 'DESC'));
		if ($entity) {
			$arrEntity = (array) $entity; 
			foreach ($arrEntity as $k => $v) {
				$fields[] = preg_replace('/[\x00-\x1F\x7F]/u', '', str_replace($this->getEntityFqcn(), '', $k));
			}
		}
		$fields = FieldCollection::new($fields);
		$filters = $this->container->get(FilterFactory::class)->create($context->getCrud()->getFiltersConfig(), $fields, $context->getEntity());
		$entities = $this->createIndexQueryBuilder($context->getSearch(), $context->getEntity(), $fields, $filters)->getQuery()->getResult();
		$data = $this->csvService->getEntityAsData($entities, $fields);
		$entityName = $this->translator->trans('entities.rate.plural');
		return $this->csvService->export($data, $entityName . ' - ' . date_create()->format('Y-m-d_H-i-s') . '.csv');
	}

	public function rateServicesAction(Request $request)
	{
		$context = $request->attributes->get(EA::CONTEXT_REQUEST_ATTRIBUTE);
		$rate = $context->getEntity()->getInstance();
		$services = $this->em->getRepository(Service::class)->findAll();
		$form = $this->getForm_rateServices($request);

		if ($request->isMethod('POST')) {
			$form->handleRequest($request);
			if ($form->isValid()) {
				foreach ($services as $service) {
					$rateService = $this->em->getRepository(RateService::class)->findOneBy([
						'rate' => $rate,
						'service' => $service
					]);
					if (!$rateService) {
						$rateService = new RateService();
						$rateService->setRate($rate);
						$rateService->setService($service);
					}
					$rateService->setPriceBooking($form['priceBooking-' . $service->getId()]->getData());
					$rateService->setPriceTesting($form['priceTesting-' . $service->getId()]->getData());
					$rateService->setPriceWedding($form['priceWedding-' . $service->getId()]->getData());
					$this->em->persist($rateService);
					$this->em->flush();
				}

				$url = $this->adminUrlGenerator->setAction(Crud::PAGE_INDEX)->setController(RateCrudController::class)->generateUrl();
				return $this->redirect($url);
			}
		}

		return $this->render('admin/rate_rateServicesAction.html.twig', [
			'rate' => $rate,
			'services' => $services,
			'form' => $form->createView()
		]);
	}

	public function duplicateAction(Request $request)
	{
		$context = $request->attributes->get(EA::CONTEXT_REQUEST_ATTRIBUTE);
		$rate = $context->getEntity()->getInstance();

		$newRate = new Rate();
		$newRate->setName($rate->getName() . " - copia");
		$newRate->setURL($rate->getURL());
		foreach ($rate->getRateServices() as $rateService) {
			$newRateService = new RateService();
			$newRateService->setRate($newRate);
			$newRateService->setService($rateService->getService());
			$newRateService->setPriceBooking($rateService->getPriceBooking());
			$newRateService->setPriceTesting($rateService->getPriceTesting());
			$newRateService->setPriceWedding($rateService->getPriceWedding());
			$this->em->persist($newRateService);
			$newRate->addRateService($newRateService);
		}
		$this->em->persist($newRate);
		$this->em->flush();

		$url = $this->adminUrlGenerator->setAction(Crud::PAGE_INDEX)->setController(RateCrudController::class)->generateUrl();
		return $this->redirect($url);
	}

	private function getForm_rateServices($request) {
		$form = $this->createFormBuilder();
		$services = $this->em->getRepository(Service::class)->findBy([], ['name' => 'ASC']);

		foreach ($services as $service) {
			$rateServices = $service->getRateServices();
			$priceBookingVal = null;
			$priceTestingVal = null;
			$priceWeddingVal = null;
			foreach ($rateServices as $rateService) {
				if ($rateService->getRate()->getId() == $request->get('entityId')) {
					$priceBookingVal = $rateService->getPriceBooking();
					$priceTestingVal = $rateService->getPriceTesting();
					$priceWeddingVal = $rateService->getPriceWedding();
				}
			}
			$priceBooking = FieldGenerator::float('priceBooking-' . $service->getID())
				->setLabel($this->translator->trans('entities.rateService.fields.priceBooking'))
				->setFormTypeOption('data', $priceBookingVal)
				->setRequired(true);
			$priceTesting = FieldGenerator::float('priceTesting-' . $service->getID())
				->setLabel($this->translator->trans('entities.rateService.fields.priceTesting'))
				->setFormTypeOption('data', $priceTestingVal)
				->setRequired(true);
			$priceWedding = FieldGenerator::float('priceWedding-' . $service->getID())
				->setLabel($this->translator->trans('entities.rateService.fields.priceWedding'))
				->setFormTypeOption('data', $priceWeddingVal)
				->setRequired(true);
			$form = FormGenerator::getFormBuilder($form, [
				$priceBooking,
				$priceTesting,
				$priceWedding,
			]);
			$form->add('save', SubmitType::class, [
				'label' => '<i class="icon ti ti-device-floppy"></i> <span class="action-label">' . $this->translator->trans('action.create', [], 'EasyAdminBundle') . '</span>',
				'label_html' => true,
				'attr' => [
					'class' => 'btn btn-success',
					'form' => 'rateServices-form'
				]
			]);
		}

		return $form->getForm();
	}
}
