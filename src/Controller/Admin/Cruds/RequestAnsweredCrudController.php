<?php

namespace App\Controller\Admin\Cruds;

use App\Entity\Request as EntityRequest;
use App\Entity\Email;
use App\Entity\Event;
use App\Entity\Role;
use App\Entity\TemplateEmail;
use App\Entity\User;
use App\Entity\Wedding;
use App\Field\FieldGenerator;
use App\Form\FormGenerator;
use App\Service\CsvService;

use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Option\EA;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Factory\FilterFactory;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Filter\TextFilter;
use EasyCorp\Bundle\EasyAdminBundle\Filter\DateTimeFilter;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

class RequestAnsweredCrudController extends AbstractCrudController
{
	private $em;
	private $translator;
	private $adminUrlGenerator;
	private $csvService;
	private $passwordHasher;

	public function __construct(EntityManagerInterface $em, TranslatorInterface $translator, AdminUrlGenerator $adminUrlGenerator, CsvService $csvService, UserPasswordHasherInterface $passwordHasher)
	{
		$this->em = $em;
		$this->translator = $translator;
		$this->adminUrlGenerator = $adminUrlGenerator;
		$this->csvService = $csvService;
		$this->passwordHasher = $passwordHasher;
	}

	public static function getEntityFqcn(): string
	{
		return EntityRequest::class;
	}

	public function configureCrud(Crud $crud): Crud
	{
		$crud->setEntityLabelInSingular($this->translator->trans('entities.requestAnswered.singular'));
		$crud->setEntityLabelInPlural($this->translator->trans('entities.requestAnswered.plural'));
		$crud->setDefaultSort(['creationDate' => 'DESC']);
		$crud->setSearchFields(['name', 'lastname', 'email', 'weddingProvince.name', 'weddingCity', 'weddingAddress']);

		$entityId = filter_input(INPUT_GET, EA::ENTITY_ID, FILTER_SANITIZE_URL);
		$entity = $entityId ? $this->em->getRepository($this->getEntityFqcn())->find($entityId) : null;
		if ($entity) {
			$crud->setPageTitle(Crud::PAGE_DETAIL, $this->translator->trans('entities.requestAnswered.singular') . ': ' . $entity);
			$crud->setPageTitle(Crud::PAGE_EDIT, $this->translator->trans('ea.titles.edit', [
				'%entity_label_singular%' => $this->translator->trans('entities.requestAnswered.singular') . ': ' . $entity
			]));
		}

		return $crud;
	}

	public function configureFields(string $pageName): iterable
	{
		$dataPanel = FieldGenerator::panel($this->translator->trans('entities.request.sections.data'))
			->setIcon('fas fa-fw fa-circle-question');
		$name = FieldGenerator::text('name')
			->setLabel($this->translator->trans('entities.request.fields.name'))
			->setColumns(6);
		$lastname = FieldGenerator::text('lastname')
			->setLabel($this->translator->trans('entities.request.fields.lastname'))
			->setColumns(6);
		$fullname = FieldGenerator::text('fullname')
			->setLabel($this->translator->trans('entities.request.fields.fullname'));
		$tel = FieldGenerator::phone('tel')
			->setLabel($this->translator->trans('entities.request.fields.tel'))
			->setColumns(4);
		$email = FieldGenerator::email('email')
			->setLabel($this->translator->trans('entities.request.fields.email'))
			->setColumns(4);
		$weddingDate = FieldGenerator::datetime('weddingDate')
			->setLabel($this->translator->trans('entities.request.fields.weddingDate'));
		$weddingProvince = FieldGenerator::association('weddingProvince')
			->setLabel($this->translator->trans('entities.province.singular'))
			->setColumns(3);
		$weddingCity = FieldGenerator::text('weddingCity')
			->setLabel($this->translator->trans('entities.request.fields.weddingCity'))
			->setColumns(3);
		$weddingAddress = FieldGenerator::text('weddingAddress')
			->setLabel($this->translator->trans('entities.request.fields.weddingAddress'))
			->setColumns(6);
		$weddingFulladdress = FieldGenerator::text('weddingFulladdress')
			->setLabel($this->translator->trans('entities.request.fields.weddingFulladdress'));
		$comments = FieldGenerator::textarea('comments')
			->setLabel($this->translator->trans('entities.request.fields.comments'));
		$answered = FieldGenerator::switch('answered')
			->setLabel($this->translator->trans('entities.request.fields.answered'));
		$answeredDate = FieldGenerator::datetime('answeredDate')
			->setLabel($this->translator->trans('entities.request.fields.answeredDate'));
		$validated = FieldGenerator::switch('validated')
			->setLabel($this->translator->trans('entities.request.fields.validated'));
		$validatedDate = FieldGenerator::datetime('validatedDate')
			->setLabel($this->translator->trans('entities.request.fields.validatedDate'));
		$pending = FieldGenerator::switch('pending')
			->setLabel($this->translator->trans('entities.request.fields.pending'));
		$accepted = FieldGenerator::switch('accepted')
			->setLabel($this->translator->trans('entities.request.fields.accepted'));
		$denyReason = FieldGenerator::choice('denyReason')
			->setLabel($this->translator->trans('entities.request.fields.denyReason'))
			->setChoices([
				$this->translator->trans('entities.request.fields.denyReasons.1') => 'Presupuesto',
				$this->translator->trans('entities.request.fields.denyReasons.2') => 'Desplazamiento',
				$this->translator->trans('entities.request.fields.denyReasons.3') => 'Estilistas',
				$this->translator->trans('entities.request.fields.denyReasons.4') => 'Disponibilidad',
				$this->translator->trans('entities.request.fields.denyReasons.5') => 'Gestion',
				$this->translator->trans('entities.request.fields.denyReasons.6') => 'Otro'
			]);
		$creationDate = FieldGenerator::datetime('creationDate')
			->setLabel($this->translator->trans('entities.request.fields.creationDate'));
		$callDateAnswered = FieldGenerator::datetime('callDateAnswered')
			->setLabel($this->translator->trans('entities.request.fields.callDateAnswered'));
		$callDateValidated = FieldGenerator::datetime('callDateValidated')
			->setLabel($this->translator->trans('entities.request.fields.callDateValidated'));
		$callTodayAnswered = FieldGenerator::checkbox('callTodayAnswered')
			->setLabel($this->translator->trans('entities.request.fields.callTodayAnswered'))
			->addCssClass('mark-table mark-table-danger');
		$callTodayValidated = FieldGenerator::checkbox('callTodayValidated')
			->setLabel($this->translator->trans('entities.request.fields.callTodayValidated'));
		$tipo = FieldGenerator::choice('tipo')
			->setLabel($this->translator->trans('entities.request.fields.tipo'))
			->setChoices([
				$this->translator->trans('entities.request.fields.tipos.1') => 1,
				$this->translator->trans('entities.request.fields.tipos.2') => 2
			])->setColumns(4);

		if ($pageName == Crud::PAGE_INDEX) {
			yield $fullname;
			yield $weddingFulladdress;
			yield $weddingDate;
			yield $callTodayAnswered;
		} else if ($pageName == Crud::PAGE_DETAIL) {
			yield $dataPanel;
			yield $fullname;
			yield $tipo;
			yield $tel;
			yield $email;
			yield $weddingDate;
			yield $weddingProvince;
			yield $weddingCity;
			yield $weddingAddress;
			yield $comments;
		} else if ($pageName == Crud::PAGE_NEW) {
			yield $dataPanel;
			yield $name;
			yield $lastname;
			yield $tipo;
			yield $tel;
			yield $email;
			yield $weddingDate;
			yield $weddingProvince;
			yield $weddingCity;
			yield $weddingAddress;
			yield $comments;
		} else if ($pageName == Crud::PAGE_EDIT) {
			yield $dataPanel;
			yield $name;
			yield $lastname;
			yield $tipo;
			yield $tel;
			yield $email;
			yield $weddingDate;
			yield $weddingProvince;
			yield $weddingCity;
			yield $weddingAddress;
			yield $comments;
		}
	}

	public function configureActions(Actions $actions): Actions
	{
		if (!$this->getUser()->hasPermission('entityRequest')) {
			$actions = Actions::new();
		} else {
			$actions->remove(Crud::PAGE_INDEX, Action::NEW);

			$callAnsweredRequestList = Action::new('callAnsweredRequestList', $this->translator->trans('ea.actions.callRequest'), 'fas fa-fw fa-phone')
				->linkToCrudAction('callAnsweredRequestAction');
			$callAnsweredRequestDetail = Action::new('callAnsweredRequestDetail', $this->translator->trans('ea.actions.callRequest'), 'me-2 fas fa-fw fa-phone')
				->linkToCrudAction('callAnsweredRequestAction')
				->addCssClass('btn btn-outline-secondary');
			$validateRequestList = Action::new('validateRequestList', $this->translator->trans('ea.actions.validateRequest'), 'fas fa-fw fa-square-check')
				->linkToCrudAction('validateRequestAction');
			$validateRequestDetail = Action::new('validateRequestDetail', $this->translator->trans('ea.actions.validateRequest'), 'me-2 fas fa-fw fa-square-check')
				->linkToCrudAction('validateRequestAction')
				->addCssClass('btn btn-outline-secondary');
			$directPendingRequestList = Action::new('directPendingRequestList', $this->translator->trans('ea.actions.pendingRequest'), 'fas fa-fw fa-exclamation-circle')
				->linkToCrudAction('directPendingRequestAction');
			$directPendingRequestDetail = Action::new('directPendingRequestDetail', $this->translator->trans('ea.actions.pendingRequest'), 'me-2 fas fa-fw fa-exclamation-circle')
				->linkToCrudAction('directPendingRequestAction')
				->addCssClass('btn btn-outline-secondary');
			$directDenyRequestList = Action::new('directDenyRequestList', $this->translator->trans('ea.actions.denyRequest'), 'fas fa-fw fa-times')
				->linkToCrudAction('directDenyRequestAction');
			$directDenyRequestDetail = Action::new('directDenyRequestDetail', $this->translator->trans('ea.actions.denyRequest'), 'me-2 fas fa-fw fa-times')
				->linkToCrudAction('directDenyRequestAction')
				->addCssClass('btn btn-outline-secondary');

			$actions->add(Crud::PAGE_INDEX, $callAnsweredRequestList);
			$actions->add(Crud::PAGE_DETAIL, $callAnsweredRequestDetail);
			$actions->add(Crud::PAGE_EDIT, $callAnsweredRequestDetail);
			$actions->add(Crud::PAGE_INDEX, $validateRequestList);
			$actions->add(Crud::PAGE_DETAIL, $validateRequestDetail);
			$actions->add(Crud::PAGE_EDIT, $validateRequestDetail);
			$actions->add(Crud::PAGE_INDEX, $directPendingRequestList);
			$actions->add(Crud::PAGE_DETAIL, $directPendingRequestDetail);
			$actions->add(Crud::PAGE_EDIT, $directPendingRequestDetail);
			$actions->add(Crud::PAGE_INDEX, $directDenyRequestList);
			$actions->add(Crud::PAGE_DETAIL, $directDenyRequestDetail);
			$actions->add(Crud::PAGE_EDIT, $directDenyRequestDetail);
			$actions->reorder(Crud::PAGE_INDEX, ['callAnsweredRequestList', 'validateRequestList', 'directPendingRequestList', 'directDenyRequestList', Action::DETAIL, Action::EDIT, Action::DELETE]);
			$actions->reorder(Crud::PAGE_DETAIL, [Action::INDEX, Action::DELETE, 'callAnsweredRequestDetail', 'validateRequestDetail', 'directPendingRequestDetail','directDenyRequestDetail', Action::EDIT]);
			$actions->reorder(Crud::PAGE_EDIT, [Action::INDEX, Action::DELETE, 'callAnsweredRequestDetail', 'validateRequestDetail', 'directPendingRequestDetail','directDenyRequestDetail', Action::SAVE_AND_RETURN]);
		}

		$actions->add(Crud::PAGE_INDEX, Action::new('export', $this->translator->trans('ea.actions.downloadAsCSV'))
			->setIcon('icon ti ti-download')
			->linkToCrudAction('exportAction')
			->createAsGlobalAction()
		);

		return $actions;
	}
    
    public function configureFilters(Filters $filters): Filters
    {
        $filters->add(TextFilter::new('weddingCity', $this->translator->trans('entities.request.fields.weddingCity')));
        $filters->add(DateTimeFilter::new('weddingDate', $this->translator->trans('entities.request.fields.weddingDate')));

        return $filters;
    }

	public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
	{
		$response = $this->container->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters)
			->andWhere("entity.answered = true AND entity.validated IS NULL");

		return $response;
	}

	public function exportAction(Request $request)
	{
		$context = $request->attributes->get(EA::CONTEXT_REQUEST_ATTRIBUTE);
		$fields = array();
		$entity = $this->em->getRepository($this->getEntityFqcn())->findOneBy(array(), array('id' => 'DESC'));
		if ($entity) {
			$arrEntity = (array) $entity; 
			foreach ($arrEntity as $k => $v) {
				$fields[] = preg_replace('/[\x00-\x1F\x7F]/u', '', str_replace($this->getEntityFqcn(), '', $k));
			}
		}
		$fields = FieldCollection::new($fields);
		$filters = $this->container->get(FilterFactory::class)->create($context->getCrud()->getFiltersConfig(), $fields, $context->getEntity());
		$entities = $this->createIndexQueryBuilder($context->getSearch(), $context->getEntity(), $fields, $filters)->getQuery()->getResult();
		$data = $this->csvService->getEntityAsData($entities, $fields);
		$entityName = $this->translator->trans('entities.request.plural');
		return $this->csvService->export($data, $entityName . ' - ' . date_create()->format('Y-m-d_H-i-s') . '.csv');
	}

	public function callAnsweredRequestAction(Request $request)
	{
		$entityRequest = $this->em->getRepository($this->getEntityFqcn())->find($request->get('entityId'));
		$form = $this->getFormCallRequest();

		if ($request->isMethod('POST')) {
			$form->handleRequest($request);
			if ($form->isValid()) {
				$date = $form['date']->getData();
				$date = $date ?? null;

				$entityRequest->setCallDateAnswered($date);
				$this->em->persist($entityRequest);

				$this->em->flush();

				$url = $this->adminUrlGenerator->setController(RequestAnsweredCrudController::class)->setAction(Crud::PAGE_INDEX)->generateUrl();
				return $this->redirect($url);
			}
		}

		return $this->render('admin/form_callRequest.html.twig', [
			'request' => $entityRequest,
			'form' => $form->createView()
		]);
	}

	public function validateRequestAction(Request $request)
	{
		$session = $this->container->get('request_stack')->getSession();
		$entityRequest = $this->em->getRepository($this->getEntityFqcn())->find($request->get('entityId'));
		$form = $this->getFormValidateRequest();

		// START Prepare templates
		$templatesEntities = $this->em->getRepository(TemplateEmail::class)->createQueryBuilder('t')
			->andWhere('t.type IN (' . implode(',', [15]) . ')')
			->getQuery()->getResult();
		$templates = array();
		foreach ($templatesEntities as $template) {
			$newTemplate = new \stdClass();
			$newTemplate->id = $template->getId();
			$newTemplate->template = $template->getTemplate();

			$newTemplate->template = str_replace("{{ user }}", $entityRequest->getName(), $newTemplate->template);
			$newTemplate->template = str_replace("{{ date }}", $entityRequest->getWeddingDate()->format('d/m/Y'), $newTemplate->template);

			$templates[$template->getId()] = $newTemplate;
		}
		// END Prepare templates

		// START Prepare professionals
		$professionalsEntities = $this->em->getRepository(User::class)->createQueryBuilder('p')
			->leftJoin('p.role', 'r')
			->andWhere("r.name = 'ROLE_PROFESSIONAL'")
			->andWhere("p.active = true");
		if ($entityRequest->getWeddingProvince()) {
			$professionalsEntities = $professionalsEntities
				->leftJoin('p.professionalProvinces', 'prov')
				->andWhere("prov = :province")
				->setParameter(":province", $entityRequest->getWeddingProvince());
		}
		$professionalsEntities = $professionalsEntities->getQuery()->getResult();
		$professionals = array();

		$date = $entityRequest->getWeddingDate()->format('Y-m-d');
		$start = date('Y-m-d H:i:s', strtotime($date . ' 00:00:00'));
		$end = date('Y-m-d H:i:s', strtotime($date . ' 23:59:59'));
		foreach ($professionalsEntities as $professional) {
			$newProfessional = new \stdClass();
			$newProfessional->id = $professional->getId();
			$newProfessional->type = $professional->getProfessionalType();

			// Checkbox label
			$hasWedding = $this->em->getRepository(Wedding::class)->createQueryBuilder('w')
				->join('w.weddingServices', 'ws')
				->where('ws.professional = :professional')
				->andWhere('w.weddingDate BETWEEN :start AND :end')
				->setParameter('professional', $professional)
				->setParameter('start', $start)
				->setParameter('end', $end)
				->setMaxResults(1)
				->getQuery()->getResult();
			$hasTesting = $this->em->getRepository(Wedding::class)->createQueryBuilder('w')
				->join('w.weddingServices', 'ws')
				->where('ws.professional = :professional')
				->andWhere('w.testingDate BETWEEN :start AND :end')
				->setParameter('professional', $professional)
				->setParameter('start', $start)
				->setParameter('end', $end)
				->setMaxResults(1)
				->getQuery()->getResult();
			$hasEvent = $this->em->getRepository(Event::class)->createQueryBuilder('e')
				->where('e.professional = :professional')
				->andWhere('e.start <= :end')
				->andWhere('e.end >= :start')
				->setParameter('professional', $professional)
				->setParameter('end', $end)
				->setParameter('start', $start)
				->setMaxResults(1)
				->getQuery()->getResult();

			$label = $professional->getFullName();
			$newProfessional->occupied = false;
			if (count($hasWedding) || count($hasTesting) || count($hasEvent)) {
				$newProfessional->occupied = true;
				$label .= ' <small class="text-warning">';
				if (count($hasWedding)) {
					foreach ($hasWedding as $wedding) {
						$label .= '<i class="icon ti ti-alert-triangle ms-1"></i>';
						$label .= $this->translator->trans('ea.validateRequest.hasWedding') . $wedding->getWeddingDate()->format('H:i') . ' (' . $wedding->getWeddingLocation() . ')';
					}
				}
				if (count($hasTesting)) {
					foreach ($hasTesting as $testing) {
						$label .= '<i class="icon ti ti-alert-triangle ms-1"></i>';
						$label .= $this->translator->trans('ea.validateRequest.hasTesting') . $testing->getTestingDate()->format('H:i') . ' (' . $testing->getTestingCity() . ')';
					}
				}
				if (count($hasEvent)) {
					foreach ($hasEvent as $event) {
						$label .= '<i class="icon ti ti-alert-triangle ms-1"></i>';
						$label .= $this->translator->trans('ea.validateRequest.hasEvent') . $event->getRangeStr();
					}
				}
				$label .= '</small>';
			}
			$newProfessional->label = $label;

			// Email label
			$newProfessional->content = '';

			$name = $professional->getFullname();
			if ($professional->getProfessionalURL()) {
				$name = '<a href="' . $professional->getProfessionalURL() . '">' . $name . '</a>';
			}

			$rate = null;
			if ($professional->getProfessionalRate()) {
				//$rate = $professional->getProfessionalRate()->getName();
				if ($professional->getProfessionalRate()->getUrl()) {
					//$rate = '<a href="' . $professional->getProfessionalRate()->getUrl() . '">' . $rate . '</a>';
				}
			}

			$newProfessional->content = $name;
			$newProfessional->content .= $rate ? ' - (' . $rate . ')' : null;

			$professionals[$professional->getId()] = $newProfessional;
		}
		// END Prepare professionals

		if ($request->isMethod('POST')) {
			$form->handleRequest($request);
			if ($form->isValid()) {
				$title = $this->translator->trans('ea.validateRequest.validate');
				$content = $form['content']->getData();

				$now = new \DateTime('now');
				$threeDaysFuture = clone $now;
				$threeDaysFuture = $threeDaysFuture->modify('+3 day');
				$entityRequest->setValidated(true);
				$entityRequest->setValidatedDate($now);
				$entityRequest->setCallDateValidated($threeDaysFuture);
				$this->em->persist($entityRequest);

				$emails = array();
				$emailsCC = array();
				$emailsCCO = $session->get('configApp')->emailCCO ? explode(',', $session->get('configApp')->emailCCO) : [];
				$emails[] = $entityRequest->getEmail();
				$email = $this->em->getRepository(Email::class)->sendEmail($title, $content, $emails, $emailsCC, $emailsCCO);
				$this->em->persist($email);

				$this->em->flush();

				$url = $this->adminUrlGenerator->setController(RequestValidatedCrudController::class)->setAction(Crud::PAGE_INDEX)->generateUrl();
				return $this->redirect($url);
			}
		}

		return $this->render('admin/form_validateRequest.html.twig', [
			'request' => $entityRequest,
			'form' => $form->createView(),
			'templates' => $templates,
			'professionals' => $professionals
		]);
	}

	public function directPendingRequestAction(Request $request)
	{
		$session = $this->container->get('request_stack')->getSession();
		$entityRequest = $this->em->getRepository($this->getEntityFqcn())->find($request->get('entityId'));
		$form = $this->getFormPendingRequest();

		// START Prepare templates
		$templatesEntities = $this->em->getRepository(TemplateEmail::class)->createQueryBuilder('t')
			->andWhere('t.type IN (' . implode(',', [14]) . ')')
			->getQuery()->getResult();
		$templates = array();
		foreach ($templatesEntities as $template) {
			$newTemplate = new \stdClass();
			$newTemplate->id = $template->getId();
			$newTemplate->template = $template->getTemplate();

			$newTemplate->template = str_replace("{{ user }}", $entityRequest->getName(), $newTemplate->template);

			$templates[$template->getId()] = $newTemplate;
		}
		// END Prepare templates

		if ($request->isMethod('POST')) {
			$form->handleRequest($request);
			if ($form->isValid()) {
				$title = $this->translator->trans('ea.pendingRequest.pending');
				$content = $form['content']->getData();

				$now = new \DateTime('now');
				$threeDaysFuture = clone $now;
				$threeDaysFuture = $threeDaysFuture->modify('+3 day');

				$entityRequest->setValidated(true);
				$entityRequest->setValidatedDate($now);
				$entityRequest->setCallDateValidated($threeDaysFuture);
				$entityRequest->setPending(true);
				$entityRequest->setAccepted(null);
				$entityRequest->setDenyReason(null);
				$this->em->persist($entityRequest);

				$emails = array();
				$emailsCC = array();
				$emailsCCO = $session->get('configApp')->emailCCO ? explode(',', $session->get('configApp')->emailCCO) : [];
				$emails[] = $entityRequest->getEmail();
				$email = $this->em->getRepository(Email::class)->sendEmail($title, $content, $emails, $emailsCC, $emailsCCO);
				$this->em->persist($email);

				$this->em->flush();

				$url = $this->adminUrlGenerator->setController(RequestPendingCrudController::class)->setAction(Crud::PAGE_INDEX)->generateUrl();
				return $this->redirect($url);
			}
		}

		return $this->render('admin/form_pendingRequest.html.twig', [
			'request' => $entityRequest,
			'form' => $form->createView(),
			'templates' => $templates
		]);
	}

	public function directDenyRequestAction(Request $request)
	{
		$entityRequest = $this->em->getRepository($this->getEntityFqcn())->find($request->get('entityId'));
		$form = $this->getFormDenyRequest();

		if ($request->isMethod('POST')) {
			$form->handleRequest($request);
			if ($form->isValid()) {
				$denyReason = $form['denyReason']->getData();

				$now = new \DateTime('now');
				$entityRequest->setAnswered(true);
				$entityRequest->setAnsweredDate($now);
				$entityRequest->setValidated(true);
				$entityRequest->setValidatedDate($now);
				$entityRequest->setPending(true);
				$entityRequest->setAccepted(false);
				$entityRequest->setDenyReason($denyReason);
				$this->em->persist($entityRequest);

				$this->em->flush();

				$url = $this->adminUrlGenerator->setController(RequestDeniedCrudController::class)->setAction(Crud::PAGE_INDEX)->generateUrl();
				return $this->redirect($url);
			}
		}

		return $this->render('admin/form_denyRequest.html.twig', [
			'request' => $entityRequest,
			'form' => $form->createView()
		]);
	}

	private function getFormCallRequest() {
		$form = $this->createFormBuilder();

		$date = FieldGenerator::datetime('date')
			->setLabel($this->translator->trans('entities.request.fields.callDate'))
			->setFormTypeOption('widget', 'single_text')
			->setRequired(false);
		
		$form = FormGenerator::getFormBuilder($form, [
			$date
		]);

		$form->add('send', SubmitType::class, [
			'label' => '<i class="icon ti ti-phone"></i> <span class="action-label">' . $this->translator->trans('ea.actions.callRequest') . '</span>',
			'label_html' => true,
			'attr' => [
				'class' => 'btn btn-success',
				'form' => 'callRequest-form'
			]
		]);

		return $form->getForm();
	}

	private function getFormValidateRequest() {
		$form = $this->createFormBuilder();

		$templateEmail = FieldGenerator::association('template')
			->setLabel($this->translator->trans('entities.templateEmail.singular'))
			->setFormTypeOption('class', TemplateEmail::class)
			->setFormTypeOption('attr.data-ea-widget', 'ea-autocomplete')
			->setFormTypeOption('query_builder', function ($queryBuilder) {
				return $queryBuilder->createQueryBuilder('entity')
					->andWhere('entity.type IN (' . implode(',', [15]) . ')');
			});
		$content = FieldGenerator::texteditor('content')
			->setLabel($this->translator->trans('entities.email.singular'));
		
		$form = FormGenerator::getFormBuilder($form, [
			$templateEmail,
			$content
		]);

		$form->add('send', SubmitType::class, [
			'label' => '<i class="icon ti ti-mail"></i> <span class="action-label">' . $this->translator->trans('ea.actions.validateRequest') . '</span>',
			'label_html' => true,
			'attr' => [
				'class' => 'btn btn-success',
				'form' => 'validateRequest-form'
			]
		]);

		return $form->getForm();
	}

	private function getFormPendingRequest() {
		$form = $this->createFormBuilder();

		$templateEmail = FieldGenerator::association('template')
			->setLabel($this->translator->trans('entities.templateEmail.singular'))
			->setFormTypeOption('class', TemplateEmail::class)
			->setFormTypeOption('attr.data-ea-widget', 'ea-autocomplete')
			->setFormTypeOption('query_builder', function ($queryBuilder) {
				return $queryBuilder->createQueryBuilder('entity')
					->andWhere('entity.type IN (' . implode(',', [14]) . ')');
			});
		$content = FieldGenerator::texteditor('content')
			->setLabel($this->translator->trans('entities.email.singular'));
		
		$form = FormGenerator::getFormBuilder($form, [
			$templateEmail,
			$content
		]);

		$form->add('send', SubmitType::class, [
			'label' => '<i class="icon ti ti-mail"></i> <span class="action-label">' . $this->translator->trans('ea.actions.pendingRequest') . '</span>',
			'label_html' => true,
			'attr' => [
				'class' => 'btn btn-success',
				'form' => 'pendingRequest-form'
			]
		]);

		return $form->getForm();
	}

	private function getFormDenyRequest() {
		$form = $this->createFormBuilder();

		$denyReasonOpts = [
			$this->translator->trans('entities.request.fields.denyReasons.1') => 'Presupuesto',
			$this->translator->trans('entities.request.fields.denyReasons.2') => 'Desplazamiento',
			$this->translator->trans('entities.request.fields.denyReasons.3') => 'Estilistas',
			$this->translator->trans('entities.request.fields.denyReasons.4') => 'Disponibilidad',
			$this->translator->trans('entities.request.fields.denyReasons.5') => 'Gestion',
			$this->translator->trans('entities.request.fields.denyReasons.6') => 'Otro'
		];

		$denyReason = FieldGenerator::choice('denyReason')
			->setLabel($this->translator->trans('entities.request.fields.denyReason'))
			->setFormTypeOption('attr.data-ea-widget', 'ea-autocomplete')
			->setFormTypeOption('choices', $denyReasonOpts);
		
		$form = FormGenerator::getFormBuilder($form, [
			$denyReason
		]);

		$form->add('send', SubmitType::class, [
			'label' => '<i class="icon ti ti-mail"></i> <span class="action-label">' . $this->translator->trans('ea.actions.denyRequest') . '</span>',
			'label_html' => true,
			'attr' => [
				'class' => 'btn btn-success',
				'form' => 'denyRequest-form'
			]
		]);

		return $form->getForm();
	}
}
