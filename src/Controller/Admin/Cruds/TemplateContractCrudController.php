<?php

namespace App\Controller\Admin\Cruds;

use App\Entity\TemplateContract;
use App\Field\FieldGenerator;
use App\Service\CsvService;

use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Option\EA;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Factory\FilterFactory;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

use Doctrine\ORM\EntityManagerInterface;

class TemplateContractCrudController extends AbstractCrudController
{
	private $em;
	private $translator;
	private $adminUrlGenerator;
	private $csvService;

	public function __construct(EntityManagerInterface $em, TranslatorInterface $translator, AdminUrlGenerator $adminUrlGenerator, CsvService $csvService)
	{
		$this->em = $em;
		$this->translator = $translator;
		$this->adminUrlGenerator = $adminUrlGenerator;
		$this->csvService = $csvService;
	}

	public static function getEntityFqcn(): string
	{
		return TemplateContract::class;
	}

	public function configureCrud(Crud $crud): Crud
	{
		$crud->setEntityLabelInSingular($this->translator->trans('entities.templateContract.singular'));
		$crud->setEntityLabelInPlural($this->translator->trans('entities.templateContract.plural'));
		$crud->setDefaultSort(['type' => 'ASC', 'id' => 'DESC']);

		$entityId = filter_input(INPUT_GET, EA::ENTITY_ID, FILTER_SANITIZE_URL);
		$entity = $entityId ? $this->em->getRepository($this->getEntityFqcn())->find($entityId) : null;
		if ($entity) {
			$crud->setPageTitle(Crud::PAGE_DETAIL, $this->translator->trans('entities.templateContract.singular') . ': ' . $entity);
			$crud->setPageTitle(Crud::PAGE_EDIT, $this->translator->trans('ea.titles.edit', [
				'%entity_label_singular%' => $this->translator->trans('entities.templateContract.singular') . ': ' . $entity
			]));
		}

		return $crud;
	}

	public function configureFields(string $pageName): iterable
	{
		$entityId = filter_input(INPUT_GET, EA::ENTITY_ID, FILTER_SANITIZE_URL);
		$entity = $entityId ? $this->em->getRepository($this->getEntityFqcn())->find($entityId) : null;
		$tareasTypes = [5, 8, 9, 10, 11, 12];

		$dataPanel = FieldGenerator::panel($this->translator->trans('entities.templateContract.sections.data'))
			->setIcon('fas fa-fw fa-envelope');
		$name = FieldGenerator::text('name')
			->setLabel($this->translator->trans('entities.templateContract.fields.name'))
			->setColumns(12);
		$types = [
			$this->translator->trans('entities.templateContract.fields.types.1') => 1,
			$this->translator->trans('entities.templateContract.fields.types.2') => 2
		];
		$type = FieldGenerator::choice('type')
			->setLabel($this->translator->trans('entities.templateContract.fields.type'))
			->setChoices($types)
			->setColumns(12);
		$template = FieldGenerator::texteditor('template')
			->setLabel($this->translator->trans('entities.templateContract.fields.template'))
			->setColumns(12);

		if ($pageName == Crud::PAGE_INDEX) {
			yield $name;
			yield $type;
		} else if ($pageName == Crud::PAGE_DETAIL) {
			yield $dataPanel;
			yield $name;
			yield $type;
			yield $template;
		} else if ($pageName == Crud::PAGE_EDIT) {
			yield $dataPanel;
			yield $name;
			yield $template;
		}
	}

	public function configureActions(Actions $actions): Actions
	{
		if (!$this->getUser()->hasPermission('entityTemplateContract')) {
			$actions = Actions::new();
		} else {
			$actions->remove(Crud::PAGE_INDEX, Action::DELETE);
			$actions->remove(Crud::PAGE_INDEX, Action::NEW);
			$actions->remove(Crud::PAGE_NEW, Action::SAVE_AND_RETURN);
			$actions->remove(Crud::PAGE_DETAIL, Action::DELETE);
			$actions->remove(Crud::PAGE_EDIT, Action::DELETE);
		}

		return $actions;
	}
}
