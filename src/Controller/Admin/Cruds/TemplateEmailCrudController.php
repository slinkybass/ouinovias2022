<?php

namespace App\Controller\Admin\Cruds;

use App\Entity\TemplateEmail;
use App\Field\FieldGenerator;
use App\Service\CsvService;

use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Config\Option\EA;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Factory\FilterFactory;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

use Doctrine\ORM\EntityManagerInterface;

class TemplateEmailCrudController extends AbstractCrudController
{
	private $em;
	private $translator;
	private $adminUrlGenerator;
	private $csvService;

	public function __construct(EntityManagerInterface $em, TranslatorInterface $translator, AdminUrlGenerator $adminUrlGenerator, CsvService $csvService)
	{
		$this->em = $em;
		$this->translator = $translator;
		$this->adminUrlGenerator = $adminUrlGenerator;
		$this->csvService = $csvService;
	}

	public static function getEntityFqcn(): string
	{
		return TemplateEmail::class;
	}

	public function configureCrud(Crud $crud): Crud
	{
		$crud->setEntityLabelInSingular($this->translator->trans('entities.templateEmail.singular'));
		$crud->setEntityLabelInPlural($this->translator->trans('entities.templateEmail.plural'));
		$crud->setDefaultSort(['type' => 'ASC', 'id' => 'DESC']);

		$entityId = filter_input(INPUT_GET, EA::ENTITY_ID, FILTER_SANITIZE_URL);
		$entity = $entityId ? $this->em->getRepository($this->getEntityFqcn())->find($entityId) : null;
		if ($entity) {
			$crud->setPageTitle(Crud::PAGE_DETAIL, $this->translator->trans('entities.templateEmail.singular') . ': ' . $entity);
			$crud->setPageTitle(Crud::PAGE_EDIT, $this->translator->trans('ea.titles.edit', [
				'%entity_label_singular%' => $this->translator->trans('entities.templateEmail.singular') . ': ' . $entity
			]));
		}

		return $crud;
	}

	public function configureFields(string $pageName): iterable
	{
		$entityId = filter_input(INPUT_GET, EA::ENTITY_ID, FILTER_SANITIZE_URL);
		$entity = $entityId ? $this->em->getRepository($this->getEntityFqcn())->find($entityId) : null;
		$tareasTypes = [5, 8, 9, 10, 11, 12];

		$dataPanel = FieldGenerator::panel($this->translator->trans('entities.templateEmail.sections.data'))
			->setIcon('fas fa-fw fa-envelope');
		$name = FieldGenerator::text('name')
			->setLabel($this->translator->trans('entities.templateEmail.fields.name'))
			->setColumns(12);
		$types = [
			$this->translator->trans('entities.templateEmail.fields.types.1') => 1,
			$this->translator->trans('entities.templateEmail.fields.types.2') => 2,
			$this->translator->trans('entities.templateEmail.fields.types.3') => 3,
			$this->translator->trans('entities.templateEmail.fields.types.4') => 4,
			$this->translator->trans('entities.templateEmail.fields.types.11') => 11,
			$this->translator->trans('entities.templateEmail.fields.types.12') => 12,
			$this->translator->trans('entities.templateEmail.fields.types.13') => 13,
			$this->translator->trans('entities.templateEmail.fields.types.14') => 14,
			$this->translator->trans('entities.templateEmail.fields.types.15') => 15,
			$this->translator->trans('entities.templateEmail.fields.types.16') => 16
		];
		if ($pageName == Crud::PAGE_INDEX || $pageName == Crud::PAGE_DETAIL) {
			$types[$this->translator->trans('entities.templateEmail.fields.types.5')] = 5;
			$types[$this->translator->trans('entities.templateEmail.fields.types.6')] = 6;
			$types[$this->translator->trans('entities.templateEmail.fields.types.7')] = 7;
			$types[$this->translator->trans('entities.templateEmail.fields.types.8')] = 8;
			$types[$this->translator->trans('entities.templateEmail.fields.types.9')] = 9;
			$types[$this->translator->trans('entities.templateEmail.fields.types.10')] = 10;
		}
		$type = FieldGenerator::choice('type')
			->setLabel($this->translator->trans('entities.templateEmail.fields.type'))
			->setChoices($types)
			->setColumns(12);
		$template = FieldGenerator::texteditor('template')
			->setLabel($this->translator->trans('entities.templateEmail.fields.template'))
			->setColumns(12);

		if ($pageName == Crud::PAGE_INDEX) {
			yield $name;
			yield $type;
		} else if ($pageName == Crud::PAGE_DETAIL) {
			yield $dataPanel;
			yield $name;
			yield $type;
			yield $template;
		} else if ($pageName == Crud::PAGE_NEW) {
			yield $dataPanel;
			yield $name;
			yield $type;
			yield $template;
		} else if ($pageName == Crud::PAGE_EDIT) {
			yield $dataPanel;
			yield $name;
			if (!in_array($entity->getType(), $tareasTypes)) {
				yield $type;
			}
			yield $template;
		}
	}

	public function configureActions(Actions $actions): Actions
	{
		if (!$this->getUser()->hasPermission('entityTemplateEmail')) {
			$actions = Actions::new();
		} else {
			$actions->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
				return $action->displayIf(static function ($entity) {
					$tareasTypes = [5, 6, 7, 8, 9, 10];
					return !in_array($entity->getType(), $tareasTypes);
				});
			});
			$actions->remove(Crud::PAGE_INDEX, Action::BATCH_DELETE);
		}

		$actions->add(Crud::PAGE_INDEX, Action::new('export', $this->translator->trans('ea.actions.downloadAsCSV'))
			->setIcon('icon ti ti-download')
			->linkToCrudAction('exportAction')
			->createAsGlobalAction()
		);

		return $actions;
	}

	public function createEditFormBuilder(EntityDto $entityDto, KeyValueStore $keyValueStore, AdminContext $context): FormBuilderInterface
	{
		$formBuilder = parent::createEditFormBuilder($entityDto, $keyValueStore, $context);
		$this->clearUrlValuesEventListener($formBuilder);

		return $formBuilder;
	}

	public function createNewFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
	{
		$formBuilder = parent::createNewFormBuilder($entityDto, $formOptions, $context);
		$this->clearUrlValuesEventListener($formBuilder);

		return $formBuilder;
	}

	public function clearUrlValuesEventListener(FormBuilderInterface $formBuilder)
	{
		$formBuilder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
			$template = $event->getData();
			$template->setTemplate(str_replace('https://reservas.ouinovias.com/{{ url }}', '{{ url }}', $template->getTemplate()));
		});
	}

	public function exportAction(Request $request)
	{
		$context = $request->attributes->get(EA::CONTEXT_REQUEST_ATTRIBUTE);
		$fields = array();
		$entity = $this->em->getRepository($this->getEntityFqcn())->findOneBy(array(), array('id' => 'DESC'));
		if ($entity) {
			$arrEntity = (array) $entity; 
			foreach ($arrEntity as $k => $v) {
				$fields[] = preg_replace('/[\x00-\x1F\x7F]/u', '', str_replace($this->getEntityFqcn(), '', $k));
			}
		}
		$fields = FieldCollection::new($fields);
		$filters = $this->container->get(FilterFactory::class)->create($context->getCrud()->getFiltersConfig(), $fields, $context->getEntity());
		$entities = $this->createIndexQueryBuilder($context->getSearch(), $context->getEntity(), $fields, $filters)->getQuery()->getResult();
		$data = $this->csvService->getEntityAsData($entities, $fields);
		$entityName = $this->translator->trans('entities.templateEmail.plural');
		return $this->csvService->export($data, $entityName . ' - ' . date_create()->format('Y-m-d_H-i-s') . '.csv');
	}
}
