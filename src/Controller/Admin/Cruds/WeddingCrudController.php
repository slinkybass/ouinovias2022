<?php

namespace App\Controller\Admin\Cruds;

use App\Entity\Email;
use App\Entity\RateService;
use App\Entity\TemplateEmail;
use App\Entity\Wedding;
use App\Field\FieldGenerator;
use App\Form\FormGenerator;
use App\Service\CsvService;
use App\Twig\PriceTwigExtension;
use App\Controller\Admin\Filters\WeddingProfessionalFilter;

use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Option\EA;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Factory\FilterFactory;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Filter\TextFilter;
use EasyCorp\Bundle\EasyAdminBundle\Filter\DateTimeFilter;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

class WeddingCrudController extends AbstractCrudController
{
	private $em;
	private $translator;
	private $adminUrlGenerator;
	private $csvService;

	public function __construct(EntityManagerInterface $em, TranslatorInterface $translator, AdminUrlGenerator $adminUrlGenerator, CsvService $csvService)
	{
		$this->em = $em;
		$this->translator = $translator;
		$this->adminUrlGenerator = $adminUrlGenerator;
		$this->csvService = $csvService;
	}

	public static function getEntityFqcn(): string
	{
		return Wedding::class;
	}

	public function configureCrud(Crud $crud): Crud
	{
		$crud->setEntityLabelInSingular($this->translator->trans('entities.wedding.singular'));
		$crud->setEntityLabelInPlural($this->translator->trans('entities.wedding.plural'));
		$crud->setDefaultSort(['weddingDate' => 'ASC']);
		$crud->setSearchFields(['client.name', 'client.lastname', 'client.email', 'weddingProvince.name', 'weddingCity', 'weddingAddress', 'testingCity', 'testingAddress']);

		$entityId = filter_input(INPUT_GET, EA::ENTITY_ID, FILTER_SANITIZE_URL);
		$entity = $entityId ? $this->em->getRepository($this->getEntityFqcn())->find($entityId) : null;
		if ($entity) {
			$crud->setPageTitle(Crud::PAGE_DETAIL, $this->translator->trans('entities.wedding.singular') . ': ' . $entity);
			$crud->setPageTitle(Crud::PAGE_EDIT, $this->translator->trans('ea.titles.edit', [
				'%entity_label_singular%' => $this->translator->trans('entities.wedding.singular') . ': ' . $entity
			]));
		}

		return $crud;
	}

	public function configureFields(string $pageName): iterable
	{
		$dataPanel = FieldGenerator::panel($this->translator->trans('entities.wedding.sections.data'))
			->setIcon('fas fa-fw fa-circle-check');
		$dataWeddingPanel = FieldGenerator::panel($this->translator->trans('entities.wedding.sections.dataWedding'))
			->setIcon('fas fa-fw fa-circle-check');
		$dataTestingPanel = FieldGenerator::panel($this->translator->trans('entities.wedding.sections.dataTesting'))
			->setIcon('fas fa-fw fa-circle-check');
		$dataServicesPanel = FieldGenerator::panel($this->translator->trans('entities.wedding.sections.dataServices'))
			->setIcon('fas fa-fw fa-circle-check');
		$dataImagesPanel = FieldGenerator::panel($this->translator->trans('entities.wedding.sections.dataImages'))
			->setIcon('fas fa-fw fa-circle-check');

		$client = FieldGenerator::association('client')
			->setLabel($this->translator->trans('entities.client.singular'))
			->setCrudController(UserCrudController::class)
			->setQueryBuilder(function ($queryBuilder) {
				return $queryBuilder
					->leftJoin('entity.role', 'r')
					->andWhere("r.isAdmin = false")
					->andWhere("r.name != 'ROLE_WEDDINGPLANNER'");
			})->setColumns(6);
		$planner = FieldGenerator::association('planner')
			->setLabel($this->translator->trans('entities.weddingPlanner.singular'))
			->setCrudController(WeddingPlannerCrudController::class)
			->setQueryBuilder(function ($queryBuilder) {
				return $queryBuilder
					->leftJoin('entity.role', 'r')
					->andWhere("r.name = 'ROLE_WEDDINGPLANNER'");
			});

		$weddingAddress = FieldGenerator::text('weddingAddress')
			->setLabel($this->translator->trans('entities.wedding.fields.weddingAddress'))
			->setColumns(6);
		$weddingProvince = FieldGenerator::association('weddingProvince')
			->setLabel($this->translator->trans('entities.province.singular'))
			->setColumns(3);
		$weddingCity = FieldGenerator::text('weddingCity')
			->setLabel($this->translator->trans('entities.wedding.fields.weddingCity'))
			->setColumns(3);
		$weddingFulladdress = FieldGenerator::text('weddingFulladdress')
			->setLabel($this->translator->trans('entities.request.fields.weddingFulladdress'));
		$weddingDate = FieldGenerator::datetime('weddingDate')
			->setLabel($this->translator->trans('entities.wedding.fields.weddingDate'))
			->setColumns(6);
		$weddingComments = FieldGenerator::textarea('weddingComments')
			->setLabel($this->translator->trans('entities.wedding.fields.weddingComments'));
		$weddingDisplacementCost = FieldGenerator::float('weddingDisplacementCost')
			->setLabel($this->translator->trans('entities.wedding.fields.weddingDisplacementCost'))
			->setTemplatePath('field/price.html.twig')
			->setColumns(3);
		$weddingPayMethod = FieldGenerator::choice('weddingPayMethod')
			->setLabel($this->translator->trans('entities.wedding.fields.weddingPayMethod'))
			->setChoices([
				$this->translator->trans('entities.wedding.fields.payMethods.1') => $this->translator->trans('entities.wedding.fields.payMethods.1'),
				$this->translator->trans('entities.wedding.fields.payMethods.2') => $this->translator->trans('entities.wedding.fields.payMethods.2'),
				$this->translator->trans('entities.wedding.fields.payMethods.3') => $this->translator->trans('entities.wedding.fields.payMethods.3'),
				$this->translator->trans('entities.wedding.fields.payMethods.4') => $this->translator->trans('entities.wedding.fields.payMethods.4'),
			])
			->setColumns(6);
		$weddingPayId = FieldGenerator::url('weddingPayId')
			->setLabel($this->translator->trans('entities.wedding.fields.weddingPayId'))
			->setColumns(6);
		$weddingCitationSended = FieldGenerator::switch('weddingCitationSended')
			->setLabel($this->translator->trans('entities.wedding.fields.weddingCitationSended'));
		$weddingSatisfaction = FieldGenerator::textarea('weddingSatisfaction')
			->setLabel($this->translator->trans('entities.wedding.fields.weddingSatisfaction'));

		$testingAddress = FieldGenerator::text('testingAddress')
			->setLabel($this->translator->trans('entities.wedding.fields.testingAddress'))
			->setColumns(6);
		$testingCity = FieldGenerator::text('testingCity')
			->setLabel($this->translator->trans('entities.wedding.fields.testingCity'))
			->setColumns(6);
		$testingFulladdress = FieldGenerator::text('testingFulladdress')
			->setLabel($this->translator->trans('entities.wedding.fields.testingFulladdress'));
		$testingDate = FieldGenerator::datetime('testingDate')
			->setLabel($this->translator->trans('entities.wedding.fields.testingDate'));
		$testingComments = FieldGenerator::textarea('testingComments')
			->setLabel($this->translator->trans('entities.wedding.fields.testingComments'));
		$testingDisplacementCost = FieldGenerator::float('testingDisplacementCost')
			->setLabel($this->translator->trans('entities.wedding.fields.testingDisplacementCost'))
			->setTemplatePath('field/price.html.twig')
			->setColumns(3);
		$testingPayMethod = FieldGenerator::choice('testingPayMethod')
			->setLabel($this->translator->trans('entities.wedding.fields.testingPayMethod'))
			->setChoices([
				$this->translator->trans('entities.wedding.fields.payMethods.1') => $this->translator->trans('entities.wedding.fields.payMethods.1'),
				$this->translator->trans('entities.wedding.fields.payMethods.2') => $this->translator->trans('entities.wedding.fields.payMethods.2'),
				$this->translator->trans('entities.wedding.fields.payMethods.3') => $this->translator->trans('entities.wedding.fields.payMethods.3'),
				$this->translator->trans('entities.wedding.fields.payMethods.4') => $this->translator->trans('entities.wedding.fields.payMethods.4')
			])
			->setColumns(6);
		$testingPayId = FieldGenerator::url('testingPayId')
			->setLabel($this->translator->trans('entities.wedding.fields.testingPayId'))
			->setColumns(6);
		$testingCitationSended = FieldGenerator::switch('testingCitationSended')
			->setLabel($this->translator->trans('entities.wedding.fields.testingCitationSended'));
		$testingSign = FieldGenerator::text('testingSign')
			->setLabel($this->translator->trans('entities.wedding.fields.testingSign'))
			->setTemplatePath('field/sign.html.twig');

		$meetingTime = FieldGenerator::text('meetingTime')
			->setLabel($this->translator->trans('entities.wedding.fields.meetingTime'))
			->setFormTypeOption('attr.data-ea-time-field', 'true')
			->addWebpackEncoreEntries('form-type-time')
			->setColumns(6);
		$urlFB = FieldGenerator::text('urlFB')
			->setLabel($this->translator->trans('entities.wedding.fields.urlFB'))
			->setColumns(6);
		$urlIG = FieldGenerator::text('urlIG')
			->setLabel($this->translator->trans('entities.wedding.fields.urlIG'))
			->setColumns(6);
		$requestModify = FieldGenerator::textarea('requestModify')
			->setLabel($this->translator->trans('entities.wedding.fields.requestModify'));
		$creationDate = FieldGenerator::datetime('creationDate')
			->setLabel($this->translator->trans('entities.wedding.fields.creationDate'));
		$weddingServices = FieldGenerator::collection('weddingServices')
			->setLabel($this->translator->trans('entities.weddingService.plural'))
			->setTemplatePath('field/weddingServices.html.twig');

		$tel1 = FieldGenerator::phone('client.tel1')
			->setLabel($this->translator->trans('entities.user.fields.tel1'));
		$tel2 = FieldGenerator::phone('client.tel2')
			->setLabel($this->translator->trans('entities.user.fields.tel2'));
		$email = FieldGenerator::email('client.email')
			->setLabel($this->translator->trans('entities.user.fields.email'));
		$clientConocePor = FieldGenerator::text('client.clientConocePor')
			->setLabel($this->translator->trans('entities.user.fields.clientConocePor'));

		$image1 = FieldGenerator::media('image1')
			->setLabel($this->translator->trans('entities.wedding.fields.image'))
			->setFormTypeOption('attr.data-type', 'public_bodas');
		$image2 = FieldGenerator::media('image2')
			->setLabel($this->translator->trans('entities.wedding.fields.image'))
			->setFormTypeOption('attr.data-type', 'public_bodas');
		$image3 = FieldGenerator::media('image3')
			->setLabel($this->translator->trans('entities.wedding.fields.image'))
			->setFormTypeOption('attr.data-type', 'public_bodas');
		$image4 = FieldGenerator::media('image4')
			->setLabel($this->translator->trans('entities.wedding.fields.image'))
			->setFormTypeOption('attr.data-type', 'public_bodas');
		$image5 = FieldGenerator::media('image5')
			->setLabel($this->translator->trans('entities.wedding.fields.image'))
			->setFormTypeOption('attr.data-type', 'public_bodas');
		$image6 = FieldGenerator::media('image6')
			->setLabel($this->translator->trans('entities.wedding.fields.image'))
			->setFormTypeOption('attr.data-type', 'public_bodas');
		$tipo = FieldGenerator::choice('tipo')
			->setLabel($this->translator->trans('entities.wedding.fields.tipo'))
			->setChoices([
				$this->translator->trans('entities.wedding.fields.tipos.1') => 1,
				$this->translator->trans('entities.wedding.fields.tipos.2') => 2
			])->setColumns(6);

		if ($pageName == Crud::PAGE_INDEX) {
			yield $client;
			yield $weddingFulladdress;
			yield $testingDate;
			yield $weddingDate;
		} else if ($pageName == Crud::PAGE_DETAIL) {
			yield $dataPanel;
			yield $client;
			yield $tipo;
			yield $tel1;
			yield $tel2;
			yield $email;
			yield $clientConocePor;
			yield $urlFB;
			yield $urlIG;
			yield $planner;
			yield $dataWeddingPanel;
			yield $weddingDate;
			yield $meetingTime;
			yield $weddingProvince;
			yield $weddingCity;
			yield $weddingAddress;
			yield $weddingComments;
			//yield $weddingDisplacementCost;
			yield $weddingPayMethod;
			yield $weddingPayId;
			yield $weddingSatisfaction;
			yield $dataTestingPanel;
			yield $testingDate;
			yield $testingCity;
			yield $testingAddress;
			yield $testingComments;
			//yield $testingDisplacementCost;
			yield $testingPayMethod;
			yield $testingPayId;
			yield $testingSign;
			yield $dataImagesPanel;
			yield $image1;
			yield $image2;
			yield $image3;
			yield $image4;
			yield $image5;
			yield $image6;
			yield $dataServicesPanel;
			yield $weddingServices;
			yield $requestModify;
		} else if ($pageName == Crud::PAGE_NEW) {
			yield $dataPanel;
			yield $client;
			yield $tipo;
			yield $urlFB;
			yield $urlIG;
			yield $planner;
			yield $dataWeddingPanel;
			yield $weddingDate;
			yield $meetingTime;
			yield $weddingProvince;
			yield $weddingCity;
			yield $weddingAddress;
			yield $weddingComments;
			//yield $weddingDisplacementCost;
			yield $weddingPayMethod;
			yield $weddingPayId;
			yield $weddingSatisfaction;
			yield $dataTestingPanel;
			yield $testingDate;
			yield $testingCity;
			yield $testingAddress;
			yield $testingComments;
			//yield $testingDisplacementCost;
			yield $testingPayMethod;
			yield $testingPayId;
			yield $dataServicesPanel;
			yield $requestModify;
			yield $dataImagesPanel;
			yield $image1;
			yield $image2;
			yield $image3;
			yield $image4;
			yield $image5;
			yield $image6;
		} else if ($pageName == Crud::PAGE_EDIT) {
			yield $dataPanel;
			yield $client;
			yield $tipo;
			yield $urlFB;
			yield $urlIG;
			yield $planner;
			yield $dataWeddingPanel;
			yield $weddingDate;
			yield $meetingTime;
			yield $weddingProvince;
			yield $weddingCity;
			yield $weddingAddress;
			yield $weddingComments;
			//yield $weddingDisplacementCost;
			yield $weddingPayMethod;
			yield $weddingPayId;
			yield $weddingSatisfaction;
			yield $dataTestingPanel;
			yield $testingDate;
			yield $testingCity;
			yield $testingAddress;
			yield $testingComments;
			//yield $testingDisplacementCost;
			yield $testingPayMethod;
			yield $testingPayId;
			yield $dataServicesPanel;
			yield $requestModify;
			yield $dataImagesPanel;
			yield $image1;
			yield $image2;
			yield $image3;
			yield $image4;
			yield $image5;
			yield $image6;
		}
	}

	public function configureActions(Actions $actions): Actions
	{
		if (!$this->getUser()->hasPermission('entityWedding') && $this->getUser()->getRole()->getName() != "ROLE_PROFESSIONAL") {
			$actions = Actions::new();
		} else {
			$weddingServicesList = Action::new('weddingServicesList', $this->translator->trans('ea.actions.weddingServices'), 'icon ti ti-server')->linkToUrl(function (Wedding $entity) {
				return $this->adminUrlGenerator->setAction(Action::EDIT)->setEntityId($entity->getId())->setController(WeddingServiceCrudController::class)->generateUrl();
			});
			$weddingServicesDetail = Action::new('weddingServicesDetail', $this->translator->trans('ea.actions.weddingServices'), 'icon ti ti-server')->linkToUrl(function (Wedding $entity) {
				return $this->adminUrlGenerator->setAction(Action::EDIT)->setEntityId($entity->getId())->setController(WeddingServiceCrudController::class)->generateUrl();
			})->addCssClass('btn btn-outline-secondary');

			$citacionList = Action::new('citacionList', $this->translator->trans('ea.actions.citacion'), 'icon ti ti-calendar-plus')
				->linkToCrudAction('citacionAction');
			$citacionDetail = Action::new('citacionDetail', $this->translator->trans('ea.actions.citacion'), 'icon ti ti-calendar-plus')
				->linkToCrudAction('citacionAction')
				->addCssClass('btn btn-outline-secondary');

			$testingSignList = Action::new('testingSignList', $this->translator->trans('ea.actions.testingSign'), 'icon ti ti-pencil')
				->linkToCrudAction('testingSignAction')
				->displayIf(static function ($entity) {
					$now = new \DateTime('now');
					$signatureEnabled = $entity->getTestingDate() ? ($now->format('d-m-Y') == $entity->getTestingDate()->format('d-m-Y')) : false;
					return $signatureEnabled && $entity->getTestingSign() == null;
				});
			$testingSignDetail = Action::new('testingSignDetail', $this->translator->trans('ea.actions.testingSign'), 'icon ti ti-pencil')
				->linkToCrudAction('testingSignAction')
				->addCssClass('btn btn-outline-secondary')
				->displayIf(static function ($entity) {
					$now = new \DateTime('now');
					$signatureEnabled = $entity->getTestingDate() ? ($now->format('d-m-Y') == $entity->getTestingDate()->format('d-m-Y')) : false;
					return $signatureEnabled && $entity->getTestingSign() == null;
				});

			if ($this->getUser()->getRole()->getName() != "ROLE_PROFESSIONAL") {
				$actions->add(Crud::PAGE_INDEX, $weddingServicesList);
				$actions->add(Crud::PAGE_DETAIL, $weddingServicesDetail);
				$actions->add(Crud::PAGE_EDIT, $weddingServicesDetail);
				$actions->add(Crud::PAGE_INDEX, $citacionList);
				$actions->add(Crud::PAGE_DETAIL, $citacionDetail);
				$actions->add(Crud::PAGE_EDIT, $citacionDetail);
			} else {
				$actions->remove(Crud::PAGE_INDEX, Action::DELETE);
				$actions->remove(Crud::PAGE_DETAIL, Action::DELETE);
				$actions->remove(Crud::PAGE_EDIT, Action::DELETE);
			}
			$actions->add(Crud::PAGE_INDEX, $testingSignList);
			$actions->add(Crud::PAGE_DETAIL, $testingSignDetail);
			$actions->add(Crud::PAGE_EDIT, $testingSignDetail);
			if ($this->getUser()->getRole()->getName() != "ROLE_PROFESSIONAL") {
				$actions->reorder(Crud::PAGE_INDEX, ['weddingServicesList', 'citacionList', 'testingSignList', Action::DETAIL, Action::EDIT, Action::DELETE]);
				$actions->reorder(Crud::PAGE_DETAIL, [Action::INDEX, Action::DELETE, 'weddingServicesDetail', 'citacionDetail', 'testingSignDetail', Action::EDIT]);
				$actions->reorder(Crud::PAGE_EDIT, [Action::INDEX, Action::DELETE, 'weddingServicesDetail', 'citacionDetail', 'testingSignDetail', Action::SAVE_AND_RETURN]);
			} else {
				$actions->reorder(Crud::PAGE_INDEX, ['testingSignList', Action::DETAIL, Action::EDIT]);
				$actions->reorder(Crud::PAGE_DETAIL, [Action::INDEX, 'testingSignDetail', Action::EDIT]);
				$actions->reorder(Crud::PAGE_EDIT, [Action::INDEX, 'testingSignDetail', Action::SAVE_AND_RETURN]);
			}
		}

		$actions->add(Crud::PAGE_INDEX, Action::new('export', $this->translator->trans('ea.actions.downloadAsCSV'))
			->setIcon('icon ti ti-download')
			->linkToCrudAction('exportAction')
			->createAsGlobalAction()
		);

		return $actions;
	}
    
    public function configureFilters(Filters $filters): Filters
    {
        $filters->add(TextFilter::new('weddingCity', $this->translator->trans('entities.wedding.fields.weddingCity')));
        $filters->add(TextFilter::new('testingCity', $this->translator->trans('entities.wedding.fields.testingCity')));
        $filters->add(DateTimeFilter::new('weddingDate', $this->translator->trans('entities.wedding.fields.weddingDate')));
        $filters->add(DateTimeFilter::new('testingDate', $this->translator->trans('entities.wedding.fields.testingDate')));
        $filters->add(WeddingProfessionalFilter::new('professional', $this->translator->trans('entities.professional.singular')));

        return $filters;
    }

	public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
	{
		$now = new \Datetime('now');
		$response = $this->container->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters)
			->andWhere('entity.weddingDate >= :now')->setParameter('now', $now->setTime(0, 0));

		return $response;
	}

	public function createNewFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
	{
		$session = $this->container->get('request_stack')->getSession();

		$formBuilder = parent::createNewFormBuilder($entityDto, $formOptions, $context);
		$this->setCreationDateEventListener($formBuilder);

		return $formBuilder;
	}

	public function setCreationDateEventListener(FormBuilderInterface $formBuilder)
	{
		$formBuilder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
			$now = new \Datetime('now');
			$user = $event->getData();
			$user->setCreationDate($now);
		});
	}

	public function exportAction(Request $request)
	{
		$context = $request->attributes->get(EA::CONTEXT_REQUEST_ATTRIBUTE);
		$fields = array();
		$entity = $this->em->getRepository($this->getEntityFqcn())->findOneBy(array(), array('id' => 'DESC'));
		if ($entity) {
			$arrEntity = (array) $entity; 
			foreach ($arrEntity as $k => $v) {
				$fields[] = preg_replace('/[\x00-\x1F\x7F]/u', '', str_replace($this->getEntityFqcn(), '', $k));
			}
		}
		array_splice($fields, 2, 0, array('client.email'));
		$fields = FieldCollection::new($fields);
		$filters = $this->container->get(FilterFactory::class)->create($context->getCrud()->getFiltersConfig(), $fields, $context->getEntity());
		$entities = $this->createIndexQueryBuilder($context->getSearch(), $context->getEntity(), $fields, $filters)->getQuery()->getResult();
		$data = $this->csvService->getEntityAsData($entities, $fields);
		$entityName = $this->translator->trans('entities.wedding.plural');
		return $this->csvService->export($data, $entityName . ' - ' . date_create()->format('Y-m-d_H-i-s') . '.csv');
	}

	public function citacionAction(Request $request)
	{
		$session = $this->container->get('request_stack')->getSession();
		$wedding = $this->em->getRepository($this->getEntityFqcn())->find($request->get('entityId'));
		$form = $this->getFormCitacion();
		$priceTwigExt = new PriceTwigExtension();

		// START Prepare templates
		$templatesEntities = $this->em->getRepository(TemplateEmail::class)->createQueryBuilder('t')
			->andWhere('t.type IN (' . implode(',', [11, 12]) . ')')
			->getQuery()->getResult();
		$templates = array();
		foreach ($templatesEntities as $template) {
			$newTemplate = new \stdClass();
			$newTemplate->id = $template->getId();
			$newTemplate->type = $template->getType();
			$newTemplate->template = $template->getTemplate();
			$newTemplate->cost = $wedding->getWeddingExtraPayment() + $wedding->getTestingExtraPayment();
			$newTemplate->costDisplacement = 0;
			$newTemplate->professionals = array();
			$newTemplate->services = array();

			// START Services and Costs
			foreach ($wedding->getWeddingServices() as $weddingService) {
				$newService = new \stdClass();
				$newService->service = $weddingService->getService()->getName();
				$newService->professional = $weddingService->getProfessional();
				$newService->count = $weddingService->getCount();
				$newService->location = $weddingService->getLocation();
				$newService->cost = 0;
				$newService->costDisplacement = 0;

				$rateService = null;
				if ($weddingService->getProfessional() && $weddingService->getProfessional()->getProfessionalRate()) {
					$rateService = $this->em->getRepository(RateService::class)->findOneBy([
						'rate' => $weddingService->getProfessional()->getProfessionalRate(),
						'service' => $weddingService->getService(),
					]);
				}
				if ($rateService) {
					if ($newTemplate->type == 11) {
						$newService->cost = $rateService->getPriceWedding() * $weddingService->getCount();
						$newService->costDisplacement = $weddingService->getWeddingDisplacementCost();
					} elseif ($newTemplate->type == 12) {
						$newService->cost = $rateService->getPriceTesting() * $weddingService->getCount();
						$newService->costDisplacement = $weddingService->getTestingDisplacementCost();
					}
					if ($weddingService->getProfessional()) {
						if ((($newTemplate->type == 11) && $rateService->getPriceWedding()) ||
							(($newTemplate->type == 12) && $rateService->getPriceTesting())) {
							$newTemplate->professionals[$weddingService->getProfessional()->getId()] = $weddingService->getProfessional();
						}
					}
				}
				$newTemplate->services[] = $newService;
				$newTemplate->cost += $newService->cost + $newService->costDisplacement;
				$newTemplate->costDisplacement += $newService->costDisplacement;
			}
			// END Services and Costs

			// START Data
			$data = null;
			if ($wedding->getClient()->getTel1()) {
				$data .= '<p><b>TELÉFONO DE ' . $wedding->getClient()->getName() . '</b>: ' . $wedding->getClient()->getTel1() . '</p>';
			}
			if ($wedding->getClient()->getTel2()) {
				$data .= '<p><b>TELÉFONO EXTRA DE ' . $wedding->getClient()->getName() . '</b>: ' . $wedding->getClient()->getTel2() . '</p>';
			}
			$data .= '<p><b>TELÉFONO DE Oui Novias</b>: ' . $session->get('configApp')->tel . '</p>';
			if ($newTemplate->type == 11) {
				$data .= '<p><b>DÍA DEL ENLACE</b>: ' . $wedding->getWeddingDate()->format('d/m/Y H:i') . '</p>';
			}
			if ($newTemplate->type == 12 && $wedding->getTestingDate()) {
				$data .= '<p><b>DÍA DE LA PRUEBA</b>: ' . $wedding->getTestingDate()->format('d/m/Y H:i') . '</p>';
			}
			if ($newTemplate->type == 11 && $wedding->getWeddingCity()) {
				$data .= '<p><b>LOCALIDAD</b>:<br />' . $wedding->getWeddingCity() . '</p>';
			}
			if ($newTemplate->type == 12 && $wedding->getTestingCity()) {
				$data .= '<p><b>LOCALIDAD</b>:<br />' . $wedding->getTestingCity() . '</p>';
			}
			if ($newTemplate->type == 11) {
				$data .= '<p><b>DIRECCIÓN</b>:<br />' . $wedding->getWeddingAddress() . '</p>';
			}
			if ($newTemplate->type == 12 && $wedding->getTestingAddress()) {
				$data .= '<p><b>DIRECCIÓN</b>:<br />' . $wedding->getTestingAddress() . '</p>';
			}
			if ($newTemplate->type == 11 && $wedding->getMeetingTime()) {
				$data .= '<p><b>HORA DE LA CITA</b>:<br />' . $wedding->getMeetingTime() . '</p>';
			}
			if ($newTemplate->type == 11 && $wedding->getWeddingPayMethod()) {
				$data .= '<p><b>MÉTODO DE PAGO</b>: ' . $wedding->getWeddingPayMethod() . '</p>';
				if ($wedding->getWeddingPayMethod() == "Transferencia") {
					$data .= '<p>' . $session->get('configApp')->bankAccount . '</p>';
				}
			}
			if ($newTemplate->type == 12 && $wedding->getTestingPayMethod()) {
				$data .= '<p><b>MÉTODO DE PAGO</b>: ' . $wedding->getTestingPayMethod() . '</p>';
				if ($wedding->getTestingPayMethod() == "Transferencia") {
					$data .= '<p>' . $session->get('configApp')->bankAccount . '</p>';
				}
			}
			if ($newTemplate->costDisplacement) {
				$data .= '<p><b>DESPLAZAMIENTO</b>: ' . $priceTwigExt->formatPrice($newTemplate->costDisplacement) . ' €</p>';
			}
			$hasServices = false;
			foreach ($newTemplate->services as $service) {
				if ($service->cost) {
					$hasServices = true;
				}
			}
			if ($hasServices) {
				$data .= '<p><b>SERVICIOS</b>:</p>';
				foreach ($newTemplate->services as $service) {
					if ($service->cost) {
						$data .= '<p>' . $service->count . ' ' . $service->service . ' (' . $priceTwigExt->formatPrice($service->cost) . ' €)</p>';
					}
				}
			}
			$data .= '<p><b>ABONAR AL ESTILISTA:</b></p>';
			$data .= '<p>' . $priceTwigExt->formatPrice($newTemplate->cost) . ' €</p>';
			// END Data

			$newTemplate->template = str_replace("{{ user }}", $wedding->getClient()->getName(), $newTemplate->template);
			$newTemplate->template = str_replace("{{ data }}", $data, $newTemplate->template);

			$templates[$template->getId()] = $newTemplate;
		}
		// END Prepare templates

		if ($request->isMethod('POST')) {
			$form->handleRequest($request);
			if ($form->isValid()) {
				$templateId = $form['template']->getData()->getId();
				$template = $templates[$templateId];
				$title = null;
				$content = $form['content']->getData();
				$extraEmails = $form['extraEmails']->getData();
				$extraEmails = $extraEmails ? explode(',', $extraEmails) : array();
		
				if ($template->type == 11) {
					$title = $this->translator->trans('ea.citacion.wedding');
				} elseif ($template->type == 12) {
					$title = $this->translator->trans('ea.citacion.testing');
				}
		
				if ($template->type == 11) {
					$wedding->setWeddingCitationSended(true);
				} elseif ($template->type == 12) {
					$wedding->setTestingCitationSended(true);
				}
				$this->em->persist($wedding);

				$emails = array();
				$emailsCC = array();
				$emailsCCO = $session->get('configApp')->emailCCO ? explode(',', $session->get('configApp')->emailCCO) : [];
				$emailsCCO = array_merge($emailsCCO, $extraEmails);
				$emails[] = $wedding->getClient()->getEmail();
				foreach ($template->professionals as $professional) {
					$emailsCCO[] = $professional->getEmail();
				}
				$email = $this->em->getRepository(Email::class)->sendEmail($title, $content, $emails, $emailsCC, $emailsCCO);
				$this->em->persist($email);

				$this->em->flush();

				$url = $this->adminUrlGenerator->setController(WeddingCrudController::class)->setAction(Crud::PAGE_INDEX)->generateUrl();
				return $this->redirect($url);
			}
		}

		return $this->render('admin/form_citacion.html.twig', [
			'wedding' => $wedding,
			'templates' => $templates,
			'form' => $form->createView()
		]);
	}

	public function testingSignAction(Request $request)
	{
		$wedding = $this->em->getRepository($this->getEntityFqcn())->find($request->get('entityId'));

		return $this->render('admin/testingSign.html.twig', [
			'wedding' => $wedding
		]);
	}

	private function getFormCitacion()
	{
		$form = $this->createFormBuilder();

		$extraEmails = FieldGenerator::text('extraEmails')
			->setLabel($this->translator->trans('entities.configApp.fields.emailCCO'))
			->setRequired(false);
		$templateEmail = FieldGenerator::association('template')
			->setLabel($this->translator->trans('entities.templateEmail.singular'))
			->setFormTypeOption('class', TemplateEmail::class)
			->setFormTypeOption('attr.data-ea-widget', 'ea-autocomplete')
			->setFormTypeOption('query_builder', function ($queryBuilder) {
				return $queryBuilder->createQueryBuilder('entity')
					->andWhere('entity.type IN (' . implode(',', [11, 12]) . ')');
			});
		$content = FieldGenerator::texteditor('content')
			->setLabel($this->translator->trans('entities.email.singular'));
		
		$form = FormGenerator::getFormBuilder($form, [
			$extraEmails,
			$templateEmail,
			$content,
		]);

		$form->add('send', SubmitType::class, [
			'label' => '<i class="icon ti ti-mail"></i> <span class="action-label">' . $this->translator->trans('ea.actions.citacion') . '</span>',
			'label_html' => true,
			'attr' => [
				'class' => 'btn btn-success',
				'form' => 'citacion-form'
			]
		]);

		return $form->getForm();
	}
}
