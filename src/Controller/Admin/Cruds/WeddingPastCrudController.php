<?php

namespace App\Controller\Admin\Cruds;

use App\Entity\Wedding;
use App\Field\FieldGenerator;
use App\Service\CsvService;
use App\Controller\Admin\Filters\WeddingProfessionalFilter;

use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Option\EA;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Factory\FilterFactory;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Filter\TextFilter;
use EasyCorp\Bundle\EasyAdminBundle\Filter\DateTimeFilter;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

class WeddingPastCrudController extends AbstractCrudController
{
	private $em;
	private $translator;
	private $adminUrlGenerator;
	private $csvService;

	public function __construct(EntityManagerInterface $em, TranslatorInterface $translator, AdminUrlGenerator $adminUrlGenerator, CsvService $csvService)
	{
		$this->em = $em;
		$this->translator = $translator;
		$this->adminUrlGenerator = $adminUrlGenerator;
		$this->csvService = $csvService;
	}

	public static function getEntityFqcn(): string
	{
		return Wedding::class;
	}

	public function configureCrud(Crud $crud): Crud
	{
		$crud->setEntityLabelInSingular($this->translator->trans('entities.wedding.singular'));
		$crud->setEntityLabelInPlural($this->translator->trans('entities.wedding.plural'));
		$crud->setDefaultSort(['weddingDate' => 'DESC']);
		$crud->setSearchFields(['client.name', 'client.lastname', 'client.email', 'weddingProvince.name', 'weddingCity', 'weddingAddress', 'testingCity', 'testingAddress']);

		$entityId = filter_input(INPUT_GET, EA::ENTITY_ID, FILTER_SANITIZE_URL);
		$entity = $entityId ? $this->em->getRepository($this->getEntityFqcn())->find($entityId) : null;
		if ($entity) {
			$crud->setPageTitle(Crud::PAGE_DETAIL, $this->translator->trans('entities.wedding.singular') . ': ' . $entity);
			$crud->setPageTitle(Crud::PAGE_EDIT, $this->translator->trans('ea.titles.edit', [
				'%entity_label_singular%' => $this->translator->trans('entities.wedding.singular') . ': ' . $entity
			]));
		}
		$crud->setPageTitle(Crud::PAGE_INDEX, $this->translator->trans('entities.wedding.history'));

		return $crud;
	}

	public function configureFields(string $pageName): iterable
	{
		$dataPanel = FieldGenerator::panel($this->translator->trans('entities.wedding.sections.data'))
			->setIcon('fas fa-fw fa-circle-check');
		$dataWeddingPanel = FieldGenerator::panel($this->translator->trans('entities.wedding.sections.dataWedding'))
			->setIcon('fas fa-fw fa-circle-check');
		$dataTestingPanel = FieldGenerator::panel($this->translator->trans('entities.wedding.sections.dataTesting'))
			->setIcon('fas fa-fw fa-circle-check');
		$dataServicesPanel = FieldGenerator::panel($this->translator->trans('entities.wedding.sections.dataServices'))
			->setIcon('fas fa-fw fa-circle-check');
		$dataImagesPanel = FieldGenerator::panel($this->translator->trans('entities.wedding.sections.dataImages'))
			->setIcon('fas fa-fw fa-circle-check');

		$client = FieldGenerator::association('client')
			->setLabel($this->translator->trans('entities.client.singular'))
			->setCrudController(UserCrudController::class)
			->setQueryBuilder(function ($queryBuilder) {
				return $queryBuilder
					->leftJoin('entity.role', 'r')
					->andWhere("r.isAdmin = false")
					->andWhere("r.name != 'ROLE_WEDDINGPLANNER'");
			})->setColumns(6);
		$planner = FieldGenerator::association('planner')
			->setLabel($this->translator->trans('entities.weddingPlanner.singular'))
			->setCrudController(WeddingPlannerCrudController::class)
			->setQueryBuilder(function ($queryBuilder) {
				return $queryBuilder
					->leftJoin('entity.role', 'r')
					->andWhere("r.name = 'ROLE_WEDDINGPLANNER'");
			});

		$weddingAddress = FieldGenerator::text('weddingAddress')
			->setLabel($this->translator->trans('entities.wedding.fields.weddingAddress'))
			->setColumns(6);
		$weddingProvince = FieldGenerator::association('weddingProvince')
			->setLabel($this->translator->trans('entities.province.singular'))
			->setColumns(3);
		$weddingCity = FieldGenerator::text('weddingCity')
			->setLabel($this->translator->trans('entities.wedding.fields.weddingCity'))
			->setColumns(3);
		$weddingFulladdress = FieldGenerator::text('weddingFulladdress')
			->setLabel($this->translator->trans('entities.request.fields.weddingFulladdress'));
		$weddingDate = FieldGenerator::datetime('weddingDate')
			->setLabel($this->translator->trans('entities.wedding.fields.weddingDate'))
			->setColumns(6);
		$weddingComments = FieldGenerator::textarea('weddingComments')
			->setLabel($this->translator->trans('entities.wedding.fields.weddingComments'));
		$weddingDisplacementCost = FieldGenerator::float('weddingDisplacementCost')
			->setLabel($this->translator->trans('entities.wedding.fields.weddingDisplacementCost'))
			->setTemplatePath('field/price.html.twig')
			->setColumns(3);
		$weddingPayMethod = FieldGenerator::choice('weddingPayMethod')
			->setLabel($this->translator->trans('entities.wedding.fields.weddingPayMethod'))
			->setChoices([
				$this->translator->trans('entities.wedding.fields.payMethods.1') => $this->translator->trans('entities.wedding.fields.payMethods.1'),
				$this->translator->trans('entities.wedding.fields.payMethods.2') => $this->translator->trans('entities.wedding.fields.payMethods.2'),
				$this->translator->trans('entities.wedding.fields.payMethods.3') => $this->translator->trans('entities.wedding.fields.payMethods.3'),
				$this->translator->trans('entities.wedding.fields.payMethods.4') => $this->translator->trans('entities.wedding.fields.payMethods.4')
			])
			->setColumns(6);
		$weddingPayId = FieldGenerator::url('weddingPayId')
			->setLabel($this->translator->trans('entities.wedding.fields.weddingPayId'))
			->setColumns(6);
		$weddingCitationSended = FieldGenerator::switch('weddingCitationSended')
			->setLabel($this->translator->trans('entities.wedding.fields.weddingCitationSended'));
		$weddingSatisfaction = FieldGenerator::textarea('weddingSatisfaction')
			->setLabel($this->translator->trans('entities.wedding.fields.weddingSatisfaction'));

		$testingAddress = FieldGenerator::text('testingAddress')
			->setLabel($this->translator->trans('entities.wedding.fields.testingAddress'))
			->setColumns(6);
		$testingCity = FieldGenerator::text('testingCity')
			->setLabel($this->translator->trans('entities.wedding.fields.testingCity'))
			->setColumns(6);
		$testingFulladdress = FieldGenerator::text('testingFulladdress')
			->setLabel($this->translator->trans('entities.wedding.fields.testingFulladdress'));
		$testingDate = FieldGenerator::datetime('testingDate')
			->setLabel($this->translator->trans('entities.wedding.fields.testingDate'));
		$testingComments = FieldGenerator::textarea('testingComments')
			->setLabel($this->translator->trans('entities.wedding.fields.testingComments'));
		$testingDisplacementCost = FieldGenerator::float('testingDisplacementCost')
			->setLabel($this->translator->trans('entities.wedding.fields.testingDisplacementCost'))
			->setTemplatePath('field/price.html.twig')
			->setColumns(3);
		$testingPayMethod = FieldGenerator::choice('testingPayMethod')
			->setLabel($this->translator->trans('entities.wedding.fields.testingPayMethod'))
			->setChoices([
				$this->translator->trans('entities.wedding.fields.payMethods.1') => $this->translator->trans('entities.wedding.fields.payMethods.1'),
				$this->translator->trans('entities.wedding.fields.payMethods.2') => $this->translator->trans('entities.wedding.fields.payMethods.2'),
				$this->translator->trans('entities.wedding.fields.payMethods.3') => $this->translator->trans('entities.wedding.fields.payMethods.3'),
				$this->translator->trans('entities.wedding.fields.payMethods.4') => $this->translator->trans('entities.wedding.fields.payMethods.4')
			])
			->setColumns(6);
		$testingPayId = FieldGenerator::url('testingPayId')
			->setLabel($this->translator->trans('entities.wedding.fields.testingPayId'))
			->setColumns(6);
		$testingCitationSended = FieldGenerator::switch('testingCitationSended')
			->setLabel($this->translator->trans('entities.wedding.fields.testingCitationSended'));
		$testingSign = FieldGenerator::text('testingSign')
			->setLabel($this->translator->trans('entities.wedding.fields.testingSign'))
			->setTemplatePath('field/sign.html.twig');

		$meetingTime = FieldGenerator::text('meetingTime')
			->setLabel($this->translator->trans('entities.wedding.fields.meetingTime'))
			->setFormTypeOption('attr.data-ea-time-field', 'true')
			->addWebpackEncoreEntries('form-type-time')
			->setColumns(6);
		$urlFB = FieldGenerator::text('urlFB')
			->setLabel($this->translator->trans('entities.wedding.fields.urlFB'))
			->setColumns(6);
		$urlIG = FieldGenerator::text('urlIG')
			->setLabel($this->translator->trans('entities.wedding.fields.urlIG'))
			->setColumns(6);
		$requestModify = FieldGenerator::textarea('requestModify')
			->setLabel($this->translator->trans('entities.wedding.fields.requestModify'));
		$creationDate = FieldGenerator::datetime('creationDate')
			->setLabel($this->translator->trans('entities.wedding.fields.creationDate'));
		$weddingServices = FieldGenerator::collection('weddingServices')
			->setLabel($this->translator->trans('entities.weddingService.plural'))
			->setTemplatePath('field/weddingServices.html.twig');

		$tel1 = FieldGenerator::phone('client.tel1')
			->setLabel($this->translator->trans('entities.user.fields.tel1'));
		$tel2 = FieldGenerator::phone('client.tel2')
			->setLabel($this->translator->trans('entities.user.fields.tel2'));
		$email = FieldGenerator::email('client.email')
			->setLabel($this->translator->trans('entities.user.fields.email'));
		$clientConocePor = FieldGenerator::text('client.clientConocePor')
			->setLabel($this->translator->trans('entities.user.fields.clientConocePor'));

		$image1 = FieldGenerator::media('image1')
			->setLabel($this->translator->trans('entities.wedding.fields.image'))
			->setFormTypeOption('attr.data-type', 'public_bodas');
		$image2 = FieldGenerator::media('image2')
			->setLabel($this->translator->trans('entities.wedding.fields.image'))
			->setFormTypeOption('attr.data-type', 'public_bodas');
		$image3 = FieldGenerator::media('image3')
			->setLabel($this->translator->trans('entities.wedding.fields.image'))
			->setFormTypeOption('attr.data-type', 'public_bodas');
		$image4 = FieldGenerator::media('image4')
			->setLabel($this->translator->trans('entities.wedding.fields.image'))
			->setFormTypeOption('attr.data-type', 'public_bodas');
		$image5 = FieldGenerator::media('image5')
			->setLabel($this->translator->trans('entities.wedding.fields.image'))
			->setFormTypeOption('attr.data-type', 'public_bodas');
		$image6 = FieldGenerator::media('image6')
			->setLabel($this->translator->trans('entities.wedding.fields.image'))
			->setFormTypeOption('attr.data-type', 'public_bodas');
		$tipo = FieldGenerator::choice('tipo')
			->setLabel($this->translator->trans('entities.wedding.fields.tipo'))
			->setChoices([
				$this->translator->trans('entities.wedding.fields.tipos.1') => 1,
				$this->translator->trans('entities.wedding.fields.tipos.2') => 2
			])->setColumns(6);

		if ($pageName == Crud::PAGE_INDEX) {
			yield $client;
			yield $weddingFulladdress;
			yield $testingDate;
			yield $weddingDate;
		} else if ($pageName == Crud::PAGE_DETAIL) {
			yield $dataPanel;
			yield $client;
			yield $tipo;
			yield $tel1;
			yield $tel2;
			yield $email;
			yield $clientConocePor;
			yield $urlFB;
			yield $urlIG;
			yield $planner;
			yield $dataWeddingPanel;
			yield $weddingDate;
			yield $meetingTime;
			yield $weddingProvince;
			yield $weddingCity;
			yield $weddingAddress;
			yield $weddingComments;
			//yield $weddingDisplacementCost;
			yield $weddingPayMethod;
			yield $weddingPayId;
			yield $weddingSatisfaction;
			yield $dataTestingPanel;
			yield $testingDate;
			yield $testingCity;
			yield $testingAddress;
			yield $testingComments;
			//yield $testingDisplacementCost;
			yield $testingPayMethod;
			yield $testingPayId;
			yield $testingSign;
			yield $dataImagesPanel;
			yield $image1;
			yield $image2;
			yield $image3;
			yield $image4;
			yield $image5;
			yield $image6;
			yield $dataServicesPanel;
			yield $weddingServices;
			yield $requestModify;
		} else if ($pageName == Crud::PAGE_NEW) {
			yield $dataPanel;
			yield $client;
			yield $tipo;
			yield $urlFB;
			yield $urlIG;
			yield $planner;
			yield $dataWeddingPanel;
			yield $weddingDate;
			yield $meetingTime;
			yield $weddingProvince;
			yield $weddingCity;
			yield $weddingAddress;
			yield $weddingComments;
			//yield $weddingDisplacementCost;
			yield $weddingPayMethod;
			yield $weddingPayId;
			yield $weddingSatisfaction;
			yield $dataTestingPanel;
			yield $testingDate;
			yield $testingCity;
			yield $testingAddress;
			yield $testingComments;
			//yield $testingDisplacementCost;
			yield $testingPayMethod;
			yield $testingPayId;
			yield $dataServicesPanel;
			yield $requestModify;
			yield $dataImagesPanel;
			yield $image1;
			yield $image2;
			yield $image3;
			yield $image4;
			yield $image5;
			yield $image6;
		} else if ($pageName == Crud::PAGE_EDIT) {
			yield $dataPanel;
			yield $client;
			yield $tipo;
			yield $urlFB;
			yield $urlIG;
			yield $planner;
			yield $dataWeddingPanel;
			yield $weddingDate;
			yield $meetingTime;
			yield $weddingProvince;
			yield $weddingCity;
			yield $weddingAddress;
			yield $weddingComments;
			//yield $weddingDisplacementCost;
			yield $weddingPayMethod;
			yield $weddingPayId;
			yield $weddingSatisfaction;
			yield $dataTestingPanel;
			yield $testingDate;
			yield $testingCity;
			yield $testingAddress;
			yield $testingComments;
			//yield $testingDisplacementCost;
			yield $testingPayMethod;
			yield $testingPayId;
			yield $dataServicesPanel;
			yield $requestModify;
			yield $dataImagesPanel;
			yield $image1;
			yield $image2;
			yield $image3;
			yield $image4;
			yield $image5;
			yield $image6;
		}
	}

	public function configureActions(Actions $actions): Actions
	{
		if (!$this->getUser()->hasPermission('entityWedding') && $this->getUser()->getRole()->getName() != "ROLE_PROFESSIONAL") {
			$actions = Actions::new();
		} else {
			$actions->remove(Crud::PAGE_INDEX, Action::NEW);

			$weddingServicesList = Action::new('weddingServicesList', $this->translator->trans('ea.actions.weddingServices'), 'icon ti ti-server')->linkToUrl(function (Wedding $entity) {
				return $this->adminUrlGenerator->setAction(Action::EDIT)->setEntityId($entity->getId())->setController(WeddingServiceCrudController::class)->generateUrl();
			});
			$weddingServicesDetail = Action::new('weddingServicesDetail', $this->translator->trans('ea.actions.weddingServices'), 'icon ti ti-server')->linkToUrl(function (Wedding $entity) {
				return $this->adminUrlGenerator->setAction(Action::EDIT)->setEntityId($entity->getId())->setController(WeddingServiceCrudController::class)->generateUrl();
			})->addCssClass('btn btn-outline-secondary');

			$testingSignList = Action::new('testingSignList', $this->translator->trans('ea.actions.testingSign'), 'icon ti ti-pencil')
				->linkToCrudAction('testingSignAction')
				->displayIf(static function ($entity) {
					$now = new \DateTime('now');
					$signatureEnabled = $entity->getTestingDate() ? ($now->format('d-m-Y') == $entity->getTestingDate()->format('d-m-Y')) : false;
					return $signatureEnabled && $entity->getTestingSign() == null;
				});
			$testingSignDetail = Action::new('testingSignDetail', $this->translator->trans('ea.actions.testingSign'), 'icon ti ti-pencil')
				->linkToCrudAction('testingSignAction')
				->addCssClass('btn btn-outline-secondary')
				->displayIf(static function ($entity) {
					$now = new \DateTime('now');
					$signatureEnabled = $entity->getTestingDate() ? ($now->format('d-m-Y') == $entity->getTestingDate()->format('d-m-Y')) : false;
					return $signatureEnabled && $entity->getTestingSign() == null;
				});
			if ($this->getUser()->getRole()->getName() != "ROLE_PROFESSIONAL") {
				$actions->add(Crud::PAGE_INDEX, $weddingServicesList);
				$actions->add(Crud::PAGE_DETAIL, $weddingServicesDetail);
				$actions->add(Crud::PAGE_EDIT, $weddingServicesDetail);
			} else {
				$actions->remove(Crud::PAGE_INDEX, Action::DELETE);
				$actions->remove(Crud::PAGE_DETAIL, Action::DELETE);
				$actions->remove(Crud::PAGE_EDIT, Action::DELETE);
			}
			$actions->add(Crud::PAGE_INDEX, $testingSignList);
			$actions->add(Crud::PAGE_DETAIL, $testingSignDetail);
			$actions->add(Crud::PAGE_EDIT, $testingSignDetail);
			if ($this->getUser()->getRole()->getName() != "ROLE_PROFESSIONAL") {
				$actions->reorder(Crud::PAGE_INDEX, ['weddingServicesList', 'testingSignList', Action::DETAIL, Action::EDIT, Action::DELETE]);
				$actions->reorder(Crud::PAGE_DETAIL, [Action::INDEX, Action::DELETE, 'weddingServicesDetail', 'testingSignDetail', Action::EDIT]);
				$actions->reorder(Crud::PAGE_EDIT, [Action::INDEX, Action::DELETE, 'weddingServicesDetail', 'testingSignDetail', Action::SAVE_AND_RETURN]);
			} else {
				$actions->reorder(Crud::PAGE_INDEX, ['testingSignList', Action::DETAIL, Action::EDIT]);
				$actions->reorder(Crud::PAGE_DETAIL, [Action::INDEX, 'testingSignDetail', Action::EDIT]);
				$actions->reorder(Crud::PAGE_EDIT, [Action::INDEX, 'testingSignDetail', Action::SAVE_AND_RETURN]);
			}
		}

		$actions->add(Crud::PAGE_INDEX, Action::new('export', $this->translator->trans('ea.actions.downloadAsCSV'))
			->setIcon('icon ti ti-download')
			->linkToCrudAction('exportAction')
			->createAsGlobalAction()
		);

		return $actions;
	}
    
    public function configureFilters(Filters $filters): Filters
    {
        $filters->add(TextFilter::new('weddingCity', $this->translator->trans('entities.wedding.fields.weddingCity')));
        $filters->add(TextFilter::new('testingCity', $this->translator->trans('entities.wedding.fields.testingCity')));
        $filters->add(DateTimeFilter::new('weddingDate', $this->translator->trans('entities.wedding.fields.weddingDate')));
        $filters->add(DateTimeFilter::new('testingDate', $this->translator->trans('entities.wedding.fields.testingDate')));
        $filters->add(WeddingProfessionalFilter::new('professional', $this->translator->trans('entities.professional.singular')));

        return $filters;
    }

	public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
	{
		$now = new \Datetime('now');
		$response = $this->container->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters)
			->andWhere('entity.weddingDate < :now')->setParameter('now', $now->setTime(0, 0));

		return $response;
	}

	public function exportAction(Request $request)
	{
		$context = $request->attributes->get(EA::CONTEXT_REQUEST_ATTRIBUTE);
		$fields = array();
		$entity = $this->em->getRepository($this->getEntityFqcn())->findOneBy(array(), array('id' => 'DESC'));
		if ($entity) {
			$arrEntity = (array) $entity; 
			foreach ($arrEntity as $k => $v) {
				$fields[] = preg_replace('/[\x00-\x1F\x7F]/u', '', str_replace($this->getEntityFqcn(), '', $k));
			}
		}
		array_splice($fields, 2, 0, array('client.email'));
		$fields = FieldCollection::new($fields);
		$filters = $this->container->get(FilterFactory::class)->create($context->getCrud()->getFiltersConfig(), $fields, $context->getEntity());
		$entities = $this->createIndexQueryBuilder($context->getSearch(), $context->getEntity(), $fields, $filters)->getQuery()->getResult();
		$data = $this->csvService->getEntityAsData($entities, $fields);
		$entityName = $this->translator->trans('entities.wedding.plural');
		return $this->csvService->export($data, $entityName . ' - ' . date_create()->format('Y-m-d_H-i-s') . '.csv');
	}

	public function testingSignAction(Request $request)
	{
		$wedding = $this->em->getRepository($this->getEntityFqcn())->find($request->get('entityId'));

		return $this->render('admin/testingSign.html.twig', [
			'wedding' => $wedding
		]);
	}
}
