<?php

namespace App\Controller\Admin\Cruds;

use App\Entity\User;
use App\Field\FieldGenerator;
use App\Service\CsvService;

use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Config\Option\EA;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Factory\FilterFactory;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

class WeddingPlannerCrudController extends AbstractCrudController
{
	private $em;
	private $translator;
	private $adminUrlGenerator;
	private $csvService;
	private $passwordHasher;

	public function __construct(EntityManagerInterface $em, TranslatorInterface $translator, AdminUrlGenerator $adminUrlGenerator, CsvService $csvService, UserPasswordHasherInterface $passwordHasher)
	{
		$this->em = $em;
		$this->translator = $translator;
		$this->adminUrlGenerator = $adminUrlGenerator;
		$this->csvService = $csvService;
		$this->passwordHasher = $passwordHasher;
	}

	public static function getEntityFqcn(): string
	{
		return User::class;
	}

	public function configureCrud(Crud $crud): Crud
	{
		$crud->setEntityLabelInSingular($this->translator->trans('entities.weddingPlanner.singular'));
		$crud->setEntityLabelInPlural($this->translator->trans('entities.weddingPlanner.plural'));
		$crud->setDefaultSort(['name' => 'ASC', 'lastname' => 'ASC']);

		$entityId = filter_input(INPUT_GET, EA::ENTITY_ID, FILTER_SANITIZE_URL);
		$entity = $entityId ? $this->em->getRepository($this->getEntityFqcn())->find($entityId) : null;
		if ($entity) {
			$crud->setPageTitle(Crud::PAGE_DETAIL, $this->translator->trans('entities.weddingPlanner.singular') . ': ' . $entity);
			$crud->setPageTitle(Crud::PAGE_EDIT, $this->translator->trans('ea.titles.edit', [
				'%entity_label_singular%' => $this->translator->trans('entities.weddingPlanner.singular') . ': ' . $entity
			]));
		}

		return $crud;
	}

	public function configureFields(string $pageName): iterable
	{
		$session = $this->container->get('request_stack')->getSession();

		$dataPanel = FieldGenerator::panel($this->translator->trans('entities.weddingPlanner.sections.data'))
			->setIcon('fas fa-fw fa-user-clock');
		$username = FieldGenerator::text('username')
			->setLabel($this->translator->trans('entities.user.fields.username'));
		$name = FieldGenerator::text('name')
			->setLabel($this->translator->trans('entities.user.fields.name'))
			->setColumns(6);
		$lastname = FieldGenerator::text('lastname')
			->setLabel($this->translator->trans('entities.user.fields.lastname'))
			->setColumns(6);
		$fullname = FieldGenerator::text('fullname')
			->setLabel($this->translator->trans('entities.user.fields.fullname'));
		$tel1 = FieldGenerator::phone('tel1')
			->setLabel($this->translator->trans('entities.user.fields.tel1'))
			->setColumns(6);
		$tel2 = FieldGenerator::phone('tel2')
			->setLabel($this->translator->trans('entities.user.fields.tel2'))
			->setColumns(6);
		$email = FieldGenerator::email('email')
			->setLabel($this->translator->trans('entities.user.fields.email'));
		$role = FieldGenerator::association('role')
			->setLabel($this->translator->trans('entities.role.singular'))
			->setQueryBuilder(function ($queryBuilder) {
				return $queryBuilder
					->andWhere("entity.name = 'ROLE_WEDDINGPLANNER'");
			})
			->setRequired(true);

		$passwordPanel = FieldGenerator::panel($this->translator->trans('entities.user.sections.password'))
			->setIcon('fas fa-fw fa-key');
		$password = FieldGenerator::repeat('plainPassword')
			->setFormTypeOptions([
				'type' => PasswordType::class,
				'first_options' => [
					'label' => $this->translator->trans('entities.user.fields.password')
				],
				'second_options' => [
					'label' => $this->translator->trans('entities.user.fields.repeatPassword')
				]
			]);

		if ($pageName == Crud::PAGE_INDEX) {
			if ($session->get('config')->enableUsername) {
				yield $username;
			}
			yield $fullname;
			yield $email;
		} else if ($pageName == Crud::PAGE_DETAIL) {
			yield $dataPanel;
			if ($session->get('config')->enableUsername) {
				yield $username;
			}
			yield $fullname;
			yield $tel1;
			yield $tel2;
			yield $email;
		} else if ($pageName == Crud::PAGE_NEW) {
			yield $dataPanel;
			if ($session->get('config')->enableUsername) {
				yield $username;
			}
			yield $name;
			yield $lastname;
			yield $tel1;
			yield $tel2;
			yield $email;
			yield $role;
			yield $passwordPanel;
			yield $password->setRequired(true);
		} else if ($pageName == Crud::PAGE_EDIT) {
			yield $dataPanel;
			if ($session->get('config')->enableUsername) {
				yield $username;
			}
			yield $name;
			yield $lastname;
			yield $tel1;
			yield $tel2;
			yield $email;
			yield $passwordPanel;
			yield $password->setRequired(false);
		}
	}

	public function configureActions(Actions $actions): Actions
	{
		if (!$this->getUser()->hasPermission('entityWeddingPlanner')) {
			$actions = Actions::new();
		}

		$actions->add(Crud::PAGE_INDEX, Action::new('export', $this->translator->trans('ea.actions.downloadAsCSV'))
			->setIcon('icon ti ti-download')
			->linkToCrudAction('exportAction')
			->createAsGlobalAction()
		);

		return $actions;
	}

	public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
	{
		$response = $this->container->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters)
			->leftJoin('entity.role', 'r')
			->andWhere("r.name = 'ROLE_WEDDINGPLANNER'");

		return $response;
	}

	public function createEditFormBuilder(EntityDto $entityDto, KeyValueStore $keyValueStore, AdminContext $context): FormBuilderInterface
	{
		$session = $this->container->get('request_stack')->getSession();

		$formBuilder = parent::createEditFormBuilder($entityDto, $keyValueStore, $context);
		$this->addEncodePasswordEventListener($formBuilder);
		if (!$session->get('config')->enableUsername) {
			$this->addUsernameEventListener($formBuilder);
		}

		return $formBuilder;
	}

	public function createNewFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
	{
		$session = $this->container->get('request_stack')->getSession();

		$formBuilder = parent::createNewFormBuilder($entityDto, $formOptions, $context);
		$this->addEncodePasswordEventListener($formBuilder);
		if (!$session->get('config')->enableUsername) {
			$this->addUsernameEventListener($formBuilder);
		}

		return $formBuilder;
	}

	public function addEncodePasswordEventListener(FormBuilderInterface $formBuilder)
	{
		$formBuilder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
			$user = $event->getData();
			if ($user->getPlainPassword()) {
				$user->setPassword($this->passwordHasher->hashPassword($user, $user->getPlainPassword()));
			}
		});
	}

	public function addUsernameEventListener(FormBuilderInterface $formBuilder)
	{
		$formBuilder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
			$user = $event->getData();
			$user->setUsername($user->getEmail());
		});
	}

	public function exportAction(Request $request)
	{
		$context = $request->attributes->get(EA::CONTEXT_REQUEST_ATTRIBUTE);
		$fields = array();
		$entity = $this->em->getRepository($this->getEntityFqcn())->findOneBy(array(), array('id' => 'DESC'));
		if ($entity) {
			$arrEntity = (array) $entity; 
			foreach ($arrEntity as $k => $v) {
				$fields[] = preg_replace('/[\x00-\x1F\x7F]/u', '', str_replace($this->getEntityFqcn(), '', $k));
			}
		}
		$fields = FieldCollection::new($fields);
		$filters = $this->container->get(FilterFactory::class)->create($context->getCrud()->getFiltersConfig(), $fields, $context->getEntity());
		$entities = $this->createIndexQueryBuilder($context->getSearch(), $context->getEntity(), $fields, $filters)->getQuery()->getResult();
		$data = $this->csvService->getEntityAsData($entities, $fields);
		$entityName = $this->translator->trans('entities.weddingPlanner.plural');
		return $this->csvService->export($data, $entityName . ' - ' . date_create()->format('Y-m-d_H-i-s') . '.csv');
	}
}
