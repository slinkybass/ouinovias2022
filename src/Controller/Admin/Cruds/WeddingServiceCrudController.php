<?php

namespace App\Controller\Admin\Cruds;

use App\Entity\Email;
use App\Entity\TemplateEmail;
use App\Entity\User;
use App\Entity\Wedding;
use App\Entity\WeddingService;
use App\Field\FieldGenerator;
use App\Form\Type\WeddingServiceType;
use App\Service\CsvService;

use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Option\EA;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Factory\FilterFactory;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

use Doctrine\ORM\EntityManagerInterface;

class WeddingServiceCrudController extends AbstractCrudController
{
	private $em;
	private $translator;
	private $adminUrlGenerator;
	private $csvService;

	public function __construct(EntityManagerInterface $em, TranslatorInterface $translator, AdminUrlGenerator $adminUrlGenerator, CsvService $csvService)
	{
		$this->em = $em;
		$this->translator = $translator;
		$this->adminUrlGenerator = $adminUrlGenerator;
		$this->csvService = $csvService;
	}

	public static function getEntityFqcn(): string
	{
		return Wedding::class;
	}

	public function configureCrud(Crud $crud): Crud
	{
		$crud->setEntityLabelInSingular($this->translator->trans('entities.weddingService.singular'));
		$crud->setEntityLabelInPlural($this->translator->trans('entities.weddingService.plural'));
		$crud->setDefaultSort(['creationDate' => 'ASC']);

		$entityId = filter_input(INPUT_GET, EA::ENTITY_ID, FILTER_SANITIZE_URL);
		$entity = $entityId ? $this->em->getRepository($this->getEntityFqcn())->find($entityId) : null;
		if ($entity) {
			$crud->setPageTitle(Crud::PAGE_DETAIL, $this->translator->trans('entities.weddingService.singular') . ': ' . $entity);
			$crud->setPageTitle(Crud::PAGE_EDIT, $this->translator->trans('ea.titles.edit', [
				'%entity_label_singular%' => $this->translator->trans('entities.weddingService.singular') . ': ' . $entity
			]));
		}

		return $crud;
	}

	public function configureFields(string $pageName): iterable
	{
		$dataPanel = FieldGenerator::panel($this->translator->trans('entities.weddingService.sections.data'))
			->setIcon('fas fa-fw fa-circle-check');

		$weddingServices = FieldGenerator::collection('weddingServices')
			->setLabel($this->translator->trans('entities.weddingService.plural'))
			->setEntryType(WeddingServiceType::class)
			->setEntryIsComplex(true);

		if ($pageName == Crud::PAGE_EDIT) {
			yield $dataPanel;
			yield $weddingServices;
		}
	}

	public function configureActions(Actions $actions): Actions
	{
		if (!$this->getUser()->hasPermission('entityWedding')) {
			$actions = Actions::new();
		} else {
			$actions->remove(Crud::PAGE_EDIT, Action::INDEX);
			$actions->remove(Crud::PAGE_EDIT, Action::DELETE);
		}

		$actions->add(Crud::PAGE_INDEX, Action::new('export', $this->translator->trans('ea.actions.downloadAsCSV'))
			->setIcon('icon ti ti-download')
			->linkToCrudAction('exportAction')
			->createAsGlobalAction()
		);

		return $actions;
	}

	public function createEditFormBuilder(EntityDto $entityDto, KeyValueStore $keyValueStore, AdminContext $context): FormBuilderInterface
	{
		$formBuilder = parent::createEditFormBuilder($entityDto, $keyValueStore, $context);
		$this->sendAlertsEventListener($formBuilder);

		return $formBuilder;
	}

	public function createNewFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
	{
		$formBuilder = parent::createNewFormBuilder($entityDto, $formOptions, $context);
		$this->sendAlertsEventListener($formBuilder);

		return $formBuilder;
	}

	public function sendAlertsEventListener(FormBuilderInterface $formBuilder)
	{
		$formBuilder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
			$weddingServices = $this->em->getRepository(WeddingService::class)->findBy([
				'wedding' => $event->getData()->getId()
			]);

			$afterServices = array();
			foreach ($event->getData()->getWeddingServices()->getValues() as $weddingService) {
				$afterServices[$weddingService->getProfessional()->getId()][$weddingService->getService()->getId()] = $weddingService->getCount();
			}
			$beforeServices = array();
			foreach ($weddingServices as $weddingService) {
				$beforeServices[$weddingService->getProfessional()->getId()][$weddingService->getService()->getId()] = $weddingService->getCount();
			}

			$addedServices = array();
			$removedServices = array();
			foreach ($afterServices as $p => $services) {
				if (!array_key_exists($p, $beforeServices)) {
					$addedServices[] = $p;
				} else {
					foreach ($services as $s => $q) {
						if (!array_key_exists($s, $beforeServices[$p])) {
							$addedServices[] = $p;
						}
					}
				}
			}
			foreach ($beforeServices as $p => $services) {
				if (!array_key_exists($p, $afterServices)) {
					$removedServices[] = $p;
				} else {
					foreach ($services as $s => $q) {
						if (!array_key_exists($s, $afterServices[$p])) {
							$removedServices[] = $p;
						}
					}
				}
			}
			$addedServices = array_unique($addedServices);
			$removedServices = array_unique($removedServices);

			$addedServicesEmails = array();
			$removedServicesEmails = array();
			foreach ($addedServices as $addedService => $p) {
				$professional = $this->em->getRepository(User::class)->find($p);
				$addedServicesEmails[] = $professional->getEmail();
			}
			foreach ($removedServices as $removedService => $p) {
				$professional = $this->em->getRepository(User::class)->find($p);
				$removedServicesEmails[] = $professional->getEmail();
			}

			if (count($addedServicesEmails)) {
				$session = $this->container->get('request_stack')->getSession();
				$title = $this->translator->trans('ea.actions.serviceAdded');
				$templateEntity = $this->em->getRepository(TemplateEmail::class)->findOneBy(['type' => 3]);
				$content = $templateEntity->getTemplate();
				$content = str_replace('{{ date }}', $event->getData()->getWeddingDate()->format('d-m-Y'), $content);
				$content = str_replace("{{ url }}", $this->container->get('request_stack')->getCurrentRequest()->getSchemeAndHttpHost(), $content);
				$emailsCC = array();
				$emailsCCO = $session->get('configApp')->emailCCO ? explode(',', $session->get('configApp')->emailCCO) : [];
				$email = $this->em->getRepository(Email::class)->sendEmail($title, $content, $addedServicesEmails, $emailsCC, $emailsCCO);
				$this->em->persist($email);
			}

			if (count($removedServicesEmails)) {
				$session = $this->container->get('request_stack')->getSession();
				$title = $this->translator->trans('ea.actions.serviceRemoved');
				$templateEntity = $this->em->getRepository(TemplateEmail::class)->findOneBy(['type' => 4]);
				$content = $templateEntity->getTemplate();
				$content = str_replace('{{ date }}', $event->getData()->getWeddingDate()->format('d-m-Y'), $content);
				$content = str_replace("{{ url }}", $this->container->get('request_stack')->getCurrentRequest()->getSchemeAndHttpHost(), $content);
				$emailsCC = array();
				$emailsCCO = $session->get('configApp')->emailCCO ? explode(',', $session->get('configApp')->emailCCO) : [];
				$email = $this->em->getRepository(Email::class)->sendEmail($title, $content, $removedServicesEmails, $emailsCC, $emailsCCO);
				$this->em->persist($email);
			}
		});
	}

	public function exportAction(Request $request)
	{
		$context = $request->attributes->get(EA::CONTEXT_REQUEST_ATTRIBUTE);
		$fields = array();
		$entity = $this->em->getRepository($this->getEntityFqcn())->findOneBy(array(), array('id' => 'DESC'));
		if ($entity) {
			$arrEntity = (array) $entity; 
			foreach ($arrEntity as $k => $v) {
				$fields[] = preg_replace('/[\x00-\x1F\x7F]/u', '', str_replace($this->getEntityFqcn(), '', $k));
			}
		}
		$fields = FieldCollection::new($fields);
		$filters = $this->container->get(FilterFactory::class)->create($context->getCrud()->getFiltersConfig(), $fields, $context->getEntity());
		$entities = $this->createIndexQueryBuilder($context->getSearch(), $context->getEntity(), $fields, $filters)->getQuery()->getResult();
		$data = $this->csvService->getEntityAsData($entities, $fields);
		$entityName = $this->translator->trans('entities.wedding.plural');
		return $this->csvService->export($data, $entityName . ' - ' . date_create()->format('Y-m-d_H-i-s') . '.csv');
	}

	public function edit(\EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext $context)
	{
		$redirect = parent::edit($context);
		if ($redirect instanceof \Symfony\Component\HttpFoundation\RedirectResponse) {
			$entityId = filter_input(INPUT_GET, EA::ENTITY_ID, FILTER_SANITIZE_URL);
			if ($entityId) {
				$wedding = $this->em->getRepository($this->getEntityFqcn())->find($entityId);
	
				$url = $this->adminUrlGenerator;
				$now = new \DateTime('now');
				if ($wedding->getWeddingDate() < $now) {
					$url = $url->setController(WeddingPastCrudController::class);
				} else {
					$url = $url->setController(WeddingCrudController::class);
				}
				$url = $url->setAction(Crud::PAGE_INDEX)
					->generateUrl();
	
				return $this->redirect($url);
			}
		}
		return $redirect;
	}
}
