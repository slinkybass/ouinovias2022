<?php

namespace App\Controller\Admin;

use App\Entity\Billing;
use App\Entity\Company;
use App\Entity\Config;
use App\Entity\ConfigApp;
use App\Entity\Email;
use App\Entity\Event;
use App\Entity\DisponibilityTesting;
use App\Entity\Rate;
use App\Entity\Request;
use App\Entity\Role;
use App\Entity\Service;
use App\Entity\TemplateEmail;
use App\Entity\TemplateContract;
use App\Entity\User;
use App\Entity\Wedding;

use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

use Doctrine\ORM\EntityManagerInterface;

class DashboardController extends AbstractDashboardController
{
	private $em;
	private $translator;

	public function __construct(EntityManagerInterface $em, TranslatorInterface $translator)
	{
		$this->em = $em;
		$this->translator = $translator;
	}

	/**
	 * @Route("/admin", name="admin")
	 */
	public function index(): Response
	{
		$routeBuilder = $this->container->get(AdminUrlGenerator::class);

		return $this->redirect($routeBuilder->setRoute('admin_home')->generateUrl());
	}

	public function configureDashboard(): Dashboard
	{
		$session = $this->container->get('request_stack')->getSession();

		$dashboard = Dashboard::new();

		$dashboard->setTitle('<img src="' . $session->get('config')->appLogo . '" class="mx-auto d-block">');
		$dashboard->setFaviconPath($session->get('config')->appFavicon);

		return $dashboard;
	}

	public function configureAssets(): Assets
	{
		$assets = Assets::new();

		$assets->addWebpackEncoreEntry('app');
		$assets->addWebpackEncoreEntry('admin');

		return $assets;
	}

	public function configureCrud(): Crud
	{
		$crud = Crud::new();

		$crud->setDefaultSort(['id' => 'DESC']);

		$crud->setPageTitle(Crud::PAGE_INDEX, $this->translator->trans('ea.titles.list'));
		$crud->setPageTitle(Crud::PAGE_DETAIL, $this->translator->trans('ea.titles.detail'));
		$crud->setPageTitle(Crud::PAGE_NEW, $this->translator->trans('ea.titles.new'));
		$crud->setPageTitle(Crud::PAGE_EDIT, $this->translator->trans('ea.titles.edit'));

		$crud->showEntityActionsInlined(true);
		$crud->setDateFormat('dd/MM/yyyy');
		$crud->setTimeFormat('HH:mm');
		$crud->setDateTimeFormat('dd/MM/yyyy HH:mm');

		return $crud;
	}

	public function configureMenuItems(): iterable
	{
		$config = $this->em->getRepository(Config::class)->findOneBy(array(), array('id' => 'DESC'));
		$configApp = $this->em->getRepository(ConfigApp::class)->findOneBy(array(), array('id' => 'DESC'));
		$session = $this->container->get('request_stack')->getSession();

		if ($this->getUser()->hasPermission('entityRequest')) {
			yield MenuItem::linkToRoute($this->translator->trans('ea.home.title'), 'fas fa-fw fa-home', 'admin_home');
		}
		if ($this->getUser()->hasPermission('entityMedia')) {
			yield MenuItem::linkToRoute($this->translator->trans('ea.media.title'), 'fas fa-fw fa-file', 'admin_media');
		}

		if ($this->getUser()->hasPermission('entityRequest')) {
			$requestItems = [
				MenuItem::linkToCrud($this->translator->trans('entities.request.plural'), 'fas fa-fw fa-circle-question', Request::class)->setController(Cruds\RequestCrudController::class),
				MenuItem::linkToCrud($this->translator->trans('entities.requestAnswered.pluralSimply'), 'fas fa-fw fa-reply-all', Request::class)->setController(Cruds\RequestAnsweredCrudController::class),
				MenuItem::linkToCrud($this->translator->trans('entities.requestValidated.pluralSimply'), 'fas fa-fw fa-square-check', Request::class)->setController(Cruds\RequestValidatedCrudController::class),
				MenuItem::linkToCrud($this->translator->trans('entities.requestPending.pluralSimply'), 'fas fa-fw fa-exclamation-circle', Request::class)->setController(Cruds\RequestPendingCrudController::class),
				MenuItem::linkToCrud($this->translator->trans('entities.requestAccepted.pluralSimply'), 'fas fa-fw fa-check', Request::class)->setController(Cruds\RequestAcceptedCrudController::class),
				MenuItem::linkToCrud($this->translator->trans('entities.requestDenied.pluralSimply'), 'fas fa-fw fa-times', Request::class)->setController(Cruds\RequestDeniedCrudController::class)
			];
			yield MenuItem::subMenu($this->translator->trans('entities.request.plural'), 'fas fa-fw fa-circle-question')->setSubItems($requestItems);
		}
		if ($this->getUser()->hasPermission('entityWedding')) {
			yield MenuItem::linkToCrud($this->translator->trans('entities.wedding.plural'), 'fas fa-fw fa-circle-check', Wedding::class)->setController(Cruds\WeddingCrudController::class);
			yield MenuItem::linkToCrud($this->translator->trans('entities.wedding.history'), 'fas fa-fw fa-circle-check', Wedding::class)->setController(Cruds\WeddingPastCrudController::class);
		}
		if ($this->getUser()->hasPermission('entityService')) {
			yield MenuItem::linkToCrud($this->translator->trans('entities.service.plural'), 'fas fa-fw fa-server', Service::class);
		}
		if ($this->getUser()->hasPermission('entityRate')) {
			yield MenuItem::linkToCrud($this->translator->trans('entities.rate.plural'), 'fas fa-fw fa-dollar-sign', Rate::class);
		}
		if ($this->getUser()->hasPermission('entityCompany')) {
			yield MenuItem::linkToCrud($this->translator->trans('entities.company.plural'), 'fas fa-fw fa-building', Company::class);
		}

		$userItems = array();
		if ($session->get('config')->enablePublic && $this->getUser()->hasPermission('entityUser')) {
			$userItems[] = MenuItem::linkToCrud($this->translator->trans('entities.client.plural'), 'fas fa-fw fa-user', User::class)->setController(Cruds\UserCrudController::class);
		}
		if ($this->getUser()->hasPermission('entityWeddingPlanner')) {
			$userItems[] = MenuItem::linkToCrud($this->translator->trans('entities.weddingPlanner.plural'), 'fas fa-fw fa-user-clock', User::class)->setController(Cruds\WeddingPlannerCrudController::class);
		}
		if ($this->getUser()->hasPermission('entityProfessional')) {
			$userItems[] = MenuItem::linkToCrud($this->translator->trans('entities.professional.plural'), 'fas fa-fw fa-user-tie', User::class)->setController(Cruds\ProfessionalCrudController::class);
			$userItems[] = MenuItem::linkToCrud($this->translator->trans('entities.professional.pluralInactive'), 'fas fa-fw fa-user-tie', User::class)->setController(Cruds\ProfessionalInactiveCrudController::class);
		}
		if ($this->getUser()->hasPermission('entityAdmin')) {
			if ($session->get('config')->enablePublic) {
				$userItems[] = MenuItem::linkToCrud($this->translator->trans('entities.admin.plural'), 'fas fa-fw fa-user-secret', User::class)->setController(Cruds\AdminCrudController::class);
			} else {
				$userItems[] = MenuItem::linkToCrud($this->translator->trans('entities.user.plural'), 'fas fa-fw fa-user', User::class)->setController(Cruds\AdminCrudController::class);
			}
		}
		if ($this->getUser()->hasPermission('entityRole')) {
			$userItems[] = MenuItem::linkToCrud($this->translator->trans('entities.role.plural'), 'fas fa-fw fa-lock', Role::class);
		}
		if (count($userItems) == 1) {
			yield $userItems[0];
		} elseif (count($userItems) > 1) {
			yield MenuItem::subMenu($this->translator->trans('entities.user.plural'), 'fas fa-fw fa-users')->setSubItems($userItems);
		}

		if ($this->getUser()->hasPermission('sectionDisponibility')) {
			yield MenuItem::linkToRoute($this->translator->trans('ea.disponibility.title'), 'fas fa-fw fa-calendar-xmark', 'admin_disponibility');
		}
		if ($this->getUser()->hasPermission('sectionCalendar')) {
			yield MenuItem::linkToRoute($this->translator->trans('ea.calendar.title'), 'fas fa-fw fa-calendar-days', 'admin_calendar');
		}
		if ($this->getUser()->hasPermission('entityBilling')) {
			yield MenuItem::subMenu($this->translator->trans('entities.billing.plural'), 'fas fa-fw fa-file-invoice-dollar')->setSubItems([
				MenuItem::linkToCrud($this->translator->trans('entities.billing.pluralPending'), 'fas fa-fw fa-file-invoice-dollar', Billing::class)->setController(Cruds\BillingPendingCrudController::class),
				MenuItem::linkToCrud($this->translator->trans('entities.billing.pluralPaid'), 'fas fa-fw fa-file-invoice-dollar', Billing::class)->setController(Cruds\BillingCrudController::class),
			]);
		}
		if ($this->getUser()->hasPermission('entityDisponibilityTesting')) {
			yield MenuItem::linkToCrud($this->translator->trans('entities.disponibilityTesting.plural'), 'fas fa-fw fa-calendar-day', DisponibilityTesting::class);
		}
		if ($this->getUser()->hasPermission('entityEvent')) {
			yield MenuItem::linkToCrud($this->translator->trans('entities.event.plural'), 'fas fa-fw fa-calendar-day', Event::class);
		}
		if ($this->getUser()->hasPermission('sectionBilling')) {
			yield MenuItem::linkToRoute($this->translator->trans('ea.billing.title'), 'fas fa-fw fa-file-text', 'admin_billing');
		}
		if ($this->getUser()->hasPermission('sectionContract')) {
			yield MenuItem::linkToRoute($this->translator->trans('ea.contract.title'), 'fas fa-fw fa-file', 'admin_contract');
		}
		if ($this->getUser()->hasPermission('entityEmail')) {
			yield MenuItem::linkToCrud($this->translator->trans('entities.email.plural'), 'fas fa-fw fa-envelope', Email::class);
		}
		if ($this->getUser()->hasPermission('entityTemplateEmail')) {
			yield MenuItem::linkToCrud($this->translator->trans('entities.templateEmail.plural'), 'fas fa-fw fa-object-group', TemplateEmail::class);
		}
		if ($this->getUser()->hasPermission('entityTemplateContract')) {
			yield MenuItem::linkToCrud($this->translator->trans('entities.templateContract.plural'), 'fas fa-fw fa-object-group', TemplateContract::class);
		}
		if ($this->getUser()->hasPermission('sectionReports')) {
			yield MenuItem::subMenu($this->translator->trans('ea.reports.title'), 'fas fa-fw fa-file-lines')->setSubItems([
				MenuItem::linkToRoute($this->translator->trans('ea.reports.year.title'), 'fas fa-fw fa-calendar', 'admin_reports_year'),
				MenuItem::linkToRoute($this->translator->trans('ea.reports.creationDate.title'), 'fas fa-fw fa-calendar', 'admin_reports_creationDate'),
				MenuItem::linkToRoute($this->translator->trans('ea.reports.professional.title'), 'fas fa-fw fa-user-tie', 'admin_reports_professional'),
				MenuItem::linkToRoute($this->translator->trans('ea.reports.denyReason.title'), 'fas fa-fw fa-times', 'admin_reports_denyReason'),
			]);
		}

		if ($this->getUser()->hasPermission('entityConfig')) {
			$configLink = MenuItem::linkToCrud($this->translator->trans('entities.config.singular'), 'fas fa-fw fa-cog', Config::class);
			$configLink = $config ? $configLink->setAction(Crud::PAGE_DETAIL)->setEntityId($config->getId()) : $configLink->setAction(Crud::PAGE_NEW);
			yield $configLink;
		}
		if ($this->getUser()->hasPermission('entityConfigApp')) {
			$configAppLink = MenuItem::linkToCrud($this->translator->trans('entities.configApp.singular'), 'fas fa-fw fa-cog', ConfigApp::class);
			$configAppLink = $configApp ? $configAppLink->setAction(Crud::PAGE_DETAIL)->setEntityId($configApp->getId()) : $configAppLink->setAction(Crud::PAGE_NEW);
			yield $configAppLink;
		}
	}

	public function configureUserMenu(UserInterface $user): UserMenu
	{
		$userMenu = parent::configureUserMenu($user);

		$menuItems = array();
		$menuItems[] = MenuItem::linkToCrud($this->translator->trans('ea.profile.title'), 'icon ti ti-user-circle', User::class)
			->setController($this->isGranted('ROLE_PROFESSIONAL') ? Cruds\ProfessionalCrudController::class : Cruds\AdminCrudController::class)
			->setAction(Crud::PAGE_EDIT)
			->setEntityId($user->getId());
		$menuItems[] = MenuItem::section();
		$menuItems[] = MenuItem::linkToLogout('__ea__user.sign_out', 'icon ti ti-logout');
		$userMenu->setMenuItems($menuItems);

		return $userMenu;
	}

	public function configureActions(): Actions
	{
		$actions = Actions::new();

		$actions->addBatchAction(Action::BATCH_DELETE);
		$actions->update(Crud::PAGE_INDEX, Action::BATCH_DELETE, function (Action $action) {return $action->setIcon('icon ti ti-trash')->setCssClass('action-batchDelete btn btn-danger pr-0 text-white');});

		$actions->add(Crud::PAGE_INDEX, Action::NEW);
		$actions->add(Crud::PAGE_INDEX, Action::DETAIL);
		$actions->add(Crud::PAGE_INDEX, Action::EDIT);
		$actions->add(Crud::PAGE_INDEX, Action::DELETE);
		$actions->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {return $action->setIcon('icon ti ti-plus');});
		$actions->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {return $action->setIcon('icon ti ti-eye');});
		$actions->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {return $action->setIcon('icon ti ti-edit');});
		$actions->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {return $action->setIcon('icon ti ti-trash');});

		$actions->add(Crud::PAGE_NEW, Action::INDEX);
		$actions->add(Crud::PAGE_NEW, Action::SAVE_AND_RETURN);
		$actions->update(Crud::PAGE_NEW, Action::INDEX, function (Action $action) {return $action->setIcon('icon ti ti-chevron-left');});
		$actions->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {return $action->setIcon('icon ti ti-device-floppy')->addCssClass('btn-success text-white');});

		$actions->add(Crud::PAGE_DETAIL, Action::INDEX);
		$actions->add(Crud::PAGE_DETAIL, Action::DELETE);
		$actions->add(Crud::PAGE_DETAIL, Action::EDIT);
		$actions->update(Crud::PAGE_DETAIL, Action::INDEX, function (Action $action) {return $action->setIcon('icon ti ti-chevron-left');});
		$actions->update(Crud::PAGE_DETAIL, Action::DELETE, function (Action $action) {return $action->setIcon('icon ti ti-trash')->setCssClass('action-delete btn btn-danger pr-0 text-white');});
		$actions->update(Crud::PAGE_DETAIL, Action::EDIT, function (Action $action) {return $action->setIcon('icon ti ti-edit');});

		$actions->add(Crud::PAGE_EDIT, Action::INDEX);
		$actions->add(Crud::PAGE_EDIT, Action::DELETE);
		$actions->add(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN);
		$actions->update(Crud::PAGE_EDIT, Action::INDEX, function (Action $action) {return $action->setIcon('icon ti ti-chevron-left');});
		$actions->update(Crud::PAGE_EDIT, Action::DELETE, function (Action $action) {return $action->setIcon('icon ti ti-trash')->setCssClass('action-delete btn btn-danger pr-0 text-white');});
		$actions->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN, function (Action $action) {return $action->setIcon('icon ti ti-device-floppy')->addCssClass('btn-success text-white');});
			
		$actions->reorder(Crud::PAGE_INDEX, [Action::DETAIL, Action::EDIT, Action::DELETE]);
		$actions->reorder(Crud::PAGE_NEW, [Action::INDEX, Action::SAVE_AND_RETURN]);
		$actions->reorder(Crud::PAGE_DETAIL, [Action::INDEX, Action::DELETE, Action::EDIT]);
		$actions->reorder(Crud::PAGE_EDIT, [Action::INDEX, Action::DELETE, Action::SAVE_AND_RETURN]);

		return $actions;
	}
}
