<?php

namespace App\Controller\Admin\Filters;

use EasyCorp\Bundle\EasyAdminBundle\Contracts\Filter\FilterInterface;
use EasyCorp\Bundle\EasyAdminBundle\Filter\FilterTrait;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\FieldDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\FilterDataDto;
use EasyCorp\Bundle\EasyAdminBundle\Form\Filter\Type\EntityFilterType;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\EntityRepository;

use App\Entity\User;

class WeddingProfessionalFilter implements FilterInterface
{
    use FilterTrait;

    public static function new(string $propertyName, $label = null): self
    {
        return (new self())
            ->setFilterFqcn(__CLASS__)
            ->setProperty($propertyName)
            ->setLabel($label)
            ->setFormType(EntityFilterType::class)
			->setFormTypeOption('value_type_options.class', User::class)
			->setFormTypeOption('value_type_options.query_builder', function (EntityRepository $er) {
				return $er->createQueryBuilder('u')
					->leftJoin('u.role', 'r')
					->andWhere("r.name = 'ROLE_PROFESSIONAL'")
					->andWhere("u.active = true");
			});
    }

    public function apply(QueryBuilder $queryBuilder, FilterDataDto $filterDataDto, ?FieldDto $fieldDto, EntityDto $entityDto): void
    {
        $comparison = $filterDataDto->getComparison();
        $value = $filterDataDto->getValue();

        $queryBuilder
			->leftJoin('entity.weddingServices', 'ws')
			->andWhere("ws.professional " . $comparison . " :professional")
			->setParameter('professional', $value);
    }
}