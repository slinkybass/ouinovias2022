<?php

namespace App\Controller;

use App\Controller\Admin\Cruds\EventCrudController;
use App\Controller\Admin\Cruds\WeddingCrudController;
use App\Controller\Admin\Cruds\WeddingPastCrudController;
use App\Entity\Billing;
use App\Entity\Email;
use App\Entity\Event;
use App\Entity\Province;
use App\Entity\Request as EntityRequest;
use App\Entity\RateService;
use App\Entity\TemplateContract;
use App\Entity\TemplateEmail;
use App\Entity\User;
use App\Entity\Wedding;
use App\Field\FieldGenerator;
use App\Form\FormGenerator;

use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

use Doctrine\ORM\EntityManagerInterface;

class AdminController extends AbstractController
{
	private $em;
	private $translator;
	private $adminUrlGenerator;

	public function __construct(EntityManagerInterface $em, TranslatorInterface $translator, AdminUrlGenerator $adminUrlGenerator)
	{
		$this->em = $em;
		$this->translator = $translator;
		$this->adminUrlGenerator = $adminUrlGenerator;
	}

	/**
	 * @Route("/admin/home", name="admin_home")
	 */
	public function admin_home(): Response
	{
		if (!$this->getUser()->hasPermission('entityRequest')) {
			$url = $this->adminUrlGenerator
				->setRoute('admin_calendar')
				->generateUrl();
			return $this->redirect($url);
		}
		$now = new \DateTime('now');
		$now = $now->setTime(23, 59, 59);
		$fourteenDaysFuture = clone $now;
		$fourteenDaysFuture = $fourteenDaysFuture->modify('+14 day');
		$sevenDaysFuture = clone $now;
		$sevenDaysFuture = $sevenDaysFuture->modify('+7 day');

		$requests = $this->em->getRepository(EntityRequest::class)->createQueryBuilder('r')
			->andWhere('r.answered = false')
			->getQuery()->getResult();
		$requestsAnswered = $this->em->getRepository(EntityRequest::class)->createQueryBuilder('r')
			->andWhere('r.answered = true')
			->andWhere('r.validated IS NULL')
			->andWhere('r.callDateAnswered <= :date')
			->setParameter('date', $now)
			->getQuery()->getResult();
		$requestsValidated = $this->em->getRepository(EntityRequest::class)->createQueryBuilder('r')
			->andWhere('r.validated = true')
			->andWhere('r.pending IS NULL')
			->andWhere('r.callDateValidated <= :date')
			->setParameter('date', $now)
			->getQuery()->getResult();
		$weddingsCitationTesting = $this->em->getRepository(Wedding::class)->createQueryBuilder('w')
			->andWhere('w.testingCitationSended = false')
			->andWhere('w.testingDate >= :date')
			->andWhere('w.testingDate <= :fourteenDaysFuture')
			->setParameter('date', $now)
			->setParameter('fourteenDaysFuture', $fourteenDaysFuture)
			->getQuery()->getResult();
		$weddingsCitationWedding = $this->em->getRepository(Wedding::class)->createQueryBuilder('w')
			->andWhere('w.weddingCitationSended = false')
			->andWhere('w.weddingDate >= :date')
			->andWhere('w.weddingDate <= :fourteenDaysFuture')
			->setParameter('date', $now)
			->setParameter('fourteenDaysFuture', $fourteenDaysFuture)
			->getQuery()->getResult();
		$testingsPendientPayment = $this->em->getRepository(Wedding::class)->createQueryBuilder('w')
			->andWhere('w.testingPayId IS NULL')
			->andWhere('w.testingDate >= :date')
			->andWhere('w.testingDate <= :sevenDaysFuture')
			->setParameter('date', $now)
			->setParameter('sevenDaysFuture', $sevenDaysFuture)
			->getQuery()->getResult();
		$weddingsPendientPayment = $this->em->getRepository(Wedding::class)->createQueryBuilder('w')
			->andWhere('w.weddingPayId IS NULL')
			->andWhere('w.weddingDate >= :date')
			->andWhere('w.weddingDate <= :sevenDaysFuture')
			->setParameter('date', $now)
			->setParameter('sevenDaysFuture', $sevenDaysFuture)
			->getQuery()->getResult();

		return $this->render('admin/home.html.twig', [
			'requests' => $requests,
			'requestsAnswered' => $requestsAnswered,
			'requestsValidated' => $requestsValidated,
			'weddingsCitationTesting' => $weddingsCitationTesting,
			'weddingsCitationWedding' => $weddingsCitationWedding,
			'testingsPendientPayment' => $testingsPendientPayment,
			'weddingsPendientPayment' => $weddingsPendientPayment,
		]);
	}

	/**
	 * @Route("/admin/media", name="admin_media")
	 */
	public function admin_media(): Response
	{
		return $this->render('admin/media.html.twig');
	}

	/**
	 * @Route("/admin/disponibility", name="admin_disponibility")
	 */
	public function admin_disponibility(Request $request): Response
	{
		$form = $this->getFormDisponibility();
		$professionals = null;

		if ($request->isMethod('POST')) {
			$form->handleRequest($request);
			if ($form->isValid()) {
				$professionals = array();
				$date = $form['date']->getData();
				$province = $form['province']->getData();

				$professionalsEntities = $this->em->getRepository(User::class)->createQueryBuilder('u')
					->leftJoin('u.role', 'r')
					->andWhere("r.name = 'ROLE_PROFESSIONAL'")
					->andWhere("u.active = true");
				if ($province) {
					$professionalsEntities = $professionalsEntities
						->leftJoin('u.professionalProvinces', 'p')
						->andWhere('p = :province')
						->setParameter('province', $province);
				}
				$professionalsEntities = $professionalsEntities->getQuery()->getResult();

				$date = $date->format('Y-m-d');
				$start = date('Y-m-d H:i:s', strtotime($date . ' 00:00:00'));
				$end = date('Y-m-d H:i:s', strtotime($date . ' 23:59:59'));
				foreach ($professionalsEntities as $professional) {
					$newProfessional = new \stdClass();
					$newProfessional->id = $professional->getId();
					$newProfessional->type = $professional->getProfessionalType();
		
					// Checkbox label
					$hasWedding = $this->em->getRepository(Wedding::class)->createQueryBuilder('w')
						->join('w.weddingServices', 'ws')
						->where('ws.professional = :professional')
						->andWhere('w.weddingDate BETWEEN :start AND :end')
						->setParameter('professional', $professional)
						->setParameter('start', $start)
						->setParameter('end', $end)
						->setMaxResults(1)
						->getQuery()->getResult();
					$hasTesting = $this->em->getRepository(Wedding::class)->createQueryBuilder('w')
						->join('w.weddingServices', 'ws')
						->where('ws.professional = :professional')
						->andWhere('w.testingDate BETWEEN :start AND :end')
						->setParameter('professional', $professional)
						->setParameter('start', $start)
						->setParameter('end', $end)
						->setMaxResults(1)
						->getQuery()->getResult();
					$hasEvent = $this->em->getRepository(Event::class)->createQueryBuilder('e')
						->where('e.professional = :professional')
						->andWhere('e.start <= :end')
						->andWhere('e.end >= :start')
						->setParameter('professional', $professional)
						->setParameter('end', $end)
						->setParameter('start', $start)
						->setMaxResults(1)
						->getQuery()->getResult();
		
					$label = $professional->getFullName();
					$newProfessional->occupied = false;
					if (count($hasWedding) || count($hasTesting) || count($hasEvent)) {
						$newProfessional->occupied = true;
						$label .= ' <small class="text-warning">';
						if (count($hasWedding)) {
							foreach ($hasWedding as $wedding) {
								$label .= '<i class="icon ti ti-alert-triangle ms-1"></i>';
								$label .= $this->translator->trans('ea.validateRequest.hasWedding') . $wedding->getWeddingDate()->format('H:i') . ' (' . $wedding->getWeddingLocation() . ')';
							}
						}
						if (count($hasTesting)) {
							foreach ($hasTesting as $testing) {
								$label .= '<i class="icon ti ti-alert-triangle ms-1"></i>';
								$label .= $this->translator->trans('ea.validateRequest.hasTesting') . $testing->getTestingDate()->format('H:i') . ' (' . $testing->getTestingCity() . ')';
							}
						}
						if (count($hasEvent)) {
							foreach ($hasEvent as $event) {
								$label .= '<i class="icon ti ti-alert-triangle ms-1"></i>';
								$label .= $this->translator->trans('ea.validateRequest.hasEvent') . $event->getRangeStr();
							}
						}
						$label .= '</small>';
					}
					$newProfessional->label = $label;
		
					// Email label
					$newProfessional->content = '';
		
					$name = $professional->getFullname();
					if ($professional->getProfessionalURL()) {
						$name = '<a href="' . $professional->getProfessionalURL() . '">' . $name . '</a>';
					}
		
					$rate = null;
					if ($professional->getProfessionalRate()) {
						$rate = $professional->getProfessionalRate()->getName();
						if ($professional->getProfessionalRate()->getUrl()) {
							$rate = '<a href="' . $professional->getProfessionalRate()->getUrl() . '">' . $rate . '</a>';
						}
					}
		
					$newProfessional->content = $name;
					$newProfessional->content .= $rate ? ' - (' . $rate . ')' : null;
		
					$professionals[$professional->getId()] = $newProfessional;
				}
			}
		}

		return $this->render('admin/disponibility.html.twig', [
			'form' => $form->createView(),
			'professionals' => $professionals
		]);
	}

	/**
	 * @Route("/admin/calendar", name="admin_calendar")
	 */
	public function admin_calendar(Request $request): Response
	{
		
		return $this->render('admin/calendar.html.twig');
	}

	/**
	 * @Route("/admin/api/calendar", name="admin_calendar_api")
	 */
	public function admin_calendar_api(Request $request): Response
	{
		$data = array();
		$dataWeddings = array();
		$dataTestings = array();
		$dataEvents = array();

		$seeAll = !$this->isGranted('ROLE_PROFESSIONAL');
		$start = $request->get('start');
		$end = $request->get('end');
		$data = explode(',', $request->get('data'));

		if ($start && $end) {
			if ($seeAll) {
				$weddings = $this->em->getRepository(Wedding::class)->createQueryBuilder('w')
					->where('w.weddingDate BETWEEN :start AND :end')
					->setParameter('start', $start)
					->setParameter('end', $end)
					->getQuery()->getResult();

				$testings = $this->em->getRepository(Wedding::class)->createQueryBuilder('w')
					->where('w.testingDate BETWEEN :start AND :end')
					->setParameter('start', $start)
					->setParameter('end', $end)
					->getQuery()->getResult();

				$events = $this->em->getRepository(Event::class)->createQueryBuilder('e')
					->andWhere('e.start <= :end')
					->andWhere('e.end >= :start')
					->setParameter('end', $end)
					->setParameter('start', $start)
					->getQuery()->getResult();
			} else {
				$weddings = $this->em->getRepository(Wedding::class)->createQueryBuilder('w')
					->join('w.weddingServices', 'ws')
					->where('w.weddingDate BETWEEN :start AND :end')
					->andWhere('ws.professional = :user')
					->setParameter('start', $start)
					->setParameter('end', $end)
					->setParameter('user', $this->getUser())
					->getQuery()->getResult();

				$testings = $this->em->getRepository(Wedding::class)->createQueryBuilder('w')
					->join('w.weddingServices', 'ws')
					->where('w.testingDate BETWEEN :start AND :end')
					->andWhere('ws.professional = :user')
					->setParameter('start', $start)
					->setParameter('end', $end)
					->setParameter('user', $this->getUser())
					->getQuery()->getResult();

				$events = $this->em->getRepository(Event::class)->createQueryBuilder('e')
					->andWhere('e.start <= :end')
					->andWhere('e.end >= :start')
					->andWhere('e.professional = :user')
					->setParameter('end', $end)
					->setParameter('start', $start)
					->setParameter('user', $this->getUser())
					->getQuery()->getResult();
			}

			if (in_array('weddings', $data)) {
				$dataWeddings = array_map(function ($wedding) {
					$stdWedding = new \stdClass();

					$stdWedding->id = $wedding->getId();
					$stdWedding->title = $wedding->getClient()->getFullname();
					$stdWedding->fulltitle = $wedding->getClient()->getFullname() . '<br /><small class="text-muted">' . $wedding->getWeddingFulladdress() . '</small>';
					$stdWedding->color = '#800080';

					if ($wedding->getMeetingTime()) {
						$time = $wedding->getMeetingTime();
						$time .= strlen($time) == 5 ? ':00' : null;
						$weddingDate = $wedding->getWeddingDate()->format('Y-m-d') . ' ' . $time;
					} else {
						$weddingDate = $wedding->getWeddingDate()->format('Y-m-d H:i:s');
					}
					$stdWedding->start = $weddingDate;

					$stdWedding->description = '';
					$first = true;
					foreach ($wedding->getWeddingServices() as $wservice) {
						if ($wservice->getProfessional()->getProfessionalRate()) {
							$rateService = $this->em->getRepository(RateService::class)->createQueryBuilder('rs')
								->where('rs.rate = :rate')
								->andWhere('rs.service = :service')
								->setParameter('rate', $wservice->getProfessional()->getProfessionalRate())
								->setParameter('service', $wservice->getService())
								->getQuery()->getOneOrNullResult();
							if ($rateService && $rateService->getPriceWedding()) {
								if (!$first) {
									$stdWedding->description .= '<br /><br />';
								} else {
									$first = false;
								}
								$stdWedding->description .= '<b>' . $wservice->getCount() . ' ' . $wservice->getService() . '</b>';
								$stdWedding->description .= '<br />' . $wservice->getProfessional();
								$stdWedding->description .= $wservice->getLocation() ?  '<br /><span class="text-muted">' . $wservice->getLocation() . '</span>' : null;
							}
						}
					}

					$now = new \DateTime('now');
					if ($wedding->getWeddingDate() > $now->setTime(0, 0)) {
						$url = $this->adminUrlGenerator->setController(WeddingCrudController::class);
					} else {
						$url = $this->adminUrlGenerator->setController(WeddingPastCrudController::class);
					}
					$url = $url->setAction(Crud::PAGE_DETAIL)->setEntityId($wedding->getId())->generateUrl();
					$stdWedding->url = $url;

					return $stdWedding;
				}, $weddings);
			}

			if (in_array('testings', $data)) {
				$dataTestings = array_map(function ($wedding) {
					$stdTesting = new \stdClass();

					$stdTesting->id = $wedding->getId();
					$stdTesting->title = $wedding->getClient()->getFullname();
					$stdTesting->fulltitle = $wedding->getClient()->getFullname() . '<br /><small class="text-muted">' . $wedding->getTestingFulladdress() . '</small>';
					$stdTesting->color = '#3A87AD';

					$stdTesting->start = $wedding->getTestingDate()->format('Y-m-d H:i:s');

					$stdTesting->description = '';
					$first = true;
					foreach ($wedding->getWeddingServices() as $wservice) {
						if ($wservice->getProfessional()->getProfessionalRate()) {
							$rateService = $this->em->getRepository(RateService::class)->createQueryBuilder('rs')
								->where('rs.rate = :rate')
								->andWhere('rs.service = :service')
								->setParameter('rate', $wservice->getProfessional()->getProfessionalRate())
								->setParameter('service', $wservice->getService())
								->getQuery()->getOneOrNullResult();
							if ($rateService && $rateService->getPriceTesting()) {
								if (!$first) {
									$stdTesting->description .= '<br /><br />';
								} else {
									$first = false;
								}
								$stdTesting->description .= '<b>' . $wservice->getCount() . ' ' . $wservice->getService() . '</b>';
								$stdTesting->description .= '<br />' . $wservice->getProfessional();
								$stdTesting->description .= $wservice->getLocation() ?  '<br /><span class="text-muted">' . $wservice->getLocation() . '</span>' : null;
							}
						}
					}

					$now = new \DateTime('now');
					if ($wedding->getWeddingDate() > $now->setTime(0, 0)) {
						$url = $this->adminUrlGenerator->setController(WeddingCrudController::class);
					} else {
						$url = $this->adminUrlGenerator->setController(WeddingPastCrudController::class);
					}
					$url = $url->setAction(Crud::PAGE_DETAIL)->setEntityId($wedding->getId())->generateUrl();
					$stdTesting->url = $url;

					return $stdTesting;
				}, $testings);
			}

			if (in_array('events', $data)) {
				$dataEvents = array_map(function ($event) {
					$stdEvent = new \stdClass();

					$stdEvent->id = $event->getId();
					$stdEvent->title = $event->getProfessional()->getFullname();
					$stdEvent->fulltitle = $event->getStart()->format('d-m-Y H:i') . ' - ' . $event->getEnd()->format('d-m-Y H:i');
					$stdEvent->color = '#666';

					$stdEvent->start = $event->getStart()->format('Y-m-d H:i:s');
					$stdEvent->end = $event->getEnd()->format('Y-m-d H:i:s');

					$stdEvent->description = $event->getComments() ?? '';

					$url = $this->adminUrlGenerator->setController(EventCrudController::class)
						->setAction(Crud::PAGE_DETAIL)->setEntityId($event->getId())->generateUrl();
					$stdEvent->url = $url;

					return $stdEvent;
				}, $events);
			}
		}

		$data = array_merge($dataWeddings, $dataTestings, $dataEvents);

		return $this->json($data, 200);
	}

	/**
	 * @Route("/calendar/{id}", name="calendar")
	 */
	public function calendar(Request $request, string $id): Response
	{
		$user = $this->em->getRepository(User::class)->find($id);
		$calendarEvents = array();

		if ($user) {
			$seeAll = $user ? !in_array('ROLE_PROFESSIONAL', $user->getRoles()) : false;
			$now = new \DateTime('now');
			$start = clone $now;
			$end = clone $now;
			$start = $start->modify('-6 months');
			$end = $end->modify('+1 year');

			if ($seeAll) {
				$weddings = $this->em->getRepository(Wedding::class)->createQueryBuilder('w')
					->where('w.weddingDate BETWEEN :start AND :end')
					->orderBy('w.weddingDate', 'ASC')
					->setParameter('start', $start)
					->setParameter('end', $end)
					->getQuery()->getResult();

				$testings = $this->em->getRepository(Wedding::class)->createQueryBuilder('w')
					->where('w.testingDate BETWEEN :start AND :end')
					->orderBy('w.testingDate', 'ASC')
					->setParameter('start', $start)
					->setParameter('end', $end)
					->getQuery()->getResult();
			} else {
				$weddings = $this->em->getRepository(Wedding::class)->createQueryBuilder('w')
					->join('w.weddingServices', 'ws')
					->where('w.weddingDate BETWEEN :start AND :end')
					->andWhere('ws.professional = :user')
					->setParameter('start', $start)
					->setParameter('end', $end)
					->setParameter('user', $user)
					->orderBy('w.weddingDate', 'ASC')
					->getQuery()->getResult();

				$testings = $this->em->getRepository(Wedding::class)->createQueryBuilder('w')
					->join('w.weddingServices', 'ws')
					->where('w.testingDate BETWEEN :start AND :end')
					->andWhere('ws.professional = :user')
					->setParameter('start', $start)
					->setParameter('end', $end)
					->setParameter('user', $user)
					->orderBy('w.testingDate', 'ASC')
					->getQuery()->getResult();
			}

			foreach ($weddings as $wedding) {
				$type = 'Boda';
				$title = $wedding->getClient()->getFullname();
				$location = $wedding->getWeddingFulladdress() ?? '';
				if ($wedding->getMeetingTime()) {
					$time = $wedding->getMeetingTime();
					$time .= strlen($time) == 5 ? ':00' : null;
					$date = new \Eluceo\iCal\Domain\ValueObject\Date(\DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $wedding->getWeddingDate()->format('Y-m-d') . ' ' . $time));
				} else {
					$date = new \Eluceo\iCal\Domain\ValueObject\Date(\DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $wedding->getWeddingDate()->format('Y-m-d H:i:s')));
				}
				$description = '';
				$first = true;
				foreach ($wedding->getWeddingServices() as $wservice) {
					if ($wservice->getProfessional()->getProfessionalRate()) {
						$rateService = $this->em->getRepository(RateService::class)->createQueryBuilder('rs')
							->where('rs.rate = :rate')
							->andWhere('rs.service = :service')
							->setParameter('rate', $wservice->getProfessional()->getProfessionalRate())
							->setParameter('service', $wservice->getService())
							->getQuery()->getOneOrNullResult();
						if ($rateService && $rateService->getPriceWedding()) {
							if (!$first) {
								$description .= '. ';
							} else {
								$first = false;
							}
							$description .= $wservice->getCount() . ' ' . $wservice->getService();
							$description .= ': ' . $wservice->getProfessional();
						}
					}
				}
				
				$calendarEvent = (new \Eluceo\iCal\Domain\Entity\Event(new \Eluceo\iCal\Domain\ValueObject\UniqueIdentifier('w_' . $wedding->getId())))
					->setSummary($type . ' - ' . $title)
					->setDescription($description)
					->setLocation(new \Eluceo\iCal\Domain\ValueObject\Location($location))
					->setOccurrence(new \Eluceo\iCal\Domain\ValueObject\SingleDay($date));
				$calendarEvents[] = $calendarEvent;
			}
			foreach ($testings as $wedding) {
				$type = 'Prueba';
				$title = $wedding->getClient()->getFullname();
				$location = $wedding->getTestingFulladdress() ?? '';
				$date = new \Eluceo\iCal\Domain\ValueObject\Date(\DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $wedding->getTestingDate()->format('Y-m-d H:i:s')));
				$description = '';
				$first = true;
				foreach ($wedding->getWeddingServices() as $wservice) {
					if ($wservice->getProfessional()->getProfessionalRate()) {
						$rateService = $this->em->getRepository(RateService::class)->createQueryBuilder('rs')
							->where('rs.rate = :rate')
							->andWhere('rs.service = :service')
							->setParameter('rate', $wservice->getProfessional()->getProfessionalRate())
							->setParameter('service', $wservice->getService())
							->getQuery()->getOneOrNullResult();
						if ($rateService && $rateService->getPriceTesting()) {
							if (!$first) {
								$description .= '. ';
							} else {
								$first = false;
							}
							$description .= $wservice->getCount() . ' ' . $wservice->getService();
							$description .= ': ' . $wservice->getProfessional();
						}
					}
				}

				$calendarEvent = (new \Eluceo\iCal\Domain\Entity\Event(new \Eluceo\iCal\Domain\ValueObject\UniqueIdentifier('t_' . $wedding->getId())))
					->setSummary($type . ' - ' . $title)
					->setDescription($description)
					->setLocation(new \Eluceo\iCal\Domain\ValueObject\Location($location))
					->setOccurrence(new \Eluceo\iCal\Domain\ValueObject\SingleDay($date));
				$calendarEvents[] = $calendarEvent;
			}
		}

		$calendar = new \Eluceo\iCal\Domain\Entity\Calendar($calendarEvents);

		$componentFactory = new \Eluceo\iCal\Presentation\Factory\CalendarFactory();
		$calendarComponent = $componentFactory->createCalendar($calendar);
		
		//header('Content-Type: text/calendar; charset=utf-8');
		//header('Content-Disposition: attachment; filename="cal.ics"');

		echo $calendarComponent;
		die();
	}

	/**
	 * @Route("/admin/reports/year", name="admin_reports_year")
	 */
	public function admin_reports_year(Request $request): Response
	{
		$form = $this->getFormReportsYear();
		$data = null;

		if ($request->isMethod('POST')) {
			$form->handleRequest($request);
			if ($form->isValid()) {
				$tipo = $form['tipo']->getData();
				$year = $form['year']->getData();
				$start = date('Y-m-d H:i:s', strtotime($year . '-01-01 00:00:00'));
				$end = date('Y-m-d H:i:s', strtotime($year . '-12-31 23:59:59'));
				
				$weddings = $this->em->getRepository(Wedding::class)->createQueryBuilder('w')
					->andWhere('w.weddingDate BETWEEN :start AND :end')
					->setParameter('start', $start)
					->setParameter('end', $end);
				if ($tipo) {
					$weddings = $weddings->andWhere('w.tipo = :tipo')->setParameter('tipo', $tipo);
				}
				$weddings = $weddings->getQuery()->getResult();

				$data = new \stdClass;
				$data->weddings = 0;
				$data->services = [];
				$data->bill = 0;
				$data->months = [];
				foreach ($weddings as $wedding) {
					$month = $wedding->getWeddingDate()->format('n');
					if (!array_key_exists($month, $data->months)) {
						$data->months[$month] = new \stdClass;
						$data->months[$month]->weddings = 0;
						$data->months[$month]->services = [];
						$data->months[$month]->bill = 0;
					}
					// Weddings per Year
					$data->weddings += 1;
					// Weddings per Month
					$data->months[$month]->weddings += 1;

					foreach ($wedding->getWeddingServices() as $wservice) {
						if ($wservice->getProfessional()->getProfessionalRate()) {
							$rateService = $this->em->getRepository(RateService::class)->createQueryBuilder('rs')
								->where('rs.rate = :rate')
								->andWhere('rs.service = :service')
								->setParameter('rate', $wservice->getProfessional()->getProfessionalRate())
								->setParameter('service', $wservice->getService())
								->getQuery()->getOneOrNullResult();
							if ($rateService) {
								// Services per Year
								$serviceId = $wservice->getService()->getId();
								if (!array_key_exists($serviceId, $data->services)) {
									$data->services[$serviceId] = new \stdClass;
									$data->services[$serviceId]->name = $wservice->getService()->getName();
									$data->services[$serviceId]->count = $wservice->getCount();
									$data->services[$serviceId]->bill = ($rateService->getPriceWedding() + $rateService->getPriceTesting()) * $wservice->getCount();
								} else {
									$data->services[$serviceId]->count += $wservice->getCount();
									$data->services[$serviceId]->bill += ($rateService->getPriceWedding() + $rateService->getPriceTesting()) * $wservice->getCount();
								}
								// Services per Month
								if (!array_key_exists($serviceId, $data->months[$month]->services)) {
									$data->months[$month]->services[$serviceId] = new \stdClass;
									$data->months[$month]->services[$serviceId]->name = $wservice->getService()->getName();
									$data->months[$month]->services[$serviceId]->count = $wservice->getCount();
									$data->months[$month]->services[$serviceId]->bill = ($rateService->getPriceWedding() + $rateService->getPriceTesting()) * $wservice->getCount();
								} else {
									$data->months[$month]->services[$serviceId]->count += $wservice->getCount();
									$data->months[$month]->services[$serviceId]->bill += ($rateService->getPriceWedding() + $rateService->getPriceTesting()) * $wservice->getCount();
								}
								// Bill per Year
								$data->bill += ($rateService->getPriceWedding() + $rateService->getPriceTesting()) * $wservice->getCount();
								// Bill per Month
								$data->months[$month]->bill += ($rateService->getPriceWedding() + $rateService->getPriceTesting()) * $wservice->getCount();
							}
						}
					}
					ksort($data->months[$month]->services);
				}
				krsort($data->months);
				ksort($data->services);
			}
		}

		return $this->render('admin/reports/year.html.twig', [
			'form' => $form->createView(),
			'reports' => $data
		]);
	}

	/**
	 * @Route("/admin/reports/creationDate", name="admin_reports_creationDate")
	 */
	public function admin_reports_creationDate(Request $request): Response
	{
		$form = $this->getFormReportsCreationDate();
		$data = null;

		if ($request->isMethod('POST')) {
			$form->handleRequest($request);
			if ($form->isValid()) {
				$tipo = $form['tipo']->getData();
				$year = $form['year']->getData();
				$weddingProvince = $form['weddingProvince']->getData();
				$start = date('Y-m-d H:i:s', strtotime($year . '-01-01 00:00:00'));
				$end = date('Y-m-d H:i:s', strtotime($year . '-12-31 23:59:59'));
				
				$weddings = $this->em->getRepository(Wedding::class)->createQueryBuilder('w')
					->andWhere('w.creationDate BETWEEN :start AND :end')
					->setParameter('start', $start)
					->setParameter('end', $end);
				if ($weddingProvince) {
					$weddings = $weddings->andWhere('w.weddingProvince = :weddingProvince')->setParameter('weddingProvince', $weddingProvince);
				}
				if ($tipo) {
					$weddings = $weddings->andWhere('w.tipo = :tipo')->setParameter('tipo', $tipo);
				}
				$weddings = $weddings->getQuery()->getResult();

				$data = new \stdClass;
				$data->weddings = 0;
				$data->services = [];
				$data->bill = 0;
				$data->months = [];
				foreach ($weddings as $wedding) {
					$month = $wedding->getCreationDate()->format('n');
					if (!array_key_exists($month, $data->months)) {
						$data->months[$month] = new \stdClass;
						$data->months[$month]->weddings = 0;
						$data->months[$month]->services = [];
						$data->months[$month]->bill = 0;
					}
					// Weddings per Year
					$data->weddings += 1;
					// Weddings per Month
					$data->months[$month]->weddings += 1;

					foreach ($wedding->getWeddingServices() as $wservice) {
						if ($wservice->getProfessional()->getProfessionalRate()) {
							$rateService = $this->em->getRepository(RateService::class)->createQueryBuilder('rs')
								->where('rs.rate = :rate')
								->andWhere('rs.service = :service')
								->setParameter('rate', $wservice->getProfessional()->getProfessionalRate())
								->setParameter('service', $wservice->getService())
								->getQuery()->getOneOrNullResult();
							if ($rateService) {
								// Services per Year
								$serviceId = $wservice->getService()->getId();
								if (!array_key_exists($serviceId, $data->services)) {
									$data->services[$serviceId] = new \stdClass;
									$data->services[$serviceId]->name = $wservice->getService()->getName();
									$data->services[$serviceId]->count = $wservice->getCount();
									$data->services[$serviceId]->bill = ($rateService->getPriceBooking()) * $wservice->getCount();
								} else {
									$data->services[$serviceId]->count += $wservice->getCount();
									$data->services[$serviceId]->bill += ($rateService->getPriceBooking()) * $wservice->getCount();
								}
								// Services per Month
								if (!array_key_exists($serviceId, $data->months[$month]->services)) {
									$data->months[$month]->services[$serviceId] = new \stdClass;
									$data->months[$month]->services[$serviceId]->name = $wservice->getService()->getName();
									$data->months[$month]->services[$serviceId]->count = $wservice->getCount();
									$data->months[$month]->services[$serviceId]->bill = ($rateService->getPriceBooking()) * $wservice->getCount();
								} else {
									$data->months[$month]->services[$serviceId]->count += $wservice->getCount();
									$data->months[$month]->services[$serviceId]->bill += ($rateService->getPriceBooking()) * $wservice->getCount();
								}
								// Bill per Year
								$data->bill += ($rateService->getPriceBooking()) * $wservice->getCount();
								// Bill per Month
								$data->months[$month]->bill += ($rateService->getPriceBooking()) * $wservice->getCount();
							}
						}
					}
					ksort($data->months[$month]->services);
				}
				krsort($data->months);
				ksort($data->services);
			}
		}

		return $this->render('admin/reports/creationDate.html.twig', [
			'form' => $form->createView(),
			'reports' => $data
		]);
	}

	/**
	 * @Route("/admin/reports/professional", name="admin_reports_professional")
	 */
	public function admin_reports_professional(Request $request): Response
	{
		$form = $this->getFormReportsProfessional();
		$data = null;

		if ($request->isMethod('POST')) {
			$form->handleRequest($request);
			if ($form->isValid()) {
				$tipo = $form['tipo']->getData();
				$professional = $form['professional']->getData();

				$data = new \stdClass;
				$data->professional = $professional->getFullName();
				$data->years = [];
				foreach ($professional->getWeddingservices() as $wservice) {
					if (!$tipo || ($tipo == $wservice->getWedding()->getTipo())) {
						if ($professional->getProfessionalRate()) {
							$year = $wservice->getWedding()->getWeddingDate()->format('Y');
							$month = $wservice->getWedding()->getWeddingDate()->format('n');
							if ($year && $month) {
								if (!array_key_exists($year, $data->years)) {
									$data->years[$year] = new \stdClass;
									$data->years[$year]->weddings = 0;
									$data->years[$year]->services = [];
									$data->years[$year]->bill = 0;
									$data->years[$year]->months = [];
								}
								if (!array_key_exists($month, $data->years[$year]->months)) {
									$data->years[$year]->months[$month] = new \stdClass;
									$data->years[$year]->months[$month]->weddings = 0;
									$data->years[$year]->months[$month]->services = [];
									$data->years[$year]->months[$month]->bill = 0;
								}
								// Weddings per Year
								$data->years[$year]->weddings += 1;
								// Weddings per Month
								$data->years[$year]->months[$month]->weddings += 1;

								$rateService = $this->em->getRepository(RateService::class)->createQueryBuilder('rs')
									->where('rs.rate = :rate')
									->andWhere('rs.service = :service')
									->setParameter('rate', $professional->getProfessionalRate())
									->setParameter('service', $wservice->getService())
									->getQuery()->getOneOrNullResult();
								if ($rateService) {
									// Services per Year
									$serviceId = $wservice->getService()->getId();
									if (!array_key_exists($serviceId, $data->years[$year]->services)) {
										$data->years[$year]->services[$serviceId] = new \stdClass;
										$data->years[$year]->services[$serviceId]->name = $wservice->getService()->getName();
										$data->years[$year]->services[$serviceId]->count = $wservice->getCount();
										$data->years[$year]->services[$serviceId]->bill = ($rateService->getPriceWedding() + $rateService->getPriceTesting()) * $wservice->getCount();
									} else {
										$data->years[$year]->services[$serviceId]->count += $wservice->getCount();
										$data->years[$year]->services[$serviceId]->bill += ($rateService->getPriceWedding() + $rateService->getPriceTesting()) * $wservice->getCount();
									}
									// Services per Month
									if (!array_key_exists($serviceId, $data->years[$year]->months[$month]->services)) {
										$data->years[$year]->months[$month]->services[$serviceId] = new \stdClass;
										$data->years[$year]->months[$month]->services[$serviceId]->name = $wservice->getService()->getName();
										$data->years[$year]->months[$month]->services[$serviceId]->count = $wservice->getCount();
										$data->years[$year]->months[$month]->services[$serviceId]->bill = ($rateService->getPriceWedding() + $rateService->getPriceTesting()) * $wservice->getCount();
									} else {
										$data->years[$year]->months[$month]->services[$serviceId]->count += $wservice->getCount();
										$data->years[$year]->months[$month]->services[$serviceId]->bill += ($rateService->getPriceWedding() + $rateService->getPriceTesting()) * $wservice->getCount();
									}
									// Bill per Year
									$data->years[$year]->bill += ($rateService->getPriceWedding() + $rateService->getPriceTesting()) * $wservice->getCount();
									// Bill per Month
									$data->years[$year]->months[$month]->bill += ($rateService->getPriceWedding() + $rateService->getPriceTesting()) * $wservice->getCount();
								}
								krsort($data->years[$year]->months);
								ksort($data->years[$year]->services);
								ksort($data->years[$year]->months[$month]->services);
							}
						}
					}
				}
				krsort($data->years);
			}
		}

		return $this->render('admin/reports/professional.html.twig', [
			'form' => $form->createView(),
			'reports' => $data
		]);
	}

	/**
	 * @Route("/admin/reports/denyReason", name="admin_reports_denyReason")
	 */
	public function admin_reports_denyReason(Request $request): Response
	{
		$form = $this->getFormReportsDenyReason();
		$data = null;

		if ($request->isMethod('POST')) {
			$form->handleRequest($request);
			if ($form->isValid()) {
				$tipo = $form['tipo']->getData();
				$year = $form['year']->getData();
				$start = date('Y-m-d H:i:s', strtotime($year . '-01-01 00:00:00'));
				$end = date('Y-m-d H:i:s', strtotime($year . '-12-31 23:59:59'));
				
				$requests = $this->em->getRepository(EntityRequest::class)->createQueryBuilder('r')
					->where('r.validated = :validated')
					->andWhere('r.accepted = :accepted')
					->andWhere('r.creationDate BETWEEN :start AND :end')
					->setParameter('validated', true)
					->setParameter('accepted', false)
					->setParameter('start', $start)
					->setParameter('end', $end);
				if ($tipo) {
					$requests = $requests->andWhere('r.tipo = :tipo')->setParameter('tipo', $tipo);
				}
				$requests = $requests->getQuery()->getResult();

				$data = new \stdClass;
				$data->months = [];
				$data->reasons = [];
				$data->total = 0;
				foreach ($requests as $request) {
					$denyReason = $request->getDenyReason() ? $request->getDenyReason() : 'Sin Motivo';
					// Reasons per Year
					if (!array_key_exists($denyReason, $data->reasons)) {
						$data->reasons[$denyReason] = 1;
					} else {
						$data->reasons[$denyReason] += 1;
					}
					// Months
					$month = $request->getCreationDate()->format('n');
					if (!array_key_exists($month, $data->months)) {
						$data->months[$month] = new \stdClass;
						$data->months[$month]->reasons = [];
						$data->months[$month]->total = 0;
					}
					// Reasons per Month
					if (!array_key_exists($denyReason, $data->months[$month]->reasons)) {
						$data->months[$month]->reasons[$denyReason] = 1;
					} else {
						$data->months[$month]->reasons[$denyReason] += 1;
					}
					$data->months[$month]->total += 1;
					$data->total += 1;
					ksort($data->months[$month]->reasons);
				}
				krsort($data->months);
				ksort($data->reasons);
			}
		}

		return $this->render('admin/reports/denyReason.html.twig', [
			'form' => $form->createView(),
			'reports' => $data
		]);
	}

	/**
	 * @Route("/admin/billing", name="admin_billing")
	 */
	public function admin_billing(Request $request): Response
	{
        $month = $request->query->get('month', date('m'));
        $year = $request->query->get('year', date('Y'));
        $firstDay = date('Y-m-d H:i:s', strtotime($year . '-'. $month . '-' . '01 00:00:00'));
        $lastDay = date('Y-m-t H:i:s', strtotime($year . '-'. $month . '-' . '01 23:59:59'));

		if ($request->isMethod('POST')) {
			$file = $request->files->get('file');
			$num = $request->request->get('num');
			$price = $request->request->get('price');
			$price = $price ? floatval(str_replace(',', '.', $price)) : $price;
			$accountNumber = $request->request->get('accountNumber');
			$cifNifType = $request->request->get('cifNifType');
			$cifNif = $request->request->get('cifNif');
			$address = $request->request->get('address');
			$city = $request->request->get('city');
			$cp = $request->request->get('cp');

			$billing = $this->em->getRepository(Billing::class)->createQueryBuilder('b')
				->where('b.professional = :professional')->setParameter('professional', $this->getUser())
				->andWhere('b.month = :month')->setParameter('month', $month)
				->andWhere('b.year = :year')->setParameter('year', $year)
				->getQuery()->getOneOrNullResult();
			if (!$billing) {
				$billing = new Billing();
				$billing->setProfessional($this->getUser());
				$billing->setMonth($month);
				$billing->setYear($year);
			}
			$fileName = $file->getClientOriginalName();
			$fileExt = '.' . $file->getClientOriginalExtension();
			$fileName = str_replace($fileExt, '', $fileName);
			$fileName = $fileName . '_' . uniqid() . $fileExt;
			$file->move('media/facturas', $fileName);
			$billing->setBill('/media/file/' . $fileName . '?conf=public_facturas&view=list&module=1');
			$billing->setPaid(false);
			
			$billing->setNum($num);
			$billing->setPrice($price);
			$billing->setAccountNumber($accountNumber);
			$billing->setCifNifType($cifNifType);
			$billing->setCifNif($cifNif);
			$billing->setAddress($address);
			$billing->setCity($city);
			$billing->setCp($cp);

			$this->em->persist($billing);
			$this->em->flush();
		}

		$weddings = $this->em->getRepository(Wedding::class)->createQueryBuilder('w');
		$weddings = $weddings
			->join('w.weddingServices', 'ws')
			->where($weddings->expr()->orX(
				'w.testingDate BETWEEN :firstDay AND :lastDay',
				'w.weddingDate BETWEEN :firstDay AND :lastDay'
			))->setParameter('firstDay', $firstDay)->setParameter('lastDay', $lastDay)
			->andWhere('ws.professional = :user')->setParameter('user', $this->getUser())
			->orderBy('w.weddingDate', 'ASC')
			->getQuery()->getResult();

        $weddings = array_map(function ($wedding) use ($month) {
            $obj_wedding = new \stdClass();
            $obj_wedding->weddingPayMethod = $wedding->getWeddingPayMethod();
            $obj_wedding->testingPayMethod = $wedding->getTestingPayMethod();
			$obj_wedding->isTesting = $wedding->getTestingDate() && $month == $wedding->getTestingDate()->format('m');
			$obj_wedding->isWedding = $wedding->getWeddingDate() && $month == $wedding->getWeddingDate()->format('m');
			$location = $obj_wedding->isTesting ? $wedding->getFullTestingAddress() : $wedding->getFullWeddingAddress();
            $obj_wedding->title = $wedding->getClient()->getFullname() . ' (' . $location . ')';
            $obj_wedding->datetime = $obj_wedding->isTesting ? $wedding->getTestingDate() : $wedding->getWeddingDate();
            $obj_wedding->services = [];
            foreach ($wedding->getWeddingServices() as $weddingservice) {
                if ($weddingservice->getProfessional() == $this->getUser()) {
					$obj_service = new \stdClass();
					$obj_service->title = $weddingservice->getService()->getName();
					$obj_service->title = $weddingservice->getCount() > 1 ? $obj_service->title . ' (x' . $weddingservice->getCount() . ')' : $obj_service->title;
					$obj_service->count = $weddingservice->getCount();
					$rateService = $this->em->getRepository(RateService::class)->createQueryBuilder('ps')
						->where('ps.service = :service')->setParameter('service', $weddingservice->getService())
						->andWhere('ps.rate = :rate')->setParameter('rate', $weddingservice->getProfessional()->getProfessionalRate())
						->getQuery()->getSingleResult();
					$obj_service->priceWedding = $rateService->getPriceWedding();
					$obj_service->priceTesting = $rateService->getPriceTesting();
					$obj_service->weddingDisplacementCost = $weddingservice->getWeddingDisplacementCost();
					$obj_service->testingDisplacementCost = $weddingservice->getTestingDisplacementCost();
					$obj_wedding->services[] = $obj_service;
                }
            }
            return $obj_wedding;
        }, $weddings);

		$billing = $this->em->getRepository(Billing::class)->createQueryBuilder('b')
			->where('b.month = :month')
			->andWhere('b.year = :year')
			->andWhere('b.professional = :professional')
			->setParameter('month', $month)
			->setParameter('year', $year)
			->setParameter('professional', $this->getUser())
			->getQuery()->getOneOrNullResult();
		
		return $this->render('admin/billing.html.twig', [
			'weddings' => $weddings,
			'billing' => $billing,
			'month' => $month,
			'year' => $year,
		]);
	}

	/**
	 * @Route("/admin/contract", name="admin_contract")
	 */
	public function admin_contract(Request $request): Response
	{
		$session = $this->container->get('request_stack')->getSession();
        $form = $this->getFormContractProfessional();

        if ($request->isMethod('POST')) {
			$form->handleRequest($request);
			if ($form->isValid()) {
				$file = $request->files->get('contractProfessional')['professionalContract'];
				$fileName = $file->getClientOriginalName();
				$fileExt = '.' . $file->getClientOriginalExtension();
				$fileName = str_replace($fileExt, '', $fileName);
				$fileName = $fileName . '_' . uniqid() . $fileExt;
				$file->move('media/contratosProfesionales', $fileName);
				
				$this->getUser()->setProfessionalDni($request->get('contractProfessional')['dni']);
				$this->getUser()->setProfessionalAddress($request->get('contractProfessional')['address']);
				$this->getUser()->setProfessionalContract('/media/file/' . $fileName . '?conf=public_contratosProfesionales&view=list&module=1');
				$this->getUser()->setProfessionalContractDate(new \Datetime('now'));
				
				$this->em->persist($this->getUser());

				$title = $this->translator->trans('ea.contract.mailTitle');
				$templateEntity = $this->em->getRepository(TemplateEmail::class)->findOneBy(['type' => 2]);
				$content = $templateEntity->getTemplate();
				$userId = base64_encode($this->getUser()->getId());
				$content = str_replace("{{ url }}", $this->container->get('request_stack')->getCurrentRequest()->getSchemeAndHttpHost() . '/contract?id=' . $userId, $content);
				$emails = array();
				$emails[] = $this->getUser()->getEmail();
				$emailsCC = array();
				$emailsCCO = $session->get('configApp')->emailCCO ? explode(',', $session->get('configApp')->emailCCO) : [];
				$email = $this->em->getRepository(Email::class)->sendEmail($title, $content, $emails, $emailsCC, $emailsCCO);
				$this->em->persist($email);

				$this->em->flush();
			}
        }
		
		$type = $this->getUser()->getProfessionalExclusive() ? 2 : 1;
		$templateEntity = $this->em->getRepository(TemplateContract::class)->findOneBy(['type' => $type]);
		$template = $templateEntity->getTemplate();
		$template = str_replace("{{ user }}", $this->getUser(), $template);
		if ($this->getUser()->getProfessionalContract()) {
			$dni = $this->getUser()->getProfessionalDni();
			$address = $this->getUser()->getProfessionalAddress();
		} else {
			$dni = '<input type="text" id="form_dni" name="contractProfessional[dni]" required="required" style="width: 150px; display: inline;" class="form-control">';
			$address = '<input type="text" id="form_address" name="contractProfessional[address]" required="required" style="width: 300px; display: inline;" class="form-control">';
		}
		$template = str_replace("{{ dni }}", $dni, $template);
		$template = str_replace("{{ address }}", $address, $template);
        return $this->render('admin/contract.html.twig', [
            'form' => $form->createView(),
            'template' => $template,
			'user' => $this->getUser()
        ]);
	}

	/**
	 * @Route("/admin/api/testingSign", name="admin_testingSign")
	 */
    public function admin_testingSign(Request $request)
    {
        $id = $request->request->get('id');
        $sign = $request->request->get('sign');
		
		$wedding = $this->em->getRepository(Wedding::class)->find($id);
		$wedding->setTestingSign($sign);
		
		$this->em->persist($wedding);
		$this->em->flush();
		
		return $this->json($id, 200);
    }

	private function getFormDisponibility() {
		$form = $this->createFormBuilder();

		$date = FieldGenerator::date('date')
			->setLabel($this->translator->trans('ea.disponibility.date'))
			->setFormTypeOption('widget', 'single_text');
		$province = FieldGenerator::association('province')
			->setLabel($this->translator->trans('entities.province.singular'))
			->setFormTypeOption('class', Province::class)
			->setFormTypeOption('attr.data-ea-widget', 'ea-autocomplete')
			->setRequired(false);
		
		$form = FormGenerator::getFormBuilder($form, [
			$date,
			$province
		]);

		$form->add('send', SubmitType::class, [
			'label' => '<i class="icon ti ti-search"></i> <span class="action-label">' . $this->translator->trans('action.search', [], 'EasyAdminBundle') . '</span>',
			'label_html' => true,
			'attr' => [
				'class' => 'btn btn-success',
				'form' => 'disponibility-form'
			]
		]);

		return $form->getForm();
	}

	private function getFormReportsYear() {
		$form = $this->createFormBuilder();

		$year = FieldGenerator::integer('year')
			->setLabel($this->translator->trans('ea.reports.year.year'));
		$tipo = FieldGenerator::choice('tipo')
			->setLabel($this->translator->trans('entities.wedding.fields.tipo'))
			->setFormTypeOption('choices', [
				$this->translator->trans('entities.wedding.fields.tipos.1') => 1,
				$this->translator->trans('entities.wedding.fields.tipos.2') => 2
			])->setRequired(false);
		
		$form = FormGenerator::getFormBuilder($form, [
			$tipo,
			$year
		]);

		$form->add('send', SubmitType::class, [
			'label' => '<i class="icon ti ti-search"></i> <span class="action-label">' . $this->translator->trans('action.search', [], 'EasyAdminBundle') . '</span>',
			'label_html' => true,
			'attr' => [
				'class' => 'btn btn-success',
				'form' => 'reportsYear-form'
			]
		]);

		return $form->getForm();
	}

	private function getFormReportsCreationDate() {
		$form = $this->createFormBuilder();

		$year = FieldGenerator::integer('year')
			->setLabel($this->translator->trans('ea.reports.year.year'));
		$weddingProvince = FieldGenerator::association('weddingProvince')
			->setLabel($this->translator->trans('entities.province.singular'))
			->setFormTypeOption('class', Province::class)
			->setFormTypeOption('attr.data-ea-widget', 'ea-autocomplete')
			->setRequired(false);
		$tipo = FieldGenerator::choice('tipo')
			->setLabel($this->translator->trans('entities.wedding.fields.tipo'))
			->setFormTypeOption('choices', [
				$this->translator->trans('entities.wedding.fields.tipos.1') => 1,
				$this->translator->trans('entities.wedding.fields.tipos.2') => 2
			])->setRequired(false);
		
		$form = FormGenerator::getFormBuilder($form, [
			$tipo,
			$year,
			$weddingProvince,
		]);

		$form->add('send', SubmitType::class, [
			'label' => '<i class="icon ti ti-search"></i> <span class="action-label">' . $this->translator->trans('action.search', [], 'EasyAdminBundle') . '</span>',
			'label_html' => true,
			'attr' => [
				'class' => 'btn btn-success',
				'form' => 'reportsCreationDate-form'
			]
		]);

		return $form->getForm();
	}

	private function getFormReportsProfessional() {
		$form = $this->createFormBuilder();

		$professional = FieldGenerator::association('professional')
			->setLabel($this->translator->trans('entities.professional.singular'))
			->setFormTypeOption('class', User::class)
			->setFormTypeOption('attr.data-ea-widget', 'ea-autocomplete')
			->setFormTypeOption('query_builder', function ($queryBuilder) {
				return $queryBuilder->createQueryBuilder('entity')
					->leftJoin('entity.role', 'r')
					->andWhere("r.name = 'ROLE_PROFESSIONAL'");
			});
		$tipo = FieldGenerator::choice('tipo')
			->setLabel($this->translator->trans('entities.wedding.fields.tipo'))
			->setFormTypeOption('choices', [
				$this->translator->trans('entities.wedding.fields.tipos.1') => 1,
				$this->translator->trans('entities.wedding.fields.tipos.2') => 2
			])->setRequired(false);
		
		$form = FormGenerator::getFormBuilder($form, [
			$tipo,
			$professional
		]);

		$form->add('send', SubmitType::class, [
			'label' => '<i class="icon ti ti-search"></i> <span class="action-label">' . $this->translator->trans('action.search', [], 'EasyAdminBundle') . '</span>',
			'label_html' => true,
			'attr' => [
				'class' => 'btn btn-success',
				'form' => 'reportsProfessional-form'
			]
		]);

		return $form->getForm();
	}

	private function getFormReportsDenyReason() {
		$form = $this->createFormBuilder();

		$year = FieldGenerator::integer('year')
			->setLabel($this->translator->trans('ea.reports.denyReason.year'));
		$tipo = FieldGenerator::choice('tipo')
			->setLabel($this->translator->trans('entities.wedding.fields.tipo'))
			->setFormTypeOption('choices', [
				$this->translator->trans('entities.wedding.fields.tipos.1') => 1,
				$this->translator->trans('entities.wedding.fields.tipos.2') => 2
			])->setRequired(false);
		
		$form = FormGenerator::getFormBuilder($form, [
			$tipo,
			$year
		]);

		$form->add('send', SubmitType::class, [
			'label' => '<i class="icon ti ti-search"></i> <span class="action-label">' . $this->translator->trans('action.search', [], 'EasyAdminBundle') . '</span>',
			'label_html' => true,
			'attr' => [
				'class' => 'btn btn-success',
				'form' => 'reportsDenyReason-form'
			]
		]);

		return $form->getForm();
	}

    private function getFormContractProfessional() {
        $form = $this->createFormBuilder();

		$accept = FieldGenerator::switch('accept')
			->setRequired(true)
			->setLabel($this->translator->trans('ea.contract.accept'));
		
		$form = FormGenerator::getFormBuilder($form, [
			$accept
		]);

		$form->add('send', SubmitType::class, [
			'label' => '<i class="icon ti ti-device-floppy"></i> <span class="action-label">' . $this->translator->trans('action.save', [], 'EasyAdminBundle') . '</span>',
			'label_html' => true,
			'attr' => [
				'class' => 'btn btn-success',
				'form' => 'contractProfessional-form'
			]
		]);

		return $form->getForm();
    }
}
