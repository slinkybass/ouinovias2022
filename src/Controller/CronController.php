<?php

namespace App\Controller;

use App\Entity\Email;
use App\Entity\TemplateEmail;
use App\Entity\Wedding;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Filesystem\Filesystem;

use Doctrine\ORM\EntityManagerInterface;

class CronController extends AbstractController
{
	private $em;
	private $translator;

	public function __construct(EntityManagerInterface $em, TranslatorInterface $translator)
	{
		$this->em = $em;
		$this->translator = $translator;
	}

	/**
	 * @Route("/cron/removeOldEmails", name="cron_removeOldEmails")
	 */
    public function cron_removeOldEmails(Request $request)
    {
		$date = new \DateTime('now');
		$date = $date->setTime(0, 0, 0);
		$date = $date->modify('-3 months');

		$emails = $this->em->getRepository(Email::class)->createQueryBuilder('e')
			->andWhere('e.creationDate <= :date')
			->setParameter('date', $date)
			->getQuery()->getResult();

		foreach ($emails as $email) {
			$this->em->remove($email);
		}

		$this->em->flush();

		return $this->json('ok', 200);
    }

	/**
	 * @Route("/cron/removeOldImages", name="cron_removeOldImages")
	 */
    public function cron_removeOldImages(Request $request)
    {
		$filesystem = new Filesystem();
		
		$now = new \DateTime('now');
		$date = new \DateTime('now');
		$date = $date->setTime(0, 0, 0);
		$date = $date->modify('-3 months');

		$weddings = $this->em->getRepository(Wedding::class)->createQueryBuilder('w')
			->andWhere('w.weddingDate <= :date')
			->setParameter('date', $date)
			->getQuery()->getResult();

		foreach ($weddings as $wedding) {
			$image1 = $wedding->getImage1() ? str_replace(['/media/file/', '?conf=public_bodas&view=list&module=1'], ['', ''], $wedding->getImage1()) : null;
			$image2 = $wedding->getImage2() ? str_replace(['/media/file/', '?conf=public_bodas&view=list&module=1'], ['', ''], $wedding->getImage2()) : null;
			$image3 = $wedding->getImage3() ? str_replace(['/media/file/', '?conf=public_bodas&view=list&module=1'], ['', ''], $wedding->getImage3()) : null;
			$image4 = $wedding->getImage4() ? str_replace(['/media/file/', '?conf=public_bodas&view=list&module=1'], ['', ''], $wedding->getImage4()) : null;
			$image5 = $wedding->getImage5() ? str_replace(['/media/file/', '?conf=public_bodas&view=list&module=1'], ['', ''], $wedding->getImage5()) : null;
			$image6 = $wedding->getImage6() ? str_replace(['/media/file/', '?conf=public_bodas&view=list&module=1'], ['', ''], $wedding->getImage6()) : null;
			if ($image1) {
				$filesystem->remove('media/bodas/' . $image1);
			}
			if ($image2) {
				$filesystem->remove('media/bodas/' . $image2);
			}
			if ($image3) {
				$filesystem->remove('media/bodas/' . $image3);
			}
			if ($image4) {
				$filesystem->remove('media/bodas/' . $image4);
			}
			if ($image5) {
				$filesystem->remove('media/bodas/' . $image5);
			}
			if ($image6) {
				$filesystem->remove('media/bodas/' . $image6);
			}
			$wedding->setImage1(null);
			$wedding->setImage2(null);
			$wedding->setImage3(null);
			$wedding->setImage4(null);
			$wedding->setImage5(null);
			$wedding->setImage6(null);
			$this->em->persist($wedding);
		}

		$this->em->flush();

		return $this->json('ok', 200);
    }

	/**
	 * @Route("/cron/fechaprueba", name="cron_fechaprueba")
	 */
    public function cron_fechaprueba(Request $request)
    {
		$session = $this->container->get('request_stack')->getSession();
        $date = new \DateTime("now");
        $date->modify('+90 day');
        $firstDay = \DateTime::createFromFormat('Y-m-d H:i:s', $date->format('Y') . '-'. $date->format('m') . '-' . $date->format('d') . ' 00:00:00');
        $lastDay = \DateTime::createFromFormat('Y-m-d H:i:s', $date->format('Y') . '-'. $date->format('m') . '-' . $date->format('d') . ' 23:59:59');

        $weddings = $this->em->getRepository(Wedding::class)->createQueryBuilder('w')
            ->where('w.weddingDate BETWEEN :firstDay AND :lastDay')
            ->andWhere('w.testingDate IS NULL')
			->setParameter('firstDay', $firstDay)
			->setParameter('lastDay', $lastDay)
            ->getQuery()->getResult();

        foreach ($weddings as $wedding) {
            if ($wedding->getClient()->getClientCronEmailFechaPrueba()) {
				$title = $this->translator->trans('ea.cron.fechaprueba');
				$templateEntity = $this->em->getRepository(TemplateEmail::class)->findOneBy(['type' => 6]);
				$content = $templateEntity->getTemplate();
				$emails = array();
				$emails[] = $wedding->getClient()->getEmail();
				$emailsCC = array();
				$emailsCCO = $session->get('configApp')->emailCCO ? explode(',', $session->get('configApp')->emailCCO) : [];
                $email = $this->em->getRepository(Email::class)->sendEmail($title, $content, $emails, $emailsCC, $emailsCCO);
                $this->em->persist($email);
            }
        }
    	$this->em->flush();

		return $this->json('ok', 200);
    }

	/**
	 * @Route("/cron/preguntarprueba", name="cron_preguntarprueba")
	 */
    public function cron_preguntarprueba(Request $request)
    {
		$session = $this->container->get('request_stack')->getSession();
        $date = new \DateTime("now");
        $date->modify('-1 day');
        $firstDay = \DateTime::createFromFormat('Y-m-d H:i:s', $date->format('Y') . '-'. $date->format('m') . '-' . $date->format('d') . ' 00:00:00');
        $lastDay = \DateTime::createFromFormat('Y-m-d H:i:s', $date->format('Y') . '-'. $date->format('m') . '-' . $date->format('d') . ' 23:59:59');

        $weddings = $this->em->getRepository(Wedding::class)->createQueryBuilder('w')
            ->where('w.testingDate BETWEEN :firstDay AND :lastDay')
			->setParameter('firstDay', $firstDay)
			->setParameter('lastDay', $lastDay)
            ->getQuery()->getResult();

        foreach ($weddings as $wedding) {
            if ($wedding->getClient()->getClientCronEmailPreguntarPrueba()) {
				$title = $this->translator->trans('ea.cron.preguntarprueba');
				$templateEntity = $this->em->getRepository(TemplateEmail::class)->findOneBy(['type' => 10]);
				$content = $templateEntity->getTemplate();
				$emails = array();
				$emails[] = $wedding->getClient()->getEmail();
				$emailsCC = array();
				$emailsCCO = $session->get('configApp')->emailCCO ? explode(',', $session->get('configApp')->emailCCO) : [];
                $email = $this->em->getRepository(Email::class)->sendEmail($title, $content, $emails, $emailsCC, $emailsCCO);
                $this->em->persist($email);
            }
        }
    	$this->em->flush();

		return $this->json('ok', 200);
    }

	/**
	 * @Route("/cron/preguntarboda", name="cron_preguntarboda")
	 */
    public function cron_preguntarboda(Request $request)
    {
		$session = $this->container->get('request_stack')->getSession();
        $date = new \DateTime("now");
        $date->modify('-15 day');
        $firstDay = \DateTime::createFromFormat('Y-m-d H:i:s', $date->format('Y') . '-'. $date->format('m') . '-' . $date->format('d') . ' 00:00:00');
        $lastDay = \DateTime::createFromFormat('Y-m-d H:i:s', $date->format('Y') . '-'. $date->format('m') . '-' . $date->format('d') . ' 23:59:59');

        $weddings = $this->em->getRepository(Wedding::class)->createQueryBuilder('w')
            ->where('w.weddingDate BETWEEN :firstDay AND :lastDay')
			->setParameter('firstDay', $firstDay)
			->setParameter('lastDay', $lastDay)
            ->getQuery()->getResult();

        foreach ($weddings as $wedding) {
            if ($wedding->getClient()->getClientCronEmailPreguntarBoda()) {
				$title = $this->translator->trans('ea.cron.preguntarboda');
				$templateEntity = $this->em->getRepository(TemplateEmail::class)->findOneBy(['type' => 9]);
				$content = $templateEntity->getTemplate();
				$emails = array();
				$emails[] = $wedding->getClient()->getEmail();
				$emailsCC = array();
				$emailsCCO = $session->get('configApp')->emailCCO ? explode(',', $session->get('configApp')->emailCCO) : [];
                $email = $this->em->getRepository(Email::class)->sendEmail($title, $content, $emails, $emailsCC, $emailsCCO);
                $this->em->persist($email);
            }
        }
    	$this->em->flush();

		return $this->json('ok', 200);
    }

	/**
	 * @Route("/cron/fotosboda", name="cron_fotosboda")
	 */
    public function cron_fotosboda(Request $request)
    {
		$session = $this->container->get('request_stack')->getSession();
        $date = new \DateTime("now");
        $date->modify('-5 month');
        $firstDay = \DateTime::createFromFormat('Y-m-d H:i:s', $date->format('Y') . '-'. $date->format('m') . '-' . $date->format('d') . ' 00:00:00');
        $lastDay = \DateTime::createFromFormat('Y-m-d H:i:s', $date->format('Y') . '-'. $date->format('m') . '-' . $date->format('d') . ' 23:59:59');

        $weddings = $this->em->getRepository(Wedding::class)->createQueryBuilder('w')
            ->where('w.weddingDate BETWEEN :firstDay AND :lastDay')
			->setParameter('firstDay', $firstDay)
			->setParameter('lastDay', $lastDay)
            ->getQuery()->getResult();

        foreach ($weddings as $wedding) {
            if ($wedding->getClient()->getClientCronEmailFotosBoda()) {
				$title = $this->translator->trans('ea.cron.fotosboda');
				$templateEntity = $this->em->getRepository(TemplateEmail::class)->findOneBy(['type' => 7]);
				$content = $templateEntity->getTemplate();
				$emails = array();
				$emails[] = $wedding->getClient()->getEmail();
				$emailsCC = array();
				$emailsCCO = $session->get('configApp')->emailCCO ? explode(',', $session->get('configApp')->emailCCO) : [];
                $email = $this->em->getRepository(Email::class)->sendEmail($title, $content, $emails, $emailsCC, $emailsCCO);
                $this->em->persist($email);
            }
        }
    	$this->em->flush();

		return $this->json('ok', 200);
    }

	/**
	 * @Route("/cron/ampliarboda", name="cron_ampliarboda")
	 */
    public function cron_ampliarboda(Request $request)
    {
		$session = $this->container->get('request_stack')->getSession();
        $date = new \DateTime("now");
        $date->modify('+15 day');
        $firstDay = \DateTime::createFromFormat('Y-m-d H:i:s', $date->format('Y') . '-'. $date->format('m') . '-' . $date->format('d') . ' 00:00:00');
        $lastDay = \DateTime::createFromFormat('Y-m-d H:i:s', $date->format('Y') . '-'. $date->format('m') . '-' . $date->format('d') . ' 23:59:59');

        $weddings = $this->em->getRepository(Wedding::class)->createQueryBuilder('w')
            ->where('w.weddingDate BETWEEN :firstDay AND :lastDay')
			->setParameter('firstDay', $firstDay)
			->setParameter('lastDay', $lastDay)
            ->getQuery()->getResult();

        foreach ($weddings as $wedding) {
            if ($wedding->getClient()->getClientCronEmailFotosBoda()) {
				$title = $this->translator->trans('ea.cron.ampliarboda');
				$templateEntity = $this->em->getRepository(TemplateEmail::class)->findOneBy(['type' => 5]);
				$content = $templateEntity->getTemplate();
				$emails = array();
				$emails[] = $wedding->getClient()->getEmail();
				$emailsCC = array();
				$emailsCCO = $session->get('configApp')->emailCCO ? explode(',', $session->get('configApp')->emailCCO) : [];
                $email = $this->em->getRepository(Email::class)->sendEmail($title, $content, $emails, $emailsCC, $emailsCCO);
                $this->em->persist($email);
            }
        }
    	$this->em->flush();

		return $this->json('ok', 200);
    }

	/**
	 * @Route("/cron/fotosprueba", name="cron_fotosprueba")
	 */
    public function cron_fotosprueba(Request $request)
    {
		$session = $this->container->get('request_stack')->getSession();
        $date = new \DateTime("now");
        $date->modify('+15 day');
        $firstDay = \DateTime::createFromFormat('Y-m-d H:i:s', $date->format('Y') . '-'. $date->format('m') . '-' . $date->format('d') . ' 00:00:00');
        $lastDay = \DateTime::createFromFormat('Y-m-d H:i:s', $date->format('Y') . '-'. $date->format('m') . '-' . $date->format('d') . ' 23:59:59');

        $weddings = $this->em->getRepository(Wedding::class)->createQueryBuilder('w')
            ->where('w.testingDate BETWEEN :firstDay AND :lastDay')
			->setParameter('firstDay', $firstDay)
			->setParameter('lastDay', $lastDay)
            ->getQuery()->getResult();

        foreach ($weddings as $wedding) {
            if ($wedding->getClient()->getClientCronEmailFotosBoda()) {
				$title = $this->translator->trans('ea.cron.fotosprueba');
				$templateEntity = $this->em->getRepository(TemplateEmail::class)->findOneBy(['type' => 8]);
				$content = $templateEntity->getTemplate();
				$emails = array();
				$emails[] = $wedding->getClient()->getEmail();
				$emailsCC = array();
				$emailsCCO = $session->get('configApp')->emailCCO ? explode(',', $session->get('configApp')->emailCCO) : [];
                $email = $this->em->getRepository(Email::class)->sendEmail($title, $content, $emails, $emailsCC, $emailsCCO);
                $this->em->persist($email);
            }
        }
    	$this->em->flush();

		return $this->json('ok', 200);
    }
}
