<?php

namespace App\Controller;

use App\Entity\Billing;
use App\Entity\Company;
use App\Entity\DisponibilityTesting;
use App\Entity\Email;
use App\Entity\Event;
use App\Entity\Province;
use App\Entity\Request as EntityRequest;
use App\Entity\Rate;
use App\Entity\RateService;
use App\Entity\Role;
use App\Entity\Service;
use App\Entity\TemplateContract;
use App\Entity\TemplateEmail;
use App\Entity\User;
use App\Entity\Wedding;
use App\Entity\WeddingService;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Filesystem\Filesystem;

use Doctrine\ORM\EntityManagerInterface;

class PublicController extends AbstractController
{
	private $em;
	private $translator;

	public function __construct(EntityManagerInterface $em, TranslatorInterface $translator)
	{
		$this->em = $em;
		$this->translator = $translator;
	}

	/**
	 * @Route("/login", name="login")
	 */
	public function login(AuthenticationUtils $authenticationUtils): Response
	{
		if ($this->getUser()) {
			return $this->redirectToRoute('public_home');
		}

		$session = $this->container->get('request_stack')->getSession();
		$error = $authenticationUtils->getLastAuthenticationError();
		$lastUsername = $authenticationUtils->getLastUsername();

		return $this->render('@EasyAdmin/page/login.html.twig', [
			'error' => $error,
			'last_username' => $lastUsername,
			'target_path' => $this->generateUrl('public_home'),
			'page_title' => $this->translator->trans('public.login.title'),
			'username_label' => $this->translator->trans($session->get('config')->enableUsername ? 'entities.user.fields.username' : 'entities.user.fields.email'),
			'password_label' => $this->translator->trans('entities.user.fields.password'),
			'forgot_password_label' => $this->translator->trans('public.login.forgotPassword'),
			'remember_me_label' => $this->translator->trans('public.login.rememberMe'),
			'sign_in_label' => $this->translator->trans('public.login.loginBtn'),
			'remember_me_enabled' => true,
			'remember_me_checked' => true,
			'app_logo' => $session->get('config')->appLogo,
		]);
	}

	/**
	 * @Route("/logout", name="logout")
	 */
	public function logout(): void
	{
	}

	/**
	 * Returns the corresponding redirect, if the user cannot access.
	 */
	public function checkPublicAccess()
	{
		$session = $this->container->get('request_stack')->getSession();

		if ($this->isGranted('ROLE_ADMIN')) {
			return $this->redirectToRoute('admin');
		} else if (!$session->get('config')->enablePublic) {
			return $this->redirectToRoute($this->getUser() ? 'admin' : 'login');
		}

		return null;
	}

	/**
	 * @Route("/", name="public_home")
	 */
	public function public_home(Request $request): Response
	{
		if (!$this->getUser()) {
			return $this->redirectToRoute('login');
		} else if ($this->isGranted('ROLE_ADMIN')) {
			return $this->redirectToRoute('admin');
		}

		$user = $this->getUser();
		$filesystem = new Filesystem();
		$session = $this->container->get('request_stack')->getSession();
		$services = $this->em->getRepository(Service::class)->createQueryBuilder('s')
			->getQuery()->getResult();
		$companies = $this->em->getRepository(Company::class)->createQueryBuilder('c')
			->getQuery()->getResult();
		$provinces = $this->em->getRepository(Province::class)->createQueryBuilder('p')
			->getQuery()->getResult();

		$billing = [];
		$myWeddings = [];
		$now = new \DateTime('now');
		$now = $now->setTime(0, 0, 0);

		$userWeddings = $this->isGranted('ROLE_CLIENT') ? $user->getWeddings() : ($this->isGranted('ROLE_WEDDINGPLANNER') ? $user->getPlannerWeddings() : []);
		foreach ($userWeddings as $userWedding) {
			$professional = count($userWedding->getWeddingServices()) ? $userWedding->getWeddingServices()[0]->getProfessional() : null;
			$allowedTestingDates = [];
			if ($professional) {
				foreach ($professional->getDisponibilityTestings() as $disponibilityTesting) {
					if (!$disponibilityTesting->getBlocked() && $disponibilityTesting->getDate() > $now && $disponibilityTesting->getDate() <= $userWedding->getWeddingDate()) {
						$allowedTestingDates[] = $disponibilityTesting->getDate()->format('Y-m-d');
					}
				}
			}
			$userWedding->allowedTestingDates = $allowedTestingDates;
			$myWeddings[] = $userWedding;
		}

		foreach ($myWeddings as $wedding) {
			foreach ($wedding->getWeddingServices() as $wservice) {
				if ($wservice->getProfessional()) {
					$rateService = $this->em->getRepository(RateService::class)->createQueryBuilder('rs')
							->where('rs.rate = :rate')
							->andWhere('rs.service = :service')
							->setParameter('rate', $wservice->getProfessional()->getProfessionalRate())
							->setParameter('service', $wservice->getService())
							->getQuery()->getSingleResult();
					$billing[$wservice->getId()] = new \stdClass();
					$billing[$wservice->getId()]->wedding = $rateService->getPriceWedding() * $wservice->getCount();
					$billing[$wservice->getId()]->testing = $rateService->getPriceTesting() * $wservice->getCount();
					$billing[$wservice->getId()]->booking = $rateService->getPriceBooking() * $wservice->getCount();
					$billing[$wservice->getId()]->total = ($rateService->getPriceWedding() + $rateService->getPriceTesting() + $rateService->getPriceBooking()) * $wservice->getCount();
				} else {
					$billing[$wservice->getId()] = new \stdClass();
					$billing[$wservice->getId()]->wedding = null;
					$billing[$wservice->getId()]->testing = null;
					$billing[$wservice->getId()]->booking = null;
					$billing[$wservice->getId()]->total = null;
				}
			}
		}

		if ($request->isMethod('POST')) {
			$errors = false;
			$user->setName($request->request->get('user_name'));
			$user->setLastname($request->request->get('user_lastname'));
			$user->setTel1($request->request->get('user_tel1'));
			$user->setTel2($request->request->get('user_tel2'));
			$clientConocePor = $request->request->get('user_clientConocePor') ? $this->em->getRepository(Company::class)->find($request->request->get('user_clientConocePor')) : null;
			$user->setClientConocePor($clientConocePor);
			$this->em->persist($user);

			foreach ($myWeddings as $wedding) {
				$id = $wedding->getId();
				$urlFB = $request->request->get('wedding_urlFB_' . $id) != "" ? $request->request->get('wedding_urlFB_' . $id) : null;
				$urlIG = $request->request->get('wedding_urlIG_' . $id) != "" ? $request->request->get('wedding_urlIG_' . $id) : null;
				$weddingComments = $request->request->get('wedding_weddingComments_' . $id) != "" ? $request->request->get('wedding_weddingComments_' . $id) : null;
				$weddingPayMethod = $request->request->get('wedding_weddingPayMethod_' . $id);
				$testingDate = $request->request->get('wedding_testingDate_' . $id) != "" ? str_replace('T', ' ', $request->request->get('wedding_testingDate_' . $id)) : null;
				$testingDate = $testingDate ? \DateTime::createFromFormat('Y-m-d H:i:s', $testingDate) : $wedding->getTestingDate();
				$testingComments = $request->request->get('wedding_testingComments_' . $id) != "" ? $request->request->get('wedding_testingComments_' . $id) : null;
				$testingPayMethod = $request->request->get('wedding_testingPayMethod_' . $id);
				$requestModify = $request->request->get('wedding_requestModify_' . $id) != "" ? $request->request->get('wedding_requestModify_' . $id) : null;
				$weddingSatisfaction = $request->request->get('wedding_weddingSatisfaction_' . $id) != "" ? $request->request->get('wedding_weddingSatisfaction_' . $id) : null;
				
				if ($wedding->getRequestModify() != $requestModify) {
					$title = $this->translator->trans('public.home.mailModifyTitle');
					$templateEntity = $this->em->getRepository(TemplateEmail::class)->findOneBy(['type' => 1]);
					$content = $templateEntity->getTemplate();
					$content = str_replace("{{ user }}", $wedding->getClient(), $content);
					$content = str_replace("{{ url }}", $this->container->get('request_stack')->getCurrentRequest()->getSchemeAndHttpHost(), $content);
					$emails = array();
					$emails[] = 'reservas@ouinovias.com';
					$emailsCC = array();
					$emailsCCO = array();
					$email = $this->em->getRepository(Email::class)->sendEmail($title, $content, $emails, $emailsCC, $emailsCCO);
					$this->em->persist($email);
				}
				$wedding->setUrlFB($urlFB);
				$wedding->setUrlIG($urlIG);
				$wedding->setWeddingComments($weddingComments);
				$wedding->setWeddingPayMethod($weddingPayMethod);
				$wedding->setTestingComments($testingComments);
				$hasChangedDate = false;
				if ($testingDate != $wedding->getTestingDate()) {
					$hasChangedDate = true;
				}
				$wedding->setTestingDate($testingDate);
				$wedding->setTestingPayMethod($testingPayMethod);
				$wedding->setRequestModify($requestModify);
				$wedding->setWeddingSatisfaction($weddingSatisfaction);

				//Check testingDate
				$professional = count($wedding->getWeddingServices()) ? $wedding->getWeddingServices()[0]->getProfessional() : null;
				if ($hasChangedDate && $testingDate && $professional) {
					$time = $testingDate->format('H') > 11 ? 2 : 1;
					$disponibilityTestingSelected = $this->em->getRepository(DisponibilityTesting::class)->findBy([
						'time' => $time,
						'date' => $testingDate,
						'professional' => $professional
					]);

					if ($disponibilityTestingSelected) {
						$disponibilityTestingSelected[0]->setBlocked(true);
						$this->em->persist($disponibilityTestingSelected[0]);
					} else {
						$this->addFlash('danger', $this->translator->trans('public.home.errorTestingDate'));
						$errors = true;
						break;
					}
				}

				//  Files
				$actualImage1 = $wedding->getImage1() ? str_replace(['/media/file/', '?conf=public_bodas&view=list&module=1'], ['', ''], $wedding->getImage1()) : null;
				$actualImage2 = $wedding->getImage2() ? str_replace(['/media/file/', '?conf=public_bodas&view=list&module=1'], ['', ''], $wedding->getImage2()) : null;
				$actualImage3 = $wedding->getImage3() ? str_replace(['/media/file/', '?conf=public_bodas&view=list&module=1'], ['', ''], $wedding->getImage3()) : null;
				$actualImage4 = $wedding->getImage4() ? str_replace(['/media/file/', '?conf=public_bodas&view=list&module=1'], ['', ''], $wedding->getImage4()) : null;
				$actualImage5 = $wedding->getImage5() ? str_replace(['/media/file/', '?conf=public_bodas&view=list&module=1'], ['', ''], $wedding->getImage5()) : null;
				$actualImage6 = $wedding->getImage6() ? str_replace(['/media/file/', '?conf=public_bodas&view=list&module=1'], ['', ''], $wedding->getImage6()) : null;
				$image1 = $request->files->get('wedding_file_1_' . $id);
				$removeImage1 = $request->request->get('wedding_file_remove_1_' . $id);
				$image2 = $request->files->get('wedding_file_2_' . $id);
				$removeImage2 = $request->request->get('wedding_file_remove_2_' . $id);
				$image3 = $request->files->get('wedding_file_3_' . $id);
				$removeImage3 = $request->request->get('wedding_file_remove_3_' . $id);
				$image4 = $request->files->get('wedding_file_4_' . $id);
				$removeImage4 = $request->request->get('wedding_file_remove_4_' . $id);
				$image5 = $request->files->get('wedding_file_5_' . $id);
				$removeImage5 = $request->request->get('wedding_file_remove_5_' . $id);
				$image6 = $request->files->get('wedding_file_6_' . $id);
				$removeImage6 = $request->request->get('wedding_file_remove_6_' . $id);

				if ($actualImage1 && $removeImage1) {
					$filesystem->remove('media/bodas/' . $actualImage1);
					$wedding->setImage1(null);
				} elseif ($image1) {
					$fileName = $image1->getClientOriginalName();
					$fileExt = '.' . $image1->getClientOriginalExtension();
					$fileName = str_replace($fileExt, '', $fileName);
					$fileName = $fileName . '_' . uniqid() . $fileExt;
					$image1->move('media/bodas', $fileName);
					$wedding->setImage1('/media/file/' . $fileName . '?conf=public_bodas&view=list&module=1');
				}
				if ($actualImage2 && $removeImage2) {
					$filesystem->remove('media/bodas/' . $actualImage2);
					$wedding->setImage2(null);
				} elseif ($image2) {
					$fileName = $image2->getClientOriginalName();
					$fileExt = '.' . $image2->getClientOriginalExtension();
					$fileName = str_replace($fileExt, '', $fileName);
					$fileName = $fileName . '_' . uniqid() . $fileExt;
					$image2->move('media/bodas', $fileName);
					$wedding->setImage2('/media/file/' . $fileName . '?conf=public_bodas&view=list&module=1');
				}
				if ($actualImage3 && $removeImage3) {
					$filesystem->remove('media/bodas/' . $actualImage3);
					$wedding->setImage3(null);
				} elseif ($image3) {
					$fileName = $image3->getClientOriginalName();
					$fileExt = '.' . $image3->getClientOriginalExtension();
					$fileName = str_replace($fileExt, '', $fileName);
					$fileName = $fileName . '_' . uniqid() . $fileExt;
					$image3->move('media/bodas', $fileName);
					$wedding->setImage3('/media/file/' . $fileName . '?conf=public_bodas&view=list&module=1');
				}
				if ($actualImage4 && $removeImage4) {
					$filesystem->remove('media/bodas/' . $actualImage4);
					$wedding->setImage4(null);
				} elseif ($image4) {
					$fileName = $image4->getClientOriginalName();
					$fileExt = '.' . $image4->getClientOriginalExtension();
					$fileName = str_replace($fileExt, '', $fileName);
					$fileName = $fileName . '_' . uniqid() . $fileExt;
					$image4->move('media/bodas', $fileName);
					$wedding->setImage4('/media/file/' . $fileName . '?conf=public_bodas&view=list&module=1');
				}
				if ($actualImage5 && $removeImage5) {
					$filesystem->remove('media/bodas/' . $actualImage5);
					$wedding->setImage5(null);
				} elseif ($image5) {
					$fileName = $image5->getClientOriginalName();
					$fileExt = '.' . $image5->getClientOriginalExtension();
					$fileName = str_replace($fileExt, '', $fileName);
					$fileName = $fileName . '_' . uniqid() . $fileExt;
					$image5->move('media/bodas', $fileName);
					$wedding->setImage5('/media/file/' . $fileName . '?conf=public_bodas&view=list&module=1');
				}
				if ($actualImage6 && $removeImage6) {
					$filesystem->remove('media/bodas/' . $actualImage6);
					$wedding->setImage6(null);
				} elseif ($image6) {
					$fileName = $image6->getClientOriginalName();
					$fileExt = '.' . $image6->getClientOriginalExtension();
					$fileName = str_replace($fileExt, '', $fileName);
					$fileName = $fileName . '_' . uniqid() . $fileExt;
					$image6->move('media/bodas', $fileName);
					$wedding->setImage6('/media/file/' . $fileName . '?conf=public_bodas&view=list&module=1');
				}

				$this->em->persist($wedding);
			}

			if (!$errors) {
				$this->addFlash('success', $this->translator->trans('public.home.okMsg'));
				$this->em->flush();
			} else {
				return $this->redirectToRoute('public_home');
			}
		}

		return $this->render('public/home.html.twig', [
			'user' => $user,
			'myWeddings' => $myWeddings,
			'services' => $services,
			'companies' => $companies,
			'provinces' => $provinces,
			'billing' => $billing
		]);
	}

	/**
	 * @Route("/api/importarPrecios", name="public_api_importarPrecios")
	 */
	public function public_api_importarPrecios(Request $request)
	{
		if (($fp = fopen("uploads/csv/service.csv", "r")) !== false) {
			while (($row = fgetcsv($fp)) !== false) {
				$id = $this->cleanstr($row[0]);
				$name = $this->cleanstr($row[1]);

				$service = $id ? $this->em->getRepository(Service::class)->find($id) : null;
				if (!$service && $id) {
					$service = new Service();
					$service->setId($id);
					$service->setName($name);

					$this->em->persist($service);
				}
			}
			fclose($fp);
			$this->em->flush();
		}

		if (($fp = fopen("uploads/csv/price.csv", "r")) !== false) {
			while (($row = fgetcsv($fp)) !== false) {
				$id = $this->cleanstr($row[0]);
				$name = $this->cleanstr($row[1]);
				$url = $this->cleanstr($row[4]);

				$rate = $id ? $this->em->getRepository(Rate::class)->find($id) : null;
				if (!$rate && $id) {
					$rate = new Rate();
					$rate->setId($id);
					$rate->setName($name);
					$rate->setUrl($url);

					$this->em->persist($rate);
				}
			}
			fclose($fp);
			$this->em->flush();
		}

		if (($fp = fopen("uploads/csv/price_service.csv", "r")) !== false) {
			while (($row = fgetcsv($fp)) !== false) {
				$id = $this->cleanstr($row[0]);
				$priceId = $this->cleanstr($row[1]);
				$rate = $priceId ? $this->em->getRepository(Rate::class)->find($priceId) : null;
				$serviceId = $this->cleanstr($row[2]);
				$service = $serviceId ? $this->em->getRepository(Service::class)->find($serviceId) : null;
				$valWedding = $this->cleanstr($row[3]) ?? 0;
				$valBooking = $this->cleanstr($row[4]) ?? 0;
				$valTesting = $this->cleanstr($row[5]) ?? 0;

				$rateService = $id ? $this->em->getRepository(RateService::class)->find($id) : null;
				if (!$rateService && $id) {
					$rateService = new RateService();
					$rateService->setId($id);
					$rateService->setRate($rate);
					$rateService->setService($service);
					$rateService->setPriceBooking($valBooking);
					$rateService->setPriceTesting($valTesting);
					$rateService->setPriceWedding($valWedding);

					$this->em->persist($rateService);
				}
			}
			fclose($fp);
			$this->em->flush();
		}

		return $this->json('ok', 200);
	}

	/**
	 * @Route("/api/importarEmpresas", name="public_api_importarEmpresas")
	 */
	public function public_api_importarEmpresas(Request $request)
	{
		if (($fp = fopen("uploads/csv/planner.csv", "r")) !== false) {
			while (($row = fgetcsv($fp)) !== false) {
				$id = $this->cleanstr($row[0]);
				$name = $this->cleanstr($row[1]);

				$company = $id ? $this->em->getRepository(Company::class)->find($id) : null;
				if (!$company && $id) {
					$company = new Company();
					$company->setId($id);
					$company->setName($name);

					$this->em->persist($company);
				}
			}
			fclose($fp);
			$this->em->flush();
		}

		return $this->json('ok', 200);
	}

	/**
	 * @Route("/api/importarUsuarios", name="public_api_importarUsuarios")
	 */
	public function public_api_importarUsuarios(Request $request, UserPasswordHasherInterface $passwordHasher)
	{
		if (($fp = fopen("uploads/csv/user.csv", "r")) !== false) {
			while (($row = fgetcsv($fp)) !== false) {
				$id = $this->cleanstr($row[0]);
				$roles = unserialize($this->cleanstr($row[12]));
				foreach ($roles as $roleStr) {
					if ($roleStr == "ROLE_PLANNER") {
						$roleStr = "ROLE_WEDDINGPLANNER";
					}
					$role = $this->em->getRepository(Role::class)->findOneBy([
						'name' => $roleStr
					]);
				}
				$email = $this->cleanstr($row[2]);
				$password = $this->cleanstr($row[8]);
				$priceId = $this->cleanstr($row[1]);
				$rate = $priceId ? $this->em->getRepository(Rate::class)->find($priceId) : null;
				$plannerId = $this->cleanstr($row[19]);
				$company = $plannerId ? $this->em->getRepository(Company::class)->find($plannerId) : null;
				$tel1 = $this->cleanstr($row[15]);
				$tel2 = $this->cleanstr($row[16]);
				$perfilURL = $this->cleanstr($row[20]);
				$dni = $this->cleanstr($row[22]);
				$address = $this->cleanstr($row[23]);
				$professional_exclusive = $this->cleanstr($row[26]);
				$professional_exclusive = $professional_exclusive ? true : false;
				$document = $this->cleanstr($row[24]);
				$document = $document ? '/media/file/' . $document . '?conf=public_contratosProfesionales&view=list&module=1' : null;
				$documentDate = $this->cleanstr($row[25]);
				$documentDate = $documentDate ? \DateTime::createFromFormat('Y-m-d H:i:s', $documentDate) : null;
				$cronEmailsFechaPrueba = $this->cleanstr($row[27]);
				$cronEmailsFechaPrueba = $cronEmailsFechaPrueba ? true : false;
				$cronEmailsPreguntarPrueba = $this->cleanstr($row[28]);
				$cronEmailsPreguntarPrueba = $cronEmailsPreguntarPrueba ? true : false;
				$cronEmailsPreguntarBoda = $this->cleanstr($row[29]);
				$cronEmailsPreguntarBoda = $cronEmailsPreguntarBoda ? true : false;
				$cronEmailsFotosBoda = $this->cleanstr($row[30]);
				$cronEmailsFotosBoda = $cronEmailsFotosBoda ? true : false;
				$name = $this->cleanstr($row[13]);
				$lastname = $this->cleanstr($row[14]);

				$user = $id ? $this->em->getRepository(User::class)->find($id) : null;
				if (!$user && $id) {
					$user = new User();
					$user->setId($id);
					$user->setRole($role);
					$user->setUsername($email);
					$user->setPassword($password);
					$user->setEmail($email);
					$user->setProfessionalRate($rate);
					$user->setClientConocePor($company);
					$user->setTel1($tel1);
					$user->setTel2($tel2);
					$user->setProfessionalURL($perfilURL);
					$user->setProfessionalDni($dni);
					$user->setProfessionalAddress($address);
					$user->setProfessionalExclusive($professional_exclusive);
					$user->setProfessionalContract($document);
					$user->setProfessionalContractDate($documentDate);
					$user->setClientCronEmailFechaPrueba($cronEmailsFechaPrueba);
					$user->setClientCronEmailPreguntarPrueba($cronEmailsPreguntarPrueba);
					$user->setClientCronEmailPreguntarBoda($cronEmailsPreguntarBoda);
					$user->setClientCronEmailFotosBoda($cronEmailsFotosBoda);
					$user->setName($name);
					$user->setLastname($lastname);

					$this->em->persist($user);
				}
			}
			fclose($fp);
			$this->em->flush();
		}

		return $this->json('ok', 200);
	}

	/**
	 * @Route("/api/importarSolicitudes", name="public_api_importarSolicitudes")
	 */
	public function public_api_importarSolicitudes(Request $request)
	{
		if (($fp = fopen("uploads/csv/request.csv", "r")) !== false) {
			while (($row = fgetcsv($fp)) !== false) {
				$id = $this->cleanstr($row[0]);
				$name = $this->cleanstr($row[1]);
				$lastname = $this->cleanstr($row[2]);
				$lastname = $lastname ?? '-';
				$telephone = $this->cleanstr($row[3]);
				$email = $this->cleanstr($row[4]);
				$weddingDate = $this->cleanstr($row[5]);
				$weddingDate = $weddingDate ? \DateTime::createFromFormat('Y-m-d H:i:s', $weddingDate) : null;
				$weddingLocation = $this->cleanstr($row[6]);
				$comments = $this->cleanstr($row[7]);
				$answered = $this->cleanstr($row[8]);
				$answered = $answered == "1" ? true : ($answered == "0" ? false : null);
				$validated = $this->cleanstr($row[9]);
				$validated = $validated == "1" ? true : ($validated == "0" ? false : null);
				$pending = $this->cleanstr($row[10]);
				$pending = $pending == "1" ? true : ($pending == "0" ? false : null);
				$accepted = $this->cleanstr($row[11]);
				$accepted = $accepted == "1" ? true : ($accepted == "0" ? false : null);
				$creationDate = $this->cleanstr($row[12]);
				$creationDate = $creationDate ? \DateTime::createFromFormat('Y-m-d H:i:s', $creationDate) : null;
				$answeredDate = $this->cleanstr($row[13]);
				$answeredDate = $answeredDate ? \DateTime::createFromFormat('Y-m-d H:i:s', $answeredDate) : null;
				$validatedDate = $this->cleanstr($row[14]);
				$validatedDate = $validatedDate ? \DateTime::createFromFormat('Y-m-d H:i:s', $validatedDate) : null;
				$denyReason = $this->cleanstr($row[15]);
				$weddingCity = $this->cleanstr($row[16]);
				$callDateAnswered = $this->cleanstr($row[17]);
				$callDateAnswered = $callDateAnswered ? \DateTime::createFromFormat('Y-m-d H:i:s', $callDateAnswered) : null;
				$callDateValidated = $this->cleanstr($row[18]);
				$callDateValidated = $callDateValidated ? \DateTime::createFromFormat('Y-m-d H:i:s', $callDateValidated) : null;

				$eRequest = $id ? $this->em->getRepository(EntityRequest::class)->find($id) : null;
				if (!$eRequest && $id) {
					$eRequest = new EntityRequest();
					$eRequest->setId($id);
					$eRequest->setName($name);
					$eRequest->setLastname($lastname);
					$eRequest->setTel($telephone);
					$eRequest->setEmail($email);
					$eRequest->setWeddingDate($weddingDate);
					$eRequest->setWeddingAddress($weddingLocation);
					$eRequest->setWeddingCity($weddingCity);
					$eRequest->setComments($comments);
					$eRequest->setAnswered($answered);
					$eRequest->setAnsweredDate($answeredDate);
					$eRequest->setValidated($validated);
					$eRequest->setValidatedDate($validatedDate);
					$eRequest->setPending($pending);
					$eRequest->setAccepted($accepted);
					$eRequest->setDenyReason($denyReason);
					$eRequest->setCreationDate($creationDate);
					$eRequest->setCallDateAnswered($callDateAnswered);
					$eRequest->setCallDateValidated($callDateValidated);

					$this->em->persist($eRequest);
				}
			}
			fclose($fp);
			$this->em->flush();
		}

		return $this->json('ok', 200);
	}

	/**
	 * @Route("/api/importarBodas", name="public_api_importarBodas")
	 */
	public function public_api_importarBodas(Request $request)
	{
		if (($fp = fopen("uploads/csv/wedding.csv", "r")) !== false) {
			while (($row = fgetcsv($fp)) !== false) {
				$id = $this->cleanstr($row[0]);
				$clientId = $this->cleanstr($row[1]);
				$client = $clientId ? $this->em->getRepository(User::class)->find($clientId) : null;
				$weddingDate = $this->cleanstr($row[2]);
				$weddingDate = $weddingDate ? \DateTime::createFromFormat('Y-m-d H:i:s', $weddingDate) : null;
				$testingDate = $this->cleanstr($row[3]);
				$testingDate = $testingDate ? \DateTime::createFromFormat('Y-m-d H:i:s', $testingDate) : null;
				$weddingLocation = $this->cleanstr($row[4]);
				$testingLocation = $this->cleanstr($row[5]);
				$weddingComments = $this->cleanstr($row[6]);
				$testingComments = $this->cleanstr($row[7]);
				$weddingDisplacement = $this->cleanstr($row[8]);
				$testingDisplacement = $this->cleanstr($row[9]);
				$socialFB = $this->cleanstr($row[10]);
				$socialIG = $this->cleanstr($row[11]);
				$creationDate = $this->cleanstr($row[12]);
				$creationDate = $creationDate ? \DateTime::createFromFormat('Y-m-d H:i:s', $creationDate) : null;
				$meetingTime = $this->cleanstr($row[13]);
				$request = $this->cleanstr($row[14]);
				$weddingSatisfaction = $this->cleanstr($row[15]);
				$weddingPayMethod = $this->cleanstr($row[16]);
				$testingPayMethod = $this->cleanstr($row[17]);
				$weddingCitationSended = $this->cleanstr($row[18]);
				$weddingCitationSended = $weddingCitationSended ? true : false;
				$testingCitationSended = $this->cleanstr($row[19]);
				$testingCitationSended = $testingCitationSended ? true : false;
				$plannerId = $this->cleanstr($row[20]);
				$planner = $plannerId ? $this->em->getRepository(User::class)->find($plannerId) : null;
				$weddingCity = $this->cleanstr($row[21]);
				$testingCity = $this->cleanstr($row[22]);
				$testingSign = $this->cleanstr($row[23]);

				$wedding = $id ? $this->em->getRepository(Wedding::class)->find($id) : null;
				if (!$wedding && $id) {
					$wedding = new Wedding();
					$wedding->setId($id);
					$wedding->setClient($client);
					$wedding->setPlanner($planner);
					$wedding->setWeddingAddress($weddingLocation);
					$wedding->setWeddingCity($weddingCity);
					$wedding->setWeddingDate($weddingDate);
					$wedding->setWeddingComments($weddingComments);
					$wedding->setWeddingDisplacementCost($weddingDisplacement);
					$wedding->setWeddingPayMethod($weddingPayMethod);
					$wedding->setWeddingCitationSended($weddingCitationSended);
					$wedding->setWeddingSatisfaction($weddingSatisfaction);
					$wedding->setTestingAddress($testingLocation);
					$wedding->setTestingCity($testingCity);
					$wedding->setTestingDate($testingDate);
					$wedding->setTestingComments($testingComments);
					$wedding->setTestingDisplacementCost($testingDisplacement);
					$wedding->setTestingPayMethod($testingPayMethod);
					$wedding->setTestingCitationSended($testingCitationSended);
					$wedding->setTestingSign($testingSign);
					$wedding->setMeetingTime($meetingTime);
					$wedding->setUrlFB($socialFB);
					$wedding->setUrlIG($socialIG);
					$wedding->setRequestModify($request);
					$wedding->setCreationDate($creationDate);

					$this->em->persist($wedding);
				}
			}
			fclose($fp);
			$this->em->flush();
		}

		if (($fp = fopen("uploads/csv/wedding_service.csv", "r")) !== false) {
			while (($row = fgetcsv($fp)) !== false) {
				$id = $this->cleanstr($row[0]);
				$serviceId = $this->cleanstr($row[1]);
				$service = $serviceId ? $this->em->getRepository(Service::class)->find($serviceId) : null;
				$professionalId = $this->cleanstr($row[2]);
				$professional = $professionalId ? $this->em->getRepository(User::class)->find($professionalId) : null;
				$weddingId = $this->cleanstr($row[3]);
				$wedding = $weddingId ? $this->em->getRepository(Wedding::class)->find($weddingId) : null;
				$count = $this->cleanstr($row[4]);
				$location = $this->cleanstr($row[5]);

				$weddingService = $id ? $this->em->getRepository(WeddingService::class)->find($id) : null;
				if (!$weddingService && $id) {
					$weddingService = new WeddingService();
					$weddingService->setId($id);
					$weddingService->setWedding($wedding);
					$weddingService->setService($service);
					$weddingService->setProfessional($professional);
					$weddingService->setCount($count);
					$weddingService->setLocation($location);

					$this->em->persist($weddingService);
				}
			}
			fclose($fp);
			$this->em->flush();
		}

		if (($fp = fopen("uploads/csv/wedding_file.csv", "r")) !== false) {
			while (($row = fgetcsv($fp)) !== false) {
				$id = $this->cleanstr($row[0]);
				$weddingId = $this->cleanstr($row[1]);
				$wedding = $weddingId ? $this->em->getRepository(Wedding::class)->find($weddingId) : null;
				$document = $this->cleanstr($row[3]);
				$document = $document ? '/media/file/' . $document . '?conf=public_bodas&view=list&module=1' : null;

				if ($wedding) {
					if (!$wedding->getImage1()) {
						$wedding->setImage1($document);
						$this->em->persist($wedding);
					} else if (!$wedding->getImage2()) {
						$wedding->setImage2($document);
						$this->em->persist($wedding);
					} else if (!$wedding->getImage3()) {
						$wedding->setImage3($document);
						$this->em->persist($wedding);
					} else if (!$wedding->getImage4()) {
						$wedding->setImage4($document);
						$this->em->persist($wedding);
					} else if (!$wedding->getImage5()) {
						$wedding->setImage5($document);
						$this->em->persist($wedding);
					} else if (!$wedding->getImage6()) {
						$wedding->setImage6($document);
						$this->em->persist($wedding);
					}
				}
			}
			fclose($fp);
			$this->em->flush();
		}

		return $this->json('ok', 200);
	}

	/**
	 * @Route("/api/importarFacturas", name="public_api_importarFacturas")
	 */
	public function public_api_importarFacturas(Request $request)
	{
		if (($fp = fopen("uploads/csv/billing.csv", "r")) !== false) {
			while (($row = fgetcsv($fp)) !== false) {
				$id = $this->cleanstr($row[0]);
				$professionalId = $this->cleanstr($row[1]);
				$professional = $professionalId ? $this->em->getRepository(User::class)->find($professionalId) : null;
				$month = $this->cleanstr($row[2]);
				$year = $this->cleanstr($row[3]);
				$document = $this->cleanstr($row[5]);
				$document = $document ? '/media/file/' . $document . '?conf=public_facturas&view=list&module=1' : null;
				$paid = $this->cleanstr($row[7]);
				$paid = $paid ? true : false;

				$billing = $id ? $this->em->getRepository(Billing::class)->find($id) : null;
				if (!$billing && $id) {
					$billing = new Billing();
					$billing->setId($id);
					$billing->setProfessional($professional);
					$billing->setMonth($month);
					$billing->setYear($year);
					$billing->setBill($document);
					$billing->setPaid($paid);

					$this->em->persist($billing);
				}
			}
			fclose($fp);
			$this->em->flush();
		}

		return $this->json('ok', 200);
	}

	/**
	 * @Route("/api/importarEventos", name="public_api_importarEventos")
	 */
	public function public_api_importarEventos(Request $request)
	{
		if (($fp = fopen("uploads/csv/event.csv", "r")) !== false) {
			while (($row = fgetcsv($fp)) !== false) {
				$id = $this->cleanstr($row[0]);
				$professionalId = $this->cleanstr($row[1]);
				$professional = $professionalId ? $this->em->getRepository(User::class)->find($professionalId) : null;
				$date = $this->cleanstr($row[2]);
				$date = $date ? \DateTime::createFromFormat('Y-m-d H:i:s', $date) : null;
				$comments = $this->cleanstr($row[3]);
				$dateEnd = $this->cleanstr($row[5]);
				$dateEnd = $dateEnd ? \DateTime::createFromFormat('Y-m-d H:i:s', $dateEnd) : null;

				$event = $id ? $this->em->getRepository(Event::class)->find($id) : null;
				if (!$event && $id) {
					$event = new Event();
					$event->setId($id);
					$event->setProfessional($professional);
					$event->setStart($date);
					$event->setEnd($dateEnd);
					$event->setComments($comments);

					$this->em->persist($event);
				}
			}
			fclose($fp);
			$this->em->flush();
		}

		return $this->json('ok', 200);
	}

	/**
	 * @Route("/contract", name="public_contract")
	 */
    public function public_contract(Request $request)
    {
        if ($request->get('id')) {
            $base64DecodeID = base64_decode($request->get('id'));
            $user = $this->em->getRepository(User::class)->find($base64DecodeID);
            if ($user) {
				$type = $user->getProfessionalExclusive() ? 2 : 1;
				$templateEntity = $this->em->getRepository(TemplateContract::class)->findOneBy(['type' => $type]);
				$template = $templateEntity->getTemplate();
				$template = str_replace("{{ user }}", $user, $template);
				$template = str_replace("{{ dni }}", $user->getProfessionalDni(), $template);
				$template = str_replace("{{ address }}", $user->getProfessionalAddress(), $template);
                return $this->render('public/contract.html.twig', [
					'template' => $template,
                    'user' => $user
                ]);
            } else {
                return $this->redirectToRoute('public_home');
            }
        } else {
            return $this->redirectToRoute('public_home');
        }
    }

	/**
	 * @Route("/api/webContact", name="public_api_webContact")
	 */
    public function public_api_webContact(Request $request)
    {
        $name = $request->query->get('name');
        $lastname = $request->query->get('lastname');
        $email = $request->query->get('email');
        $tel = $request->query->get('phone');
        $date = str_replace('/', '-', $request->query->get('date'));
        $hour = $request->query->get('hour');
        $weddingAddress = $request->query->get('place');
        $province = $request->query->get('province');
        $type = $request->query->get('type');
        $comments = $request->query->get('comments');

        if ($name && $lastname && $email && $tel && $date && $hour && $weddingAddress && $province && $type && $comments) {
			// Set hour in weddingDate
			$weddingDate = \DateTime::createFromFormat('d-m-Y', $date);
			if (preg_match('/^(2[0-3]|[01]?[0-9])$/', $hour)) {
				$weddingDate->setTime($hour, 0, 0, 0);
			} else if (preg_match('/^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$/', $hour)) {
				$hour = explode(':', $hour);
				$weddingDate->setTime($hour[0], $hour[1], 0, 0);
			} else {
				$weddingDate->setTime(0, 0, 0, 0);
			}
			$weddingProvince = $this->em->getRepository(Province::class)->findOneBy(['name' => $province]);

            $entityRequest = new EntityRequest();
            $entityRequest->setName($name);
            $entityRequest->setLastname($lastname);
            $entityRequest->setEmail($email);
            $entityRequest->setTel($tel);
            $entityRequest->setWeddingDate($weddingDate);
            $entityRequest->setWeddingAddress($weddingAddress);
            $entityRequest->setWeddingProvince($weddingProvince);
            $entityRequest->setComments($this->remove_emoji($comments));
            $entityRequest->setTipo($type);
            $entityRequest->setAnswered(false);
            $entityRequest->setCreationDate(new \DateTime('now'));
            $this->em->persist($entityRequest);
            $this->em->flush();
        } else {
			return $this->json('missing fields', 500);
		}

		return $this->json('ok', 200);
    }

	/**
	 * @Route("/api/testingSign", name="public_api_testingSign")
	 */
    public function public_api_testingSign(Request $request)
    {
        $id = $request->request->get('id');
        $sign = $request->request->get('sign');
		
		$wedding = $this->em->getRepository(Wedding::class)->find($id);
		if ($wedding && $wedding->getClient() == $this->getUser()) {
			$wedding->setTestingSign($sign);
			$this->em->persist($wedding);
			$this->em->flush();
		} else {
			return $this->json('id not found', 500);
		}

		return $this->json('ok', 200);
    }

	public static function slugify($text, string $divider = '-')
	{
		// replace non letter or digits by divider
		$text = preg_replace('~[^\pL\d]+~u', $divider, $text);

		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);

		// trim
		$text = trim($text, $divider);

		// remove duplicate divider
		$text = preg_replace('~-+~', $divider, $text);

		// lowercase
		$text = strtolower($text);

		if (empty($text)) {
			return 'n-a';
		}

		return $text;
	}

	public static function cleanstr($text, $multiline = false)
	{
		//$text = utf8_encode($text);
		if ($multiline) {
			$text = str_replace("\v", '<br/>', $text);
		} else {
			$text = str_replace("\v", '', $text);
		}
		$text = $text != "" ? $text : null;
		$text = $text != "NULL" ? $text : null;
		return $text;
	}

    public static function remove_emoji($text)
    {
		// Match Enclosed Alphanumeric Supplement
		$regex_alphanumeric = '/[\x{1F100}-\x{1F1FF}]/u';
		$text = preg_replace($regex_alphanumeric, '', $text);
	
		// Match Miscellaneous Symbols and Pictographs
		$regex_symbols = '/[\x{1F300}-\x{1F5FF}]/u';
		$text = preg_replace($regex_symbols, '', $text);
	
		// Match Emoticons
		$regex_emoticons = '/[\x{1F600}-\x{1F64F}]/u';
		$text = preg_replace($regex_emoticons, '', $text);
	
		// Match Transport And Map Symbols
		$regex_transport = '/[\x{1F680}-\x{1F6FF}]/u';
		$text = preg_replace($regex_transport, '', $text);
		
		// Match Supplemental Symbols and Pictographs
		$regex_supplemental = '/[\x{1F900}-\x{1F9FF}]/u';
		$text = preg_replace($regex_supplemental, '', $text);
	
		// Match Miscellaneous Symbols
		$regex_misc = '/[\x{2600}-\x{26FF}]/u';
		$text = preg_replace($regex_misc, '', $text);
	
		// Match Dingbats
		$regex_dingbats = '/[\x{2700}-\x{27BF}]/u';
		$text = preg_replace($regex_dingbats, '', $text);
	
		return $text;
	}
}
