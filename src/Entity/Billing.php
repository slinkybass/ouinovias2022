<?php

namespace App\Entity;

use App\Repository\BillingRepository;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BillingRepository::class)
 * @ORM\Table(name="billing")
 */
class Billing
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity=User::class, inversedBy="professionalBillings")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $professional;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $month;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $year;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $bill;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $paid;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $num;

	/**
	 * @ORM\Column(type="float", nullable=true)
	 */
	private $price;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $accountNumber;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $cifNifType;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $cifNif;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $address;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $city;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $cp;

	public function __toString(): string
	{
		return $this->getProfessional() . ' (' . $this->getDate() .  ')';
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getProfessional(): ?User
	{
		return $this->professional;
	}

	public function setProfessional(?User $professional): self
	{
		$this->professional = $professional;

		return $this;
	}

	public function getMonth(): ?int
	{
		return $this->month;
	}

	public function setMonth(int $month): self
	{
		$this->month = $month;

		return $this;
	}

	public function getMonthName(): string
	{
		$monthStr = null;
		if ($this->month >= 1 && $this->month <= 12) {
			setlocale(LC_ALL, "es_ES", 'Spanish_Spain', 'Spanish');
			$monthStr = strftime('%B', strtotime('01-' . $this->month . '-1970'));
			$monthStr = ucfirst($monthStr);
		}
		return $monthStr;
	}

	public function getYear(): ?int
	{
		return $this->year;
	}

	public function setYear(int $year): self
	{
		$this->year = $year;

		return $this;
	}

	public function getDate(): ?string
	{
		return $this->getMonthName() . ' ' . $this->getYear();
	}

	public function getBill(): ?string
	{
		return $this->bill;
	}

	public function setBill(?string $bill): self
	{
		$this->bill = $bill;

		return $this;
	}

	public function getPaid(): ?bool
	{
		return $this->paid;
	}

	public function setPaid(bool $paid): self
	{
		$this->paid = $paid;

		return $this;
	}

	public function getNum(): ?string
	{
		return $this->num;
	}

	public function setNum(?string $num): self
	{
		$this->num = $num;

		return $this;
	}

	public function getPrice(): ?float
	{
		return $this->price;
	}

	public function setPrice(?float $price): self
	{
		$this->price = $price;

		return $this;
	}

	public function getCalcPrice(): ?float
	{
        $firstDay = new \DateTime(date('Y-m-d H:i:s', strtotime($this->getYear() . '-'. $this->getMonth() . '-' . '01 00:00:00')));
        $lastDay = new \DateTime(date('Y-m-t H:i:s', strtotime($this->getYear() . '-'. $this->getMonth() . '-' . '01 23:59:59')));
		
		$weddings = array();
		foreach ($this->getProfessional()->getWeddingServices() as $weddingService) {
			if (!array_key_exists($weddingService->getWedding()->getId(), $weddings) && (
				$weddingService->getWedding()->getTestingDate() >= $firstDay && $weddingService->getWedding()->getTestingDate() <= $lastDay ||
				$weddingService->getWedding()->getWeddingDate() >= $firstDay && $weddingService->getWedding()->getWeddingDate() <= $lastDay
			)) {
					$weddings[$weddingService->getWedding()->getId()] = $weddingService->getWedding();
			}
		}

		$totalVal = 0;
		foreach ($weddings as $wedding) {
			foreach ($wedding->getWeddingServices() as $weddingservice) {
				$rateService = null;
				foreach ($weddingservice->getProfessional()->getProfessionalRate()->getRateServices() as $rService) {
					if ($rService->getService() == $weddingservice->getService()) {
						$rateService = $rService;
					}
				}

				if ($rateService && $weddingservice->getProfessional() == $this->getProfessional()) {
					$val = 0;
					
					if ($wedding->getWeddingDate() && $this->getMonth() == $wedding->getWeddingDate()->format('m')) {
						if ($wedding->getWeddingPayMethod() == 'Transferencia' || $wedding->getWeddingPayMethod() == 'Tarjeta') {
							$val = $val + ($rateService->getPriceWedding() * $weddingservice->getCount()) + $weddingservice->getWeddingDisplacementCost();
						}
					}

					if ($wedding->getTestingDate() && $this->getMonth() == $wedding->getTestingDate()->format('m')) {
						if ($wedding->getTestingPayMethod() == 'Transferencia' || $wedding->getTestingPayMethod() == 'Tarjeta') {
							$val = $val + ($rateService->getPriceTesting() * $weddingservice->getCount()) + $weddingservice->getTestingDisplacementCost();
						}
					}

					$totalVal = $totalVal + $val;
				}
			}
		}

		if ($totalVal) {
			$iva = round($totalVal - ($totalVal / 1.21), 2);
			$baseImponible = $totalVal - $iva;
			$professionalIrpf = $this->getProfessional()->getProfessionalIrpf() ?? 0;
			$irpf = round($baseImponible * $professionalIrpf / 100, 2);
			$total = $totalVal - $irpf;
		} else {
			$total = 0;
		}
		return $total;
	}

	public function getAccountNumber(): ?string
	{
		return $this->accountNumber;
	}

	public function setAccountNumber(?string $accountNumber): self
	{
		$this->accountNumber = $accountNumber;

		return $this;
	}

	public function getCifNifType(): ?string
	{
		return $this->cifNifType;
	}

	public function setCifNifType(?string $cifNifType): self
	{
		$this->cifNifType = $cifNifType;

		return $this;
	}

	public function getCifNif(): ?string
	{
		return $this->cifNif;
	}

	public function setCifNif(?string $cifNif): self
	{
		$this->cifNif = $cifNif;

		return $this;
	}

	public function getAddress(): ?string
	{
		return $this->address;
	}

	public function setAddress(?string $address): self
	{
		$this->address = $address;

		return $this;
	}

	public function getCity(): ?string
	{
		return $this->city;
	}

	public function setCity(?string $city): self
	{
		$this->city = $city;

		return $this;
	}

	public function getCp(): ?string
	{
		return $this->cp;
	}

	public function setCp(?string $cp): self
	{
		$this->cp = $cp;

		return $this;
	}

	public function getTipoDocumento(): ?string
	{
		return "FACTURA";
	}

	public function getFormaPago(): ?string
	{
		return "Transferencia";
	}

	public function getFechaEmision(): ?string
	{
		return '04/' . $this->getMonth() . '/' . $this->getYear();
	}

	public function getFechaVencimiento(): ?string
	{
		return '04/' . $this->getMonth() . '/' . $this->getYear();
	}
}
