<?php

namespace App\Entity;

use App\Repository\CompanyRepository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CompanyRepository::class)
 * @ORM\Table(name="company")
 */
class Company
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $name;

	/**
	 * @ORM\OneToMany(targetEntity=User::class, mappedBy="clientConocePor")
	 */
	private $clients;

	public function __construct()
	{
		$this->clients = new ArrayCollection();
	}

	public function __toString(): string
	{
		return $this->getName();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(string $name): self
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * @return Collection|User[]
	 */
	public function getClients(): Collection
	{
		return $this->clients;
	}

	public function addClient(User $client): self
	{
		if (!$this->clients->contains($client)) {
			$this->clients[] = $client;
			$client->setClientConocePor($this);
		}

		return $this;
	}

	public function removeClient(User $client): self
	{
		if ($this->clients->removeElement($client)) {
			// set the owning side to null (unless already changed)
			if ($client->getClientConocePor() === $this) {
				$client->setClientConocePor(null);
			}
		}

		return $this;
	}
}
