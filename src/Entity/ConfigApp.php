<?php

namespace App\Entity;

use App\Repository\ConfigAppRepository;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ConfigAppRepository::class)
 * @ORM\Table(name="configApp")
 */
class ConfigApp
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $bankAccount;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $tel;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $emailCCO;

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getBankAccount(): ?string
	{
		return $this->bankAccount;
	}

	public function setBankAccount(string $bankAccount): self
	{
		$this->bankAccount = $bankAccount;

		return $this;
	}

	public function getTel(): ?string
	{
		return $this->tel;
	}

	public function setTel(string $tel): self
	{
		$this->tel = $tel;

		return $this;
	}

	public function getEmailCCO(): ?string
	{
		return $this->emailCCO;
	}

	public function setEmailCCO(?string $emailCCO): self
	{
		$this->emailCCO = $emailCCO;

		return $this;
	}
}
