<?php

namespace App\Entity;

use App\Repository\DisponibilityTestingRepository;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DisponibilityTestingRepository::class)
 * @ORM\Table(name="disponibilityTesting")
 */
class DisponibilityTesting
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity=User::class, inversedBy="disponibilityTestings")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $professional;

	/**
	 * @ORM\Column(type="date")
	 */
	private $date;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $time;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $blocked = 0;

	public function __toString(): string
	{
		return $this->getProfessional() . ' (' . $this->getDate()->format('d/m/Y') .  ')';
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getProfessional(): ?User
	{
		return $this->professional;
	}

	public function setProfessional(?User $professional): self
	{
		$this->professional = $professional;

		return $this;
	}

	public function getDate(): ?\DateTimeInterface
	{
		return $this->date;
	}

	public function setDate(\DateTimeInterface $date): self
	{
		$this->date = $date;

		return $this;
	}

	public function getTime(): ?int
	{
		return $this->time;
	}

	public function setTime(int $time): self
	{
		$this->time = $time;

		return $this;
	}

	public function getBlocked(): ?bool
	{
		return $this->blocked;
	}

	public function setBlocked(bool $blocked): self
	{
		$this->blocked = $blocked;

		return $this;
	}
}
