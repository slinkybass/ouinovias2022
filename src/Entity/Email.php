<?php

namespace App\Entity;

use App\Repository\EmailRepository;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EmailRepository::class)
 * @ORM\Table(name="email")
 */
class Email
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="text", length=255)
	 */
	private $email;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $title;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $content;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $creationDate;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $sended;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $msg;

	public function __toString(): string
	{
		$append = '';
		if ($this->getCreationDate()) {
			$append = ' (' . $this->getCreationDate()->format('d/m/Y') .  ')';
		}
		return $this->getTitle() . $append;
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getEmail(): ?string
	{
		return $this->email;
	}

	public function setEmail(string $email): self
	{
		$this->email = $email;

		return $this;
	}

	public function getTitle(): ?string
	{
		return $this->title;
	}

	public function setTitle(string $title): self
	{
		$this->title = $title;

		return $this;
	}

	public function getContent(): ?string
	{
		return $this->content;
	}

	public function setContent(?string $content): self
	{
		$this->content = $content;

		return $this;
	}

	public function getCreationDate(): ?\DateTimeInterface
	{
		return $this->creationDate;
	}

	public function setCreationDate(\DateTimeInterface $creationDate): self
	{
		$this->creationDate = $creationDate;

		return $this;
	}

	public function getSended(): ?bool
	{
		return $this->sended;
	}

	public function setSended(bool $sended): self
	{
		$this->sended = $sended;

		return $this;
	}

	public function getMsg(): ?string
	{
		return $this->msg;
	}

	public function setMsg(?string $msg): self
	{
		$this->msg = $msg;

		return $this;
	}
}
