<?php

namespace App\Entity;

use App\Repository\EventRepository;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EventRepository::class)
 * @ORM\Table(name="event")
 */
class Event
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity=User::class, inversedBy="events")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $professional;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $start;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $end;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $comments;

	public function __toString(): string
	{
		return $this->getProfessional() . ' (' . $this->getRangeStr() .  ')';
	}

	public function getRangeStr(): string
	{
		return $this->getStart()->format('d/m/Y') . ' ' . $this->getStart()->format('H:i') . ' - ' . $this->getEnd()->format('d/m/y') . ' ' . $this->getEnd()->format('H:i');
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getProfessional(): ?User
	{
		return $this->professional;
	}

	public function setProfessional(?User $professional): self
	{
		$this->professional = $professional;

		return $this;
	}

	public function getStart(): ?\DateTimeInterface
	{
		return $this->start;
	}

	public function setStart(\DateTimeInterface $start): self
	{
		$this->start = $start;

		return $this;
	}

	public function getEnd(): ?\DateTimeInterface
	{
		return $this->end;
	}

	public function setEnd(\DateTimeInterface $end): self
	{
		$this->end = $end;

		return $this;
	}

	public function getComments(): ?string
	{
		return $this->comments;
	}

	public function setComments(?string $comments): self
	{
		$this->comments = $comments;

		return $this;
	}
}
