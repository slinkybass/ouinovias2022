<?php

namespace App\Entity;

use App\Repository\ProvinceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=ProvinceRepository::class)
 * @ORM\Table(name="province")
 * @UniqueEntity(fields={"name"})
 */
class Province
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $name;

	/**
	 * @ORM\ManyToMany(targetEntity=User::class, mappedBy="professionalProvinces")
	 */
	private $professionals;

	/**
	 * @ORM\OneToMany(targetEntity=Request::class, mappedBy="weddingProvince")
	 */
	private $requests;

	/**
	 * @ORM\OneToMany(targetEntity=Wedding::class, mappedBy="weddingProvince")
	 */
	private $weddings;

	public function __construct()
	{
		$this->professionals = new ArrayCollection();
		$this->requests = new ArrayCollection();
		$this->weddings = new ArrayCollection();
	}

	public function __toString(): string
	{
		return $this->getName();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(string $name): self
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * @return Collection<int, User>
	 */
	public function getProfessionals(): Collection
	{
		return $this->professionals;
	}

	public function addProfessional(User $professional): self
	{
		if (!$this->professionals->contains($professional)) {
			$this->professionals[] = $professional;
			$professional->addProfessionalProvince($this);
		}

		return $this;
	}

	public function removeProfessional(User $professional): self
	{
		if ($this->professionals->removeElement($professional)) {
			$professional->removeProfessionalProvince($this);
		}

		return $this;
	}

	/**
	 * @return Collection<int, Request>
	 */
	public function getRequests(): Collection
	{
		return $this->requests;
	}

	public function addRequest(Request $request): self
	{
		if (!$this->requests->contains($request)) {
			$this->requests[] = $request;
			$request->setWeddingProvince($this);
		}

		return $this;
	}

	public function removeRequest(Request $request): self
	{
		if ($this->requests->removeElement($request)) {
			// set the owning side to null (unless already changed)
			if ($request->getWeddingProvince() === $this) {
				$request->setWeddingProvince(null);
			}
		}

		return $this;
	}

	/**
	 * @return Collection<int, Wedding>
	 */
	public function getWeddings(): Collection
	{
		return $this->weddings;
	}

	public function addWedding(Wedding $wedding): self
	{
		if (!$this->weddings->contains($wedding)) {
			$this->weddings[] = $wedding;
			$wedding->setWeddingProvince($this);
		}

		return $this;
	}

	public function removeWedding(Wedding $wedding): self
	{
		if ($this->weddings->removeElement($wedding)) {
			// set the owning side to null (unless already changed)
			if ($wedding->getWeddingProvince() === $this) {
				$wedding->setWeddingProvince(null);
			}
		}

		return $this;
	}
}
