<?php

namespace App\Entity;

use App\Repository\RateRepository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RateRepository::class)
 * @ORM\Table(name="rate")
 */
class Rate
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $url;

	/**
	 * @ORM\OneToMany(targetEntity=RateService::class, mappedBy="rate", orphanRemoval=true)
	 */
	private $rateServices;

	/**
	 * @ORM\OneToMany(targetEntity=User::class, mappedBy="professionalRate")
	 */
	private $professionals;

	public function __construct()
	{
		$this->rateServices = new ArrayCollection();
		$this->professionals = new ArrayCollection();
	}

	public function __toString(): string
	{
		return $this->getName();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(string $name): self
	{
		$this->name = $name;

		return $this;
	}

	public function getUrl(): ?string
	{
		return $this->url;
	}

	public function setUrl(?string $url): self
	{
		$this->url = $url;

		return $this;
	}

	/**
	 * @return Collection|RateService[]
	 */
	public function getRateServices(): Collection
	{
		return $this->rateServices;
	}

	public function addRateService(RateService $rateService): self
	{
		if (!$this->rateServices->contains($rateService)) {
			$this->rateServices[] = $rateService;
			$rateService->setRate($this);
		}

		return $this;
	}

	public function removeRateService(RateService $rateService): self
	{
		if ($this->rateServices->removeElement($rateService)) {
			// set the owning side to null (unless already changed)
			if ($rateService->getRate() === $this) {
				$rateService->setRate(null);
			}
		}

		return $this;
	}

	/**
	 * @return Collection|User[]
	 */
	public function getProfessionals(): Collection
	{
		return $this->professionals;
	}

	public function addProfessional(User $professional): self
	{
		if (!$this->professionals->contains($professional)) {
			$this->professionals[] = $professional;
			$professional->setProfessionalRate($this);
		}

		return $this;
	}

	public function removeProfessional(User $professional): self
	{
		if ($this->professionals->removeElement($professional)) {
			// set the owning side to null (unless already changed)
			if ($professional->getProfessionalRate() === $this) {
				$professional->setProfessionalRate(null);
			}
		}

		return $this;
	}
}
