<?php

namespace App\Entity;

use App\Repository\RateServiceRepository;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RateServiceRepository::class)
 * @ORM\Table(name="rate_service")
 */
class RateService
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="float")
	 */
	private $priceBooking;

	/**
	 * @ORM\Column(type="float")
	 */
	private $priceTesting;

	/**
	 * @ORM\Column(type="float")
	 */
	private $priceWedding;

	/**
	 * @ORM\ManyToOne(targetEntity=Rate::class, inversedBy="rateServices")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $rate;

	/**
	 * @ORM\ManyToOne(targetEntity=Service::class, inversedBy="rateServices")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $service;

	public function __toString(): string
	{
		return $this->getRate() . ' - ' . $this->getService();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getPriceBooking(): ?float
	{
		return $this->priceBooking;
	}

	public function setPriceBooking(float $priceBooking): self
	{
		$this->priceBooking = $priceBooking;

		return $this;
	}

	public function getPriceTesting(): ?float
	{
		return $this->priceTesting;
	}

	public function setPriceTesting(float $priceTesting): self
	{
		$this->priceTesting = $priceTesting;

		return $this;
	}

	public function getPriceWedding(): ?float
	{
		return $this->priceWedding;
	}

	public function setPriceWedding(float $priceWedding): self
	{
		$this->priceWedding = $priceWedding;

		return $this;
	}

	public function getRate(): ?Rate
	{
		return $this->rate;
	}

	public function setRate(?Rate $rate): self
	{
		$this->rate = $rate;

		return $this;
	}

	public function getService(): ?Service
	{
		return $this->service;
	}

	public function setService(?Service $service): self
	{
		$this->service = $service;

		return $this;
	}
}
