<?php

namespace App\Entity;

use App\Repository\RequestRepository;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RequestRepository::class)
 * @ORM\Table(name="request")
 */
class Request
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $lastname;

	/**
	 * @ORM\Column(type="string", length=12, nullable=true)
	 */
	private $tel;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $email;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $weddingDate;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $weddingAddress;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $weddingCity;

	/**
	 * @ORM\ManyToOne(targetEntity=Province::class, inversedBy="requests")
	 */
	private $weddingProvince;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $comments;

	/**
	 * @ORM\Column(type="boolean", nullable=true)
	 */
	private $answered;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $answeredDate;

	/**
	 * @ORM\Column(type="boolean", nullable=true)
	 */
	private $validated;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $validatedDate;

	/**
	 * @ORM\Column(type="boolean", nullable=true)
	 */
	private $pending;

	/**
	 * @ORM\Column(type="boolean", nullable=true)
	 */
	private $accepted;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $denyReason;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $creationDate;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $callDateAnswered;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $callDateValidated;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	private $tipo;

	public function __toString(): string
	{
		return $this->getFullname();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(string $name): self
	{
		$this->name = $name;

		return $this;
	}

	public function getLastname(): ?string
	{
		return $this->lastname;
	}

	public function setLastname(string $lastname): self
	{
		$this->lastname = $lastname;

		return $this;
	}

	public function getFullname(): ?string
	{
		return $this->getName() . ' ' . $this->getLastname();
	}

	public function getTel(): ?string
	{
		return $this->tel;
	}

	public function setTel(?string $tel): self
	{
		$this->tel = $tel;

		return $this;
	}

	public function getEmail(): ?string
	{
		return $this->email;
	}

	public function setEmail(string $email): self
	{
		$this->email = $email;

		return $this;
	}

	public function getWeddingDate(): ?\DateTimeInterface
	{
		return $this->weddingDate;
	}

	public function setWeddingDate(\DateTimeInterface $weddingDate): self
	{
		$this->weddingDate = $weddingDate;

		return $this;
	}

	public function getWeddingAddress(): ?string
	{
		return $this->weddingAddress;
	}

	public function setWeddingAddress(?string $weddingAddress): self
	{
		$this->weddingAddress = $weddingAddress;

		return $this;
	}

	public function getWeddingCity(): ?string
	{
		return $this->weddingCity;
	}

	public function setWeddingCity(?string $weddingCity): self
	{
		$this->weddingCity = $weddingCity;

		return $this;
	}

	public function getWeddingProvince(): ?Province
	{
		return $this->weddingProvince;
	}

	public function setWeddingProvince(?Province $weddingProvince): self
	{
		$this->weddingProvince = $weddingProvince;

		return $this;
	}

	public function getWeddingFulladdress(): ?string
	{
		$ret = null;
		if ($this->getWeddingAddress() && $this->getWeddingCity() && $this->getWeddingProvince()) {
			$ret = $this->getWeddingAddress() . ' - ' . $this->getWeddingCity() . ', ' . $this->getWeddingProvince();
		} else if ($this->getWeddingAddress() && $this->getWeddingCity()) {
			$ret = $this->getWeddingAddress() . ' - ' . $this->getWeddingCity();
		} else if ($this->getWeddingAddress()) {
			$ret = $this->getWeddingAddress();
		} else if ($this->getWeddingCity()) {
			$ret = $this->getWeddingCity();
		}
		return $ret;
	}

	public function getComments(): ?string
	{
		return $this->comments;
	}

	public function setComments(?string $comments): self
	{
		$this->comments = $comments;

		return $this;
	}

	public function getAnswered(): ?bool
	{
		return $this->answered;
	}

	public function setAnswered(?bool $answered): self
	{
		$this->answered = $answered;

		return $this;
	}

	public function getAnsweredDate(): ?\DateTimeInterface
	{
		return $this->answeredDate;
	}

	public function setAnsweredDate(?\DateTimeInterface $answeredDate): self
	{
		$this->answeredDate = $answeredDate;

		return $this;
	}

	public function getValidated(): ?bool
	{
		return $this->validated;
	}

	public function setValidated(?bool $validated): self
	{
		$this->validated = $validated;

		return $this;
	}

	public function getValidatedDate(): ?\DateTimeInterface
	{
		return $this->validatedDate;
	}

	public function setValidatedDate(?\DateTimeInterface $validatedDate): self
	{
		$this->validatedDate = $validatedDate;

		return $this;
	}

	public function getPending(): ?bool
	{
		return $this->pending;
	}

	public function setPending(?bool $pending): self
	{
		$this->pending = $pending;

		return $this;
	}

	public function getAccepted(): ?bool
	{
		return $this->accepted;
	}

	public function setAccepted(?bool $accepted): self
	{
		$this->accepted = $accepted;

		return $this;
	}

	public function getDenyReason(): ?string
	{
		return $this->denyReason;
	}

	public function setDenyReason(?string $denyReason): self
	{
		$this->denyReason = $denyReason;

		return $this;
	}

	public function getCreationDate(): ?\DateTimeInterface
	{
		return $this->creationDate;
	}

	public function setCreationDate(\DateTimeInterface $creationDate): self
	{
		$this->creationDate = $creationDate;

		return $this;
	}

	public function getCallDateAnswered(): ?\DateTimeInterface
	{
		return $this->callDateAnswered;
	}

	public function setCallDateAnswered(?\DateTimeInterface $callDateAnswered): self
	{
		$this->callDateAnswered = $callDateAnswered;

		return $this;
	}

	public function getCallTodayAnswered(): ?bool
	{
		$now = new \DateTime('now');
		$now = $now->setTime(0, 0, 0);
		$callDateAnswered = $this->callDateAnswered;
		$callDateAnswered = $callDateAnswered ? $callDateAnswered->setTime(0, 0, 0) : null;

		return $callDateAnswered ? $callDateAnswered <= $now : false;
	}

	public function getCallDateValidated(): ?\DateTimeInterface
	{
		return $this->callDateValidated;
	}

	public function setCallDateValidated(?\DateTimeInterface $callDateValidated): self
	{
		$this->callDateValidated = $callDateValidated;

		return $this;
	}

	public function getCallTodayValidated(): ?bool
	{
		$now = new \DateTime('now');
		$now = $now->setTime(0, 0, 0);
		$callDateValidated = $this->callDateValidated;
		$callDateValidated = $callDateValidated ? $callDateValidated->setTime(0, 0, 0) : null;

		return $callDateValidated ? $callDateValidated <= $now : false;
	}

	public function getTipo(): ?int
	{
		return $this->tipo;
	}

	public function setTipo(?int $tipo): self
	{
		$this->tipo = $tipo;

		return $this;
	}
}
