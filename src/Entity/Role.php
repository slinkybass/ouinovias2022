<?php

namespace App\Entity;

use App\Repository\RoleRepository;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RoleRepository::class)
 * @ORM\Table(name="role")
 * @UniqueEntity(fields={"name"})
 */
class Role
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $displayName;

	/**
	 * @ORM\OneToMany(targetEntity=User::class, mappedBy="role")
	 */
	private $users;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $isAdmin;

	/**
	 * @ORM\Column(type="array")
	 */
	private $permissions;
	const PERMISSIONS = [
		'entityMedia' => false,
		'entityRequest' => false,
		'entityWedding' => false,
		'entityService' => false,
		'entityRate' => false,
		'entityCompany' => false,
		'entityUser' => false,
		'entityWeddingPlanner' => false,
		'entityProfessional' => false,
		'entityAdmin' => false,
		'entityRole' => false,
		'entityBilling' => false,
		'entityEvent' => false,
		'entityDisponibilityTesting' => false,
		'entityEmail' => false,
		'entityTemplateEmail' => false,
		'entityTemplateContract' => false,
		'entityConfig' => false,
		'entityConfigApp' => false,
		'sectionDisponibility' => false,
		'sectionCalendar' => false,
		'sectionReports' => false,
		'sectionBilling' => false,
		'sectionContract' => false,
	];

	public function __construct()
	{
		$this->users = new ArrayCollection();
	}

	public function __toString(): string
	{
		return $this->getDisplayName();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(string $name): self
	{
		$this->name = $name;

		return $this;
	}

	public function getDisplayName(): ?string
	{
		return $this->displayName;
	}

	public function setDisplayName(string $displayName): self
	{
		$this->displayName = $displayName;
		if (!$this->id) {
			$this->setName("ROLE_" . $this->cleanString($displayName));
		}

		return $this;
	}

	private function cleanString($text)
	{
		// Remove non letter or digits
		$text = preg_replace('~[^\pL\d]+~u', '', $text);
		// Transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		// Remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);
		// Uppercase
		$text = strtoupper($text);
		return $text;
	}

	/**
	 * @return Collection|User[]
	 */
	public function getUsers(): Collection
	{
		return $this->users;
	}

	public function addUser(User $user): self
	{
		if (!$this->users->contains($user)) {
			$this->users[] = $user;
			$user->setRole($this);
		}

		return $this;
	}

	public function removeUser(User $user): self
	{
		if ($this->users->removeElement($user)) {
			// set the owning side to null (unless already changed)
			if ($user->getRole() === $this) {
				$user->setRole(null);
			}
		}

		return $this;
	}

	public function getIsAdmin(): ?bool
	{
		return $this->isAdmin;
	}

	public function setIsAdmin(bool $isAdmin): self
	{
		$this->isAdmin = $isAdmin;

		return $this;
	}

	public function getPermissions(): ?array
	{
		return $this->permissions;
	}

	public function setPermissions(array $permissions): self
	{
		$this->permissions = $permissions;

		return $this;
	}

	public function getPermission($name): ?bool
	{
		$val = false;
		if ($this->permissions && array_key_exists($name, $this->permissions)) {
			$val = $this->permissions[$name];
		} elseif (array_key_exists($name, Role::PERMISSIONS)) {
			$val = Role::PERMISSIONS[$name];
		}

		return $val;
	}

	public function isUp($role): ?bool
	{
		$excludedPermissions = [
			'sectionBilling',
			'sectionContract'
		];
		if ($role == $this) {
			return true;
		}
		$hasPermissions = $this->getPermissions() && count($this->getPermissions()) > 0;
		if (!$hasPermissions) {
			return false;
		}
		foreach ($this->getPermissions() as $permissionName => $permissionValue) {
			if (!in_array($permissionName, $excludedPermissions)) {
				if (!$permissionValue && $role->getPermission($permissionName)) {
					return false;
				}
			}
		}
		return true;
	}
}
