<?php

namespace App\Entity;

use App\Repository\ServiceRepository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ServiceRepository::class)
 * @ORM\Table(name="service")
 */
class Service
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $name;

	/**
	 * @ORM\OneToMany(targetEntity=RateService::class, mappedBy="service", orphanRemoval=true)
	 */
	private $rateServices;

	/**
	 * @ORM\OneToMany(targetEntity=WeddingService::class, mappedBy="service", orphanRemoval=true)
	 */
	private $weddingServices;

	public function __construct()
	{
		$this->rateServices = new ArrayCollection();
		$this->weddingServices = new ArrayCollection();
	}

	public function __toString(): string
	{
		return $this->getName();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(string $name): self
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * @return Collection|RateService[]
	 */
	public function getRateServices(): Collection
	{
		return $this->rateServices;
	}

	public function addRateService(RateService $rateService): self
	{
		if (!$this->rateServices->contains($rateService)) {
			$this->rateServices[] = $rateService;
			$rateService->setService($this);
		}

		return $this;
	}

	public function removeRateService(RateService $rateService): self
	{
		if ($this->rateServices->removeElement($rateService)) {
			// set the owning side to null (unless already changed)
			if ($rateService->getService() === $this) {
				$rateService->setService(null);
			}
		}

		return $this;
	}

	/**
	 * @return Collection|WeddingService[]
	 */
	public function getWeddingServices(): Collection
	{
		return $this->weddingServices;
	}

	public function addWeddingService(WeddingService $weddingService): self
	{
		if (!$this->weddingServices->contains($weddingService)) {
			$this->weddingServices[] = $weddingService;
			$weddingService->setService($this);
		}

		return $this;
	}

	public function removeWeddingService(WeddingService $weddingService): self
	{
		if ($this->weddingServices->removeElement($weddingService)) {
			// set the owning side to null (unless already changed)
			if ($weddingService->getService() === $this) {
				$weddingService->setService(null);
			}
		}

		return $this;
	}
}
