<?php

namespace App\Entity;

use App\Repository\UserRepository;

use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="user")
 * @UniqueEntity(fields={"username"})
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=180, unique=true)
	 */
	private $username;

	/**
	 * @var string
	 */
	private $plainPassword;

	/**
	 * @var string The hashed password
	 * @ORM\Column(type="string")
	 */
	private $password;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $email;

	/**
	 * @ORM\ManyToOne(targetEntity=Role::class, inversedBy="users")
	 * @ORM\JoinColumn(name="role_id", nullable=false)
	 */
	private $role;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $lastname;

	/**
	 * @ORM\Column(type="string", length=12, nullable=true)
	 */
	private $tel1;

	/**
	 * @ORM\Column(type="string", length=12, nullable=true)
	 */
	private $tel2;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $professionalURL;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $professionalDni;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $professionalAddress;

	/**
	 * @ORM\Column(type="boolean", nullable=true)
	 */
	private $professionalExclusive;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $professionalContract;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $professionalContractDate;

	/**
	 * @ORM\ManyToOne(targetEntity=Rate::class, inversedBy="professionals")
	 */
	private $professionalRate;

	/**
	 * @ORM\OneToMany(targetEntity=Billing::class, mappedBy="professional", orphanRemoval=true)
	 */
	private $professionalBillings;

	/**
	 * @ORM\ManyToMany(targetEntity=Province::class, inversedBy="professionals")
	 * @ORM\JoinTable(name="professional_province")
	 */
	private $professionalProvinces;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	private $professionalType;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $professionalIrpf;

	/**
	 * @ORM\Column(type="boolean", nullable=true)
	 */
	private $clientCronEmailFechaPrueba = true;

	/**
	 * @ORM\Column(type="boolean", nullable=true)
	 */
	private $clientCronEmailPreguntarPrueba = true;

	/**
	 * @ORM\Column(type="boolean", nullable=true)
	 */
	private $clientCronEmailPreguntarBoda = true;

	/**
	 * @ORM\Column(type="boolean", nullable=true)
	 */
	private $clientCronEmailFotosBoda = true;

	/**
	 * @ORM\ManyToOne(targetEntity=Company::class, inversedBy="clients")
	 */
	private $clientConocePor;

	/**
	 * @ORM\OneToMany(targetEntity=Wedding::class, mappedBy="client", orphanRemoval=true)
	 */
	private $weddings;

	/**
	 * @ORM\OneToMany(targetEntity=Wedding::class, mappedBy="planner")
	 */
	private $plannerWeddings;

	/**
	 * @ORM\OneToMany(targetEntity=WeddingService::class, mappedBy="professional", orphanRemoval=true)
	 */
	private $weddingServices;

	/**
	 * @ORM\OneToMany(targetEntity=Event::class, mappedBy="professional", orphanRemoval=true)
	 */
	private $events;

	/**
	 * @ORM\OneToMany(targetEntity=DisponibilityTesting::class, mappedBy="professional", orphanRemoval=true)
	 */
	private $disponibilityTestings;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $active = true;

	public function __construct()
	{
		$this->professionalBillings = new ArrayCollection();
		$this->professionalProvinces = new ArrayCollection();
		$this->weddings = new ArrayCollection();
		$this->plannerWeddings = new ArrayCollection();
		$this->weddingServices = new ArrayCollection();
		$this->events = new ArrayCollection();
		$this->disponibilityTestings = new ArrayCollection();
	}

	public function __toString(): string
	{
		return $this->getFullname();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getUsername(): string
	{
		return $this->getUserIdentifier();
	}

	public function setUsername(string $username): self
	{
		$this->username = $username;

		return $this;
	}

	/**
	 * A visual identifier that represents this user.
	 *
	 * @see UserInterface
	 */
	public function getUserIdentifier(): string
	{
		return (string) $this->username;
	}

	public function getPlainPassword(): ?string
	{
		return $this->plainPassword;
	}

	public function setPlainPassword(?string $plainPassword): void
	{
		$this->plainPassword = $plainPassword;
	}

	/**
	 * @see PasswordAuthenticatedUserInterface
	 */
	public function getPassword(): string
	{
		return $this->password;
	}

	public function setPassword(string $password): self
	{
		$this->password = $password;

		return $this;
	}

	/**
	 * Returning a salt is only needed, if you are not using a modern
	 * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
	 *
	 * @see UserInterface
	 */
	public function getSalt(): ?string
	{
		return null;
	}

	/**
	 * @see UserInterface
	 */
	public function eraseCredentials()
	{
		$this->plainPassword = null;
	}

	public function getEmail(): ?string
	{
		return $this->email;
	}

	public function setEmail(string $email): self
	{
		$this->email = $email;

		return $this;
	}

	/**
	 * @see UserInterface
	 */
	public function getRoles(): array
	{
		$roles = array($this->getRole()->getName());
		if ($this->getRole()->getIsAdmin()) {
			$roles[] = 'ROLE_ADMIN';
		}

		return array_unique($roles);
	}

	public function getRole(): ?Role
	{
		return $this->role;
	}

	public function setRole(?Role $role): self
	{
		$this->role = $role;

		return $this;
	}

	public function hasPermission($name): ?bool
	{
		return $this->role->getPermission($name);
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(string $name): self
	{
		$this->name = $name;

		return $this;
	}

	public function getLastname(): ?string
	{
		return $this->lastname;
	}

	public function setLastname(string $lastname): self
	{
		$this->lastname = $lastname;

		return $this;
	}

	public function getFullname(): ?string
	{
		return $this->getName() . ' ' . $this->getLastname();
	}

	public function getTel1(): ?string
	{
		return $this->tel1;
	}

	public function setTel1(?string $tel1): self
	{
		$this->tel1 = $tel1;

		return $this;
	}

	public function getTel2(): ?string
	{
		return $this->tel2;
	}

	public function setTel2(?string $tel2): self
	{
		$this->tel2 = $tel2;

		return $this;
	}

	public function getProfessionalURL(): ?string
	{
		return $this->professionalURL;
	}

	public function setProfessionalURL(?string $professionalURL): self
	{
		$this->professionalURL = $professionalURL;

		return $this;
	}

	public function getProfessionalDni(): ?string
	{
		return $this->professionalDni;
	}

	public function setProfessionalDni(?string $professionalDni): self
	{
		$this->professionalDni = $professionalDni;

		return $this;
	}

	public function getProfessionalAddress(): ?string
	{
		return $this->professionalAddress;
	}

	public function setProfessionalAddress(?string $professionalAddress): self
	{
		$this->professionalAddress = $professionalAddress;

		return $this;
	}

	public function getProfessionalExclusive(): ?bool
	{
		return $this->professionalExclusive;
	}

	public function setProfessionalExclusive(?bool $professionalExclusive): self
	{
		$this->professionalExclusive = $professionalExclusive;

		return $this;
	}

	public function getProfessionalContract(): ?string
	{
		return $this->professionalContract;
	}

	public function setProfessionalContract(?string $professionalContract): self
	{
		$this->professionalContract = $professionalContract;

		return $this;
	}

	public function getProfessionalContractDate(): ?\DateTimeInterface
	{
		return $this->professionalContractDate;
	}

	public function setProfessionalContractDate(?\DateTimeInterface $professionalContractDate): self
	{
		$this->professionalContractDate = $professionalContractDate;

		return $this;
	}

	public function getProfessionalRate(): ?Rate
	{
		return $this->professionalRate;
	}

	public function setProfessionalRate(?Rate $professionalRate): self
	{
		$this->professionalRate = $professionalRate;

		return $this;
	}

	/**
	 * @return Collection|Billing[]
	 */
	public function getProfessionalBillings(): Collection
	{
		return $this->professionalBillings;
	}

	public function addProfessionalBilling(Billing $professionalBilling): self
	{
		if (!$this->professionalBillings->contains($professionalBilling)) {
			$this->professionalBillings[] = $professionalBilling;
			$professionalBilling->setProfessional($this);
		}

		return $this;
	}

	public function removeProfessionalBilling(Billing $professionalBilling): self
	{
		if ($this->professionalBillings->removeElement($professionalBilling)) {
			// set the owning side to null (unless already changed)
			if ($professionalBilling->getProfessional() === $this) {
				$professionalBilling->setProfessional(null);
			}
		}

		return $this;
	}

	/**
	 * @return Collection<int, Province>
	 */
	public function getProfessionalProvinces(): Collection
	{
		return $this->professionalProvinces;
	}

	public function addProfessionalProvince(Province $professionalProvince): self
	{
		if (!$this->professionalProvinces->contains($professionalProvince)) {
			$this->professionalProvinces[] = $professionalProvince;
		}

		return $this;
	}

	public function removeProfessionalProvince(Province $professionalProvince): self
	{
		$this->professionalProvinces->removeElement($professionalProvince);

		return $this;
	}

	public function getProfessionalType(): ?int
	{
		return $this->professionalType;
	}

	public function setProfessionalType(?int $professionalType): self
	{
		$this->professionalType = $professionalType;

		return $this;
	}

	public function getProfessionalIrpf(): ?string
	{
		return $this->professionalIrpf;
	}

	public function setProfessionalIrpf(?string $professionalIrpf): self
	{
		$this->professionalIrpf = $professionalIrpf;

		return $this;
	}

	public function getClientCronEmailFechaPrueba(): ?bool
	{
		return $this->clientCronEmailFechaPrueba;
	}

	public function setClientCronEmailFechaPrueba(?bool $clientCronEmailFechaPrueba): self
	{
		$this->clientCronEmailFechaPrueba = $clientCronEmailFechaPrueba;

		return $this;
	}

	public function getClientCronEmailPreguntarPrueba(): ?bool
	{
		return $this->clientCronEmailPreguntarPrueba;
	}

	public function setClientCronEmailPreguntarPrueba(?bool $clientCronEmailPreguntarPrueba): self
	{
		$this->clientCronEmailPreguntarPrueba = $clientCronEmailPreguntarPrueba;

		return $this;
	}

	public function getClientCronEmailPreguntarBoda(): ?bool
	{
		return $this->clientCronEmailPreguntarBoda;
	}

	public function setClientCronEmailPreguntarBoda(?bool $clientCronEmailPreguntarBoda): self
	{
		$this->clientCronEmailPreguntarBoda = $clientCronEmailPreguntarBoda;

		return $this;
	}

	public function getClientCronEmailFotosBoda(): ?bool
	{
		return $this->clientCronEmailFotosBoda;
	}

	public function setClientCronEmailFotosBoda(?bool $clientCronEmailFotosBoda): self
	{
		$this->clientCronEmailFotosBoda = $clientCronEmailFotosBoda;

		return $this;
	}

	public function getClientConocePor(): ?Company
	{
		return $this->clientConocePor;
	}

	public function setClientConocePor(?Company $clientConocePor): self
	{
		$this->clientConocePor = $clientConocePor;

		return $this;
	}

	/**
	 * @return Collection|Wedding[]
	 */
	public function getWeddings(): Collection
	{
		return $this->weddings;
	}

	public function addWedding(Wedding $wedding): self
	{
		if (!$this->weddings->contains($wedding)) {
			$this->weddings[] = $wedding;
			$wedding->setClient($this);
		}

		return $this;
	}

	public function removeWedding(Wedding $wedding): self
	{
		if ($this->weddings->removeElement($wedding)) {
			// set the owning side to null (unless already changed)
			if ($wedding->getClient() === $this) {
				$wedding->setClient(null);
			}
		}

		return $this;
	}

	/**
	 * @return Collection|Wedding[]
	 */
	public function getPlannerWeddings(): Collection
	{
		return $this->plannerWeddings;
	}

	public function addPlannerWedding(Wedding $plannerWedding): self
	{
		if (!$this->plannerWeddings->contains($plannerWedding)) {
			$this->plannerWeddings[] = $plannerWedding;
			$plannerWedding->setPlanner($this);
		}

		return $this;
	}

	public function removePlannerWedding(Wedding $plannerWedding): self
	{
		if ($this->plannerWeddings->removeElement($plannerWedding)) {
			// set the owning side to null (unless already changed)
			if ($plannerWedding->getPlanner() === $this) {
				$plannerWedding->setPlanner(null);
			}
		}

		return $this;
	}

	/**
	 * @return Collection|WeddingService[]
	 */
	public function getWeddingServices(): Collection
	{
		return $this->weddingServices;
	}

	public function addWeddingService(WeddingService $weddingService): self
	{
		if (!$this->weddingServices->contains($weddingService)) {
			$this->weddingServices[] = $weddingService;
			$weddingService->setProfessional($this);
		}

		return $this;
	}

	public function removeWeddingService(WeddingService $weddingService): self
	{
		if ($this->weddingServices->removeElement($weddingService)) {
			// set the owning side to null (unless already changed)
			if ($weddingService->getProfessional() === $this) {
				$weddingService->setProfessional(null);
			}
		}

		return $this;
	}

	/**
	 * @return Collection|Event[]
	 */
	public function getEvents(): Collection
	{
		return $this->events;
	}

	public function addEvent(Event $event): self
	{
		if (!$this->events->contains($event)) {
			$this->events[] = $event;
			$event->setProfessional($this);
		}

		return $this;
	}

	public function removeEvent(Event $event): self
	{
		if ($this->events->removeElement($event)) {
			// set the owning side to null (unless already changed)
			if ($event->getProfessional() === $this) {
				$event->setProfessional(null);
			}
		}

		return $this;
	}

	/**
	 * @return Collection|DisponibilityTesting[]
	 */
	public function getDisponibilityTestings(): Collection
	{
		return $this->disponibilityTestings;
	}

	public function addDisponibilityTesting(DisponibilityTesting $disponibilityTesting): self
	{
		if (!$this->disponibilityTestings->contains($disponibilityTesting)) {
			$this->disponibilityTestings[] = $disponibilityTesting;
			$disponibilityTesting->setProfessional($this);
		}

		return $this;
	}

	public function removeDisponibilityTesting(DisponibilityTesting $disponibilityTesting): self
	{
		if ($this->disponibilityTestings->removeElement($disponibilityTesting)) {
			// set the owning side to null (unless already changed)
			if ($disponibilityTesting->getProfessional() === $this) {
				$disponibilityTesting->setProfessional(null);
			}
		}

		return $this;
	}

	public function getActive(): ?bool
	{
		return $this->active;
	}

	public function setActive(bool $active): self
	{
		$this->active = $active;

		return $this;
	}
}
