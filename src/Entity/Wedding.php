<?php

namespace App\Entity;

use App\Repository\WeddingRepository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WeddingRepository::class)
 * @ORM\Table(name="wedding")
 */
class Wedding
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity=User::class, inversedBy="weddings")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $client;

	/**
	 * @ORM\ManyToOne(targetEntity=User::class, inversedBy="plannerWeddings")
	 */
	private $planner;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $weddingAddress;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $weddingCity;

	/**
	 * @ORM\ManyToOne(targetEntity=Province::class, inversedBy="weddings")
	 */
	private $weddingProvince;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $weddingDate;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $weddingComments;

	/**
	 * @ORM\Column(type="float", nullable=true)
	 */
	private $weddingDisplacementCost;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $weddingPayMethod;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $weddingPayId;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $weddingCitationSended = 0;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $weddingSatisfaction;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $testingAddress;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $testingCity;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $testingDate;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $testingComments;

	/**
	 * @ORM\Column(type="float", nullable=true)
	 */
	private $testingDisplacementCost;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $testingPayMethod;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $testingPayId;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $testingCitationSended = 0;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $testingSign;

	/**
	 * @ORM\Column(type="string", length=8, nullable=true)
	 */
	private $meetingTime;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $urlFB;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $urlIG;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $requestModify;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $creationDate;

	/**
	 * @ORM\OneToMany(targetEntity=WeddingService::class, mappedBy="wedding", orphanRemoval=true, cascade={"persist", "remove"})
	 */
	private $weddingServices;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $image1;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $image2;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $image3;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $image4;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $image5;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $image6;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	private $tipo;

	public function __construct()
	{
		$this->weddingServices = new ArrayCollection();
	}

	public function __toString(): string
	{
		return $this->getClient() . ' - ' . $this->getWeddingDate()->format('d-m-Y');
	}

    public function getFullWeddingAddress()
    {
		$ret = $this->weddingAddress;
		$ret = empty($this->weddingCity) ? $ret : $ret . ' - ' . $this->weddingCity;
        return $ret;
    }

    public function getFullTestingAddress()
    {
		$ret = $this->testingAddress;
		$ret = empty($this->testingCity) ? $ret : $ret . ' - ' . $this->testingCity;
        return $ret;
    }

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getClient(): ?User
	{
		return $this->client;
	}

	public function setClient(?User $client): self
	{
		$this->client = $client;

		return $this;
	}

	public function getPlanner(): ?User
	{
		return $this->planner;
	}

	public function setPlanner(?User $planner): self
	{
		$this->planner = $planner;

		return $this;
	}

	public function getWeddingAddress(): ?string
	{
		return $this->weddingAddress;
	}

	public function setWeddingAddress(string $weddingAddress): self
	{
		$this->weddingAddress = $weddingAddress;

		return $this;
	}

	public function getWeddingCity(): ?string
	{
		return $this->weddingCity;
	}

	public function setWeddingCity(?string $weddingCity): self
	{
		$this->weddingCity = $weddingCity;

		return $this;
	}

	public function getWeddingProvince(): ?Province
	{
		return $this->weddingProvince;
	}

	public function setWeddingProvince(?Province $weddingProvince): self
	{
		$this->weddingProvince = $weddingProvince;

		return $this;
	}

	public function getWeddingFulladdress(): ?string
	{
		$ret = null;
		if ($this->getWeddingAddress() && $this->getWeddingCity() && $this->getWeddingProvince()) {
			$ret = $this->getWeddingAddress() . ' - ' . $this->getWeddingCity() . ', ' . $this->getWeddingProvince();
		} else if ($this->getWeddingAddress() && $this->getWeddingCity()) {
			$ret = $this->getWeddingAddress() . ' - ' . $this->getWeddingCity();
		} else if ($this->getWeddingAddress()) {
			$ret = $this->getWeddingAddress();
		} else if ($this->getWeddingCity()) {
			$ret = $this->getWeddingCity();
		}
		return $ret;
	}

	public function getWeddingLocation(): ?string
	{
		$ret = null;
		if ($this->getWeddingCity() && $this->getWeddingProvince()) {
			$ret = $this->getWeddingCity() . ', ' . $this->getWeddingProvince();
		} else if ($this->getWeddingCity()) {
			$ret = $this->getWeddingCity();
		} else if ($this->getWeddingProvince()) {
			$ret = $this->getWeddingProvince();
		}
		return $ret;
	}

	public function getWeddingDate(): ?\DateTimeInterface
	{
		return $this->weddingDate;
	}

	public function setWeddingDate(\DateTimeInterface $weddingDate): self
	{
		$this->weddingDate = $weddingDate;

		return $this;
	}

	public function getWeddingComments(): ?string
	{
		return $this->weddingComments;
	}

	public function setWeddingComments(?string $weddingComments): self
	{
		$this->weddingComments = $weddingComments;

		return $this;
	}

	public function getWeddingDisplacementCost(): ?float
	{
		return $this->weddingDisplacementCost;
	}

	public function setWeddingDisplacementCost(?float $weddingDisplacementCost): self
	{
		$this->weddingDisplacementCost = $weddingDisplacementCost;

		return $this;
	}

	public function getWeddingPayMethod(): ?string
	{
		return $this->weddingPayMethod;
	}

	public function setWeddingPayMethod(?string $weddingPayMethod): self
	{
		$this->weddingPayMethod = $weddingPayMethod;

		return $this;
	}

	public function getWeddingPayId(): ?string
	{
		return $this->weddingPayId;
	}

	public function setWeddingPayId(?string $weddingPayId): self
	{
		$this->weddingPayId = $weddingPayId;

		return $this;
	}

	public function getWeddingCitationSended(): ?bool
	{
		return $this->weddingCitationSended;
	}

	public function setWeddingCitationSended(bool $weddingCitationSended): self
	{
		$this->weddingCitationSended = $weddingCitationSended;

		return $this;
	}

	public function getWeddingSatisfaction(): ?string
	{
		return $this->weddingSatisfaction;
	}

	public function setWeddingSatisfaction(?string $weddingSatisfaction): self
	{
		$this->weddingSatisfaction = $weddingSatisfaction;

		return $this;
	}

	public function getTestingAddress(): ?string
	{
		return $this->testingAddress;
	}

	public function setTestingAddress(?string $testingAddress): self
	{
		$this->testingAddress = $testingAddress;

		return $this;
	}

	public function getTestingCity(): ?string
	{
		return $this->testingCity;
	}

	public function setTestingCity(?string $testingCity): self
	{
		$this->testingCity = $testingCity;

		return $this;
	}

	public function getTestingFulladdress(): ?string
	{
		$ret = null;
		if ($this->getWeddingAddress() && $this->getWeddingCity()) {
			$ret = $this->getWeddingAddress() . ' - ' . $this->getWeddingCity();
		} else if ($this->getWeddingAddress()) {
			$ret = $this->getWeddingAddress();
		} else if ($this->getWeddingCity()) {
			$ret = $this->getWeddingCity();
		}
		return $ret;
	}

	public function getTestingDate(): ?\DateTimeInterface
	{
		return $this->testingDate;
	}

	public function setTestingDate(?\DateTimeInterface $testingDate): self
	{
		$this->testingDate = $testingDate;

		return $this;
	}

	public function getTestingComments(): ?string
	{
		return $this->testingComments;
	}

	public function setTestingComments(?string $testingComments): self
	{
		$this->testingComments = $testingComments;

		return $this;
	}

	public function getTestingDisplacementCost(): ?float
	{
		return $this->testingDisplacementCost;
	}

	public function setTestingDisplacementCost(?float $testingDisplacementCost): self
	{
		$this->testingDisplacementCost = $testingDisplacementCost;

		return $this;
	}

	public function getTestingPayMethod(): ?string
	{
		return $this->testingPayMethod;
	}

	public function setTestingPayMethod(?string $testingPayMethod): self
	{
		$this->testingPayMethod = $testingPayMethod;

		return $this;
	}

	public function getTestingPayId(): ?string
	{
		return $this->testingPayId;
	}

	public function setTestingPayId(?string $testingPayId): self
	{
		$this->testingPayId = $testingPayId;

		return $this;
	}

	public function getTestingCitationSended(): ?bool
	{
		return $this->testingCitationSended;
	}

	public function setTestingCitationSended(bool $testingCitationSended): self
	{
		$this->testingCitationSended = $testingCitationSended;

		return $this;
	}

	public function getTestingSign(): ?string
	{
		return $this->testingSign;
	}

	public function setTestingSign(?string $testingSign): self
	{
		$this->testingSign = $testingSign;

		return $this;
	}

	public function getMeetingTime(): ?string
	{
		$ret = $this->meetingTime;
		if (strlen($ret) == 8) {
			$ret = substr($ret, 0, -3);
		}
		return $ret;
	}

	public function setMeetingTime(?string $meetingTime): self
	{
		$this->meetingTime = $meetingTime;

		return $this;
	}

	public function getUrlFB(): ?string
	{
		return $this->urlFB;
	}

	public function setUrlFB(?string $urlFB): self
	{
		$this->urlFB = $urlFB;

		return $this;
	}

	public function getUrlIG(): ?string
	{
		return $this->urlIG;
	}

	public function setUrlIG(?string $urlIG): self
	{
		$this->urlIG = $urlIG;

		return $this;
	}

	public function getRequestModify(): ?string
	{
		return $this->requestModify;
	}

	public function setRequestModify(?string $requestModify): self
	{
		$this->requestModify = $requestModify;

		return $this;
	}

	public function getCreationDate(): ?\DateTimeInterface
	{
		return $this->creationDate;
	}

	public function setCreationDate(\DateTimeInterface $creationDate): self
	{
		$this->creationDate = $creationDate;

		return $this;
	}

	/**
	 * @return Collection|WeddingService[]
	 */
	public function getWeddingServices(): Collection
	{
		return $this->weddingServices;
	}

	public function addWeddingService(WeddingService $weddingService): self
	{
		if (!$this->weddingServices->contains($weddingService)) {
			$this->weddingServices[] = $weddingService;
			$weddingService->setWedding($this);
		}

		return $this;
	}

	public function removeWeddingService(WeddingService $weddingService): self
	{
		if ($this->weddingServices->removeElement($weddingService)) {
			// set the owning side to null (unless already changed)
			if ($weddingService->getWedding() === $this) {
				$weddingService->setWedding(null);
			}
		}

		return $this;
	}

	public function getTestingExtraPayment(): ?float
	{
		$val = 0;
		if ($this->getTestingPayMethod() == "Tarjeta") {
			//$val = 2.80;
			$val = 0;
		}
		return $val;
	}

	public function getWeddingExtraPayment(): ?float
	{
		$val = 0;
		if ($this->getWeddingPayMethod() == "Tarjeta") {
			//$val = 2.80;
			$val = 0;
		}
		return $val;
	}

	public function getImage1(): ?string
	{
		return $this->image1;
	}

	public function setImage1(?string $image1): self
	{
		$this->image1 = $image1;

		return $this;
	}

	public function getImage2(): ?string
	{
		return $this->image2;
	}

	public function setImage2(?string $image2): self
	{
		$this->image2 = $image2;

		return $this;
	}

	public function getImage3(): ?string
	{
		return $this->image3;
	}

	public function setImage3(?string $image3): self
	{
		$this->image3 = $image3;

		return $this;
	}

	public function getImage4(): ?string
	{
		return $this->image4;
	}

	public function setImage4(?string $image4): self
	{
		$this->image4 = $image4;

		return $this;
	}

	public function getImage5(): ?string
	{
		return $this->image5;
	}

	public function setImage5(?string $image5): self
	{
		$this->image5 = $image5;

		return $this;
	}

	public function getImage6(): ?string
	{
		return $this->image6;
	}

	public function setImage6(?string $image6): self
	{
		$this->image6 = $image6;

		return $this;
	}

	public function getTipo(): ?int
	{
		return $this->tipo;
	}

	public function setTipo(?int $tipo): self
	{
		$this->tipo = $tipo;

		return $this;
	}
}
