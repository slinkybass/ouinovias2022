<?php

namespace App\Entity;

use App\Repository\WeddingServiceRepository;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WeddingServiceRepository::class)
 * @ORM\Table(name="wedding_service")
 */
class WeddingService
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity=Wedding::class, inversedBy="weddingServices")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $wedding;

	/**
	 * @ORM\ManyToOne(targetEntity=Service::class, inversedBy="weddingServices")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $service;

	/**
	 * @ORM\ManyToOne(targetEntity=User::class, inversedBy="weddingServices")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $professional;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $count;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $location;

	/**
	 * @ORM\Column(type="float", nullable=true)
	 */
	private $weddingDisplacementCost;

	/**
	 * @ORM\Column(type="float", nullable=true)
	 */
	private $testingDisplacementCost;


	public function __toString(): string
	{
		return $this->getCount() . " " . $this->getService() . " (" . $this->getProfessional() . ")";
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getWedding(): ?Wedding
	{
		return $this->wedding;
	}

	public function setWedding(?Wedding $wedding): self
	{
		$this->wedding = $wedding;

		return $this;
	}

	public function getService(): ?Service
	{
		return $this->service;
	}

	public function setService(?Service $service): self
	{
		$this->service = $service;

		return $this;
	}

	public function getProfessional(): ?User
	{
		return $this->professional;
	}

	public function setProfessional(?User $professional): self
	{
		$this->professional = $professional;

		return $this;
	}

	public function getCount(): ?int
	{
		return $this->count;
	}

	public function setCount(int $count): self
	{
		$this->count = $count;

		return $this;
	}

	public function getLocation(): ?string
	{
		return $this->location;
	}

	public function setLocation(?string $location): self
	{
		$this->location = $location;

		return $this;
	}

	public function getWeddingDisplacementCost(): ?float
	{
		return $this->weddingDisplacementCost;
	}

	public function setWeddingDisplacementCost(?float $weddingDisplacementCost): self
	{
		$this->weddingDisplacementCost = $weddingDisplacementCost;

		return $this;
	}

	public function getTestingDisplacementCost(): ?float
	{
		return $this->testingDisplacementCost;
	}

	public function setTestingDisplacementCost(?float $testingDisplacementCost): self
	{
		$this->testingDisplacementCost = $testingDisplacementCost;

		return $this;
	}
}
