<?php

namespace App\EventSubscriber;

use App\Entity\Config;
use App\Entity\ConfigApp;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

use Doctrine\ORM\EntityManagerInterface;

class ConfigSuscriber implements EventSubscriberInterface
{
	private $em;
	private $config;

	public function __construct(EntityManagerInterface $entityManager)
	{
		$this->em = $entityManager;

		//Definition of default config
		$this->config = new \stdClass();
		$this->config->appName = 'Symfony 6 Base';
		$this->config->appLogo = '/build/images/logo.png';
		$this->config->appFavicon = '/build/images/favicon.png';
		$this->config->locale = 'es';
		$this->config->enableUsername = false;
		$this->config->enablePublic = false;
		$this->configApp = new \stdClass();
		$this->configApp->bankAccount = null;
		$this->configApp->tel = null;
		$this->configApp->emailCCO = null;
	}

	public function onKernelRequest(RequestEvent $event)
	{
		$request = $event->getRequest();

		//Set variables
		$dcConfig = $this->em->getRepository(Config::class)->findOneBy(array(), array('id' => 'DESC'));
		$dcConfigApp = $this->em->getRepository(ConfigApp::class)->findOneBy(array(), array('id' => 'DESC'));
		if ($dcConfig) {
			$this->config->appName = $dcConfig->getAppName() ?? $this->config->appName;
			$this->config->appLogo = $dcConfig->getAppLogo() ?? $this->config->appLogo;
			$this->config->appFavicon = $dcConfig->getAppFavicon() ?? $this->config->appFavicon;
			$this->config->locale = $dcConfig->getLocale() ?? $this->config->locale;
			$this->config->enableUsername = $dcConfig->getEnableUsername() ?? $this->config->enableUsername;
			$this->config->enablePublic = $dcConfig->getEnablePublic() ?? $this->config->enablePublic;
			if ($dcConfigApp) {
				$this->configApp->bankAccount = $dcConfigApp->getBankAccount() ?? $this->configApp->bankAccount;
				$this->configApp->tel = $dcConfigApp->getTel() ?? $this->configApp->tel;
				$this->configApp->emailCCO = $dcConfigApp->getEmailCCO() ?? $this->configApp->emailCCO;
			}
		}

		//Set locale
		$request->setLocale($this->config->locale);

		//Return config variable
		$event->getRequest()->getSession()->set('config', $this->config);
		$event->getRequest()->getSession()->set('configApp', $this->configApp);
	}

	public static function getSubscribedEvents()
	{
		return [
			KernelEvents::REQUEST => [['onKernelRequest', 20]],
		];
	}
}
