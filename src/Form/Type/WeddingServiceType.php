<?php

namespace App\Form\Type;

use App\Entity\Service;
use App\Entity\User;
use App\Entity\WeddingService;
use App\Field\FieldGenerator;
use App\Form\FormGenerator;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class WeddingServiceType extends AbstractType
{
	private $translator;

	public function __construct(TranslatorInterface $translator)
	{
		$this->translator = $translator;
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$count = FieldGenerator::integer('count')
			->setLabel($this->translator->trans('entities.weddingService.fields.count'))
			->setFormTypeOption('label_attr.class', 'required')
			->setFormTypeOption('attr.required', true);
		$service = FieldGenerator::association('service')
			->setLabel($this->translator->trans('entities.service.singular'))
			->setFormTypeOption('class', Service::class)
			->setFormTypeOption('label_attr.class', 'required')
			->setFormTypeOption('attr.required', true)
			->setFormTypeOption('attr.data-ea-widget', 'ea-autocomplete');
		$professional = FieldGenerator::association('professional')
			->setLabel($this->translator->trans('entities.professional.singular'))
			->setFormTypeOption('class', User::class)
			->setFormTypeOption('label_attr.class', 'required')
			->setFormTypeOption('attr.required', true)
			->setFormTypeOption('query_builder', function ($queryBuilder) {
				return $queryBuilder->createQueryBuilder('entity')
					->leftJoin('entity.role', 'r')
					->andWhere("r.name = 'ROLE_PROFESSIONAL'")
					->andWhere("entity.active = true");
			})
			->setFormTypeOption('attr.data-ea-widget', 'ea-autocomplete');
		$location = FieldGenerator::text('location')
			->setLabel($this->translator->trans('entities.weddingService.fields.location'));
		$weddingDisplacementCost = FieldGenerator::float('weddingDisplacementCost')
			->setLabel($this->translator->trans('entities.weddingService.fields.weddingDisplacementCost'));
		$testingDisplacementCost = FieldGenerator::float('testingDisplacementCost')
			->setLabel($this->translator->trans('entities.weddingService.fields.testingDisplacementCost'));
			
		$builder = FormGenerator::getFormBuilder($builder, [
			$count,
			$service,
			$professional,
			$location,
			$weddingDisplacementCost,
			$testingDisplacementCost,
		]);
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => WeddingService::class,
		));
	}
}