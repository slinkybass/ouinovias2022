<?php

namespace App\Repository;

use App\Entity\DisponibilityTesting;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DisponibilityTesting|null find($id, $lockMode = null, $lockVersion = null)
 * @method DisponibilityTesting|null findOneBy(array $criteria, array $orderBy = null)
 * @method DisponibilityTesting[]	findAll()
 * @method DisponibilityTesting[]	findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DisponibilityTestingRepository extends ServiceEntityRepository
{
	public function __construct(ManagerRegistry $registry)
	{
		parent::__construct($registry, DisponibilityTesting::class);
	}

	// /**
	//  * @return DisponibilityTesting[] Returns an array of DisponibilityTesting objects
	//  */
	/*
	public function findByExampleField($value)
	{
		return $this->createQueryBuilder('e')
			->andWhere('e.exampleField = :val')
			->setParameter('val', $value)
			->orderBy('e.id', 'ASC')
			->setMaxResults(10)
			->getQuery()
			->getResult()
		;
	}
	*/

	/*
	public function findOneBySomeField($value): ?DisponibilityTesting
	{
		return $this->createQueryBuilder('e')
			->andWhere('e.exampleField = :val')
			->setParameter('val', $value)
			->getQuery()
			->getOneOrNullResult()
		;
	}
	*/
}
