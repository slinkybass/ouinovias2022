<?php

namespace App\Repository;

use App\Entity\Email;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email as SymfonyEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;

/**
 * @method Email|null find($id, $lockMode = null, $lockVersion = null)
 * @method Email|null findOneBy(array $criteria, array $orderBy = null)
 * @method Email[]	findAll()
 * @method Email[]	findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmailRepository extends ServiceEntityRepository
{
	private $mailer;

	public function __construct(ManagerRegistry $registry, MailerInterface $mailer)
	{
		parent::__construct($registry, Email::class);
		$this->mailer = $mailer;
	}

	public function sendEmail($title, $content, $emails, $emailsCC, $emailsCCO, $dev = true)
	{
		$emailAccounts = array_merge($emails, $emailsCC, $emailsCCO);
		$devEmail = 'isra792@gmail.com';

		$email = (new SymfonyEmail())
			->from('reservas@ouinovias.com')
			->subject($title)
			->html($content);
		foreach ($emails as $emailAccount) {
			$emailAccount = $dev ? $devEmail : $emailAccount;
			$email = $email->addTo($emailAccount);
		}
		foreach ($emailsCC as $emailAccount) {
			$emailAccount = $dev ? $devEmail : $emailAccount;
			$email = $email->addCc($emailAccount);
		}
		foreach ($emailsCCO as $emailAccount) {
			$emailAccount = $dev ? $devEmail : $emailAccount;
			$email = $email->addBcc($emailAccount);
		}


		$emailSended = null;
		$emailErrorMsg = null;
		try {
			$this->mailer->send($email);
			$emailSended = true;
		} catch (TransportExceptionInterface $e) {
			$emailSended = false;
			$emailErrorMsg = $e->getMessage();
		}

		$now = new \DateTime('now');
		$email = new Email();
		$email->setEmail(implode(", ", $emailAccounts));
		$email->setTitle($title);
		$email->setContent($content);
		$email->setCreationDate($now);
		$email->setSended($emailSended);
		$email->setMsg($emailErrorMsg);

		return $email;
	}
}
