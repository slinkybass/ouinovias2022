<?php

namespace App\Repository;

use App\Entity\TemplateContract;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TemplateContract|null find($id, $lockMode = null, $lockVersion = null)
 * @method TemplateContract|null findOneBy(array $criteria, array $orderBy = null)
 * @method TemplateContract[]	findAll()
 * @method TemplateContract[]	findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TemplateContractRepository extends ServiceEntityRepository
{
	public function __construct(ManagerRegistry $registry)
	{
		parent::__construct($registry, TemplateContract::class);
	}

	// /**
	//  * @return TemplateContract[] Returns an array of TemplateContract objects
	//  */
	/*
	public function findByExampleField($value)
	{
		return $this->createQueryBuilder('t')
			->andWhere('t.exampleField = :val')
			->setParameter('val', $value)
			->orderBy('t.id', 'ASC')
			->setMaxResults(10)
			->getQuery()
			->getResult()
		;
	}
	*/

	/*
	public function findOneBySomeField($value): ?TemplateContract
	{
		return $this->createQueryBuilder('t')
			->andWhere('t.exampleField = :val')
			->setParameter('val', $value)
			->getQuery()
			->getOneOrNullResult()
		;
	}
	*/
}
