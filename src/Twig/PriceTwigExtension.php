<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class PriceTwigExtension extends AbstractExtension
{
	public function getFilters()
	{
		return [
			new TwigFilter('price', array($this, 'formatPrice'))
		];
	}

	public function formatPrice(float $number, int $decimals = 2, string $decPoint = ',', string $thousandsSep = '.'): string
	{
		$zeroDecimals = null;
		for ($i = $decimals; $i > 0; $i--) { 
			$zeroDecimals .= "0";
		}
		$zeroDecimals = $decPoint . $zeroDecimals;
		return str_replace($zeroDecimals, "", number_format($number, $decimals, $decPoint, $thousandsSep));
	}
}
